function b = isBasis (basis)
%isBasis - Class for the basis of the TauToolbox
%   b = polynomial.isBasis(basis) returns whether or not the basis
%       is implemented in the Tau Toolbox.
%
%input:
%   basis = char array with the basis name
%
%output:
%   b     = logical value
%
%examples:
%   b = polynomial.isBasis('LegendreL');

    persistent warn;
    persistent validBases;
    persistent experimentalBases;

    if isempty(warn)
        validBases = polynomial.basis('supported');
        experimentalBases = polynomial.basis('experimental');
        n = length(experimentalBases);
        warn = containers.Map(experimentalBases, zeros(n,1));
    end

    b = ismember(basis, validBases);
    if b
        return
    end

    b = ismember(basis, experimentalBases);
    if b && ~warn(basis)
        warning('Tau Toolbox support for %s basis is experimental', basis);
        warn(basis) = 1;
    end
end
