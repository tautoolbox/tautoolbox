function errorOp(op, cls1, cls2)
%errorOp - returns an error message describing why the operation is not possible
%          operation: obj1 <op> obj2
%          since the error is fatal the execution is halted
%
%input (required):
%   op         = operator where the error occurred
%   cls1       = left object class
%   cls2       = right object class
%
% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    error('Tau Toolbox: Undefined operator "%s" for input arguments of types "%s" and "%s"', ...
           op, cls1, cls2);
end
