function c = color(varargin)
%color - function to manage the color scheme used in Tau Toolbox
%   c = polynomial.utils.color() returns the next color in the color scheme used
%
% This function handles Tau Toolbox colormap operations.
%
%input(optional):
%  command = char array or string (only known option is "reset")
%
%output:
%   c = RGB triplet
%
%examples:
%   c = polynomial.utils.color()
%
%See also polynomial.Polynomial1.plot.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    persistent cstate;  % state of the color counter
    persistent nc;      % number of color in the scheme
    persistent theme;   % set of triplets with theme colors (RGB)

    if isempty(nc)
        theme = [0         0.4470    0.7410
                 0.8500    0.3250    0.0980
                 0.9290    0.6940    0.1250
                 0.4940    0.1840    0.5560
                 0.4660    0.6740    0.1880
                 0.3010    0.7450    0.9330
                 0.6350    0.0780    0.1840];
        nc = size(theme, 1);
        cstate = 1; % initial color
    end

    if nargin == 0
        c = theme(cstate, :);
        cstate = 1+rem(cstate, nc);
    elseif nargin == 1 && (ischar(varargin{1}) || isstring(varargin{1}))
        if strcmp(varargin{1}, 'reset')
            cstate = 1;
        end
    end
end
