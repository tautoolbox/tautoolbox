function yy = barycentric(x, y, xx)
% computes the value at the abscissae xx of the polynomial interpolating data
% (x,y), via barycentric formula.
n = length(x);
w = ones(n,1);
C = 4/(max(x)-min(x));

% barycentric weights
for j = 1:n
    v = (x(j) - x)*C; v(j) = 1;
    w(j) = prod(sign(v)).*exp(-sum(log(abs(v)))); % change scale
end
w = w./norm(w, inf);

num = zeros(size(xx)); den = num; exact = num;
for j=1:n
    xdiff = xx-x(j); wx = w(j)./xdiff;
    den = den+wx;  num = num+wx*y(j);
    exact(xdiff==0)=j;
end
yy = num./den;
yy(exact>0) = y(exact(exact>0));