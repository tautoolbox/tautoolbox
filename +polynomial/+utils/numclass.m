function cls = numclass(x)
%numclass - returns the type of numeric class
%
%input (required):
%   x          = argument to determine the class type
%
%output:
%   cls        = type of numeric class: vector or matrix

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    if isvector(x)
        cls = 'vector';
    else
        cls = 'matrix';
    end
end
