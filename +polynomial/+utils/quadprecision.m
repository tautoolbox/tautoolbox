function xp = quadprecision(x)
%quadprecision - returns a quadruple precision version of the input argument
%
%input (required):
%   x          = argument to convert
%
%output:
%   xp          = argument in quadruple precision

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

persistent init
if isempty(init)
    init = 1;
    if is_octave()
        pkg load symbolic
        warning('off', 'OctSymPy:sym:rationalapprox');
    end
    if ~exist('vpa') %#ok
        error('polynomial.utils.quadprecision: quadruple precision through vpa is not available')
    end
end

if ~is_octave()
    xp = vpa(x);
else
    xp = sym(x);
end

end
