function n = significantTerms(cn)
    % significantTerms: evaluates how many terms are significant
    n = size(cn, 1);
    extra = ~any(cn, 2);
    while n > 1 && extra(n)
        n = n - 1;
    end
end
