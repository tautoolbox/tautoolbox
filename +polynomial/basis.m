function obj = basis(options)
%basis - returns either a basis object or a list of basis of the TauToolbox
%   b = polynomial.basis(options) creates an orthogonal basis object b
%       (if options is polynomial.settings).
%   b = polynomial.basis(options) creates a string structure b (if options is a string).
%
%input:
%   options = polynomial.settings object with the settings to be used
%   options = a string with command to return a structure char array of polynomial basis
%
%output:
%   b = polynomial.basis object
%   b = structure char array (for options: 'all', 'supported' and 'experimental')
%
%examples:
%   options = polynomial.settings('basis', 'LegendreP', 'domain', [0 1], 'degree', 20);
%   b = polynomial.basis(options);
%
%   % returns list of supported basis
%   polynomial.basis('supported')

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    persistent supportedBases
    persistent experimentalBases
    if isempty(experimentalBases)
        supportedBases = {'ChebyshevT', 'ChebyshevU', 'LegendreP', 'GegenbauerC', 'PowerX'};
        experimentalBases = {'BesselY', 'HermiteH', 'LaguerreL', 'ChebyshevV', 'ChebyshevW'};
    end

    if nargin >= 2
        error('polynomial.basis: wrong number of arguments')
    end

    if nargin == 1
        if ischar(options) || isstring(options)
            options = char(options);
            switch options
                case 'supported'
                    obj = supportedBases;
                case 'experimental'
                    obj = experimentalBases;
                case 'all'
                    obj = [supportedBases(:)', experimentalBases(:)'];
                otherwise
                    error('polynomial.basis: unknown string option')
            end
            return
        end
        if ~isa(options, 'polynomial.settings')
            error('polynomial.basis: unknown argument type')
        end
    else
        options = polynomial.settings();
    end

    if iscell(options.basis)
        basis__  = options.basis{1};
        params__ = options.basis(2:end);
        options.basis = basis__;
    else
        params__ = {};
    end

    switch options.basis
        case 'ChebyshevT'
            obj = polynomial.bases.ChebyshevT(options, params__{:});
        case 'ChebyshevU'
            obj = polynomial.bases.ChebyshevU(options, params__{:});
        case 'ChebyshevV'
            obj = polynomial.bases.ChebyshevV(options, params__{:});
        case 'ChebyshevW'
            obj = polynomial.bases.ChebyshevW(options, params__{:});
        case 'LegendreP'
            obj = polynomial.bases.LegendreP(options, params__{:});
        case 'GegenbauerC'
            obj = polynomial.bases.GegenbauerC(options, params__{:});
        case 'BesselY'
            obj = polynomial.bases.BesselY(options, params__{:});
        case 'HermiteH'
            obj = polynomial.bases.HermiteH(options, params__{:});
        case 'LaguerreL'
            obj = polynomial.bases.LaguerreL(options, params__{:});
        case 'PowerX'
            obj = polynomial.bases.PowerX(options, params__{:});
        otherwise
            error('polynomial.basis: unknown basis %s', options.basis);
    end
end
