classdef BesselY < polynomial.bases.T3Basis
%BesselY - Base class for the orthogonal bases of the Tau Toolbox
%   b = polynomial.bases.BesselY(options) creates an orthogonal basis object b.
%
%input (optional):
%   options = polynomial.settings options (if not provided assume the session options)
%
%output:
%   b       = polynomial.bases.BesselY basis object
%
%example:
%   b = polynomial.bases.BesselY();
%
%See also polynomial.settings.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    methods
        function obj = BesselY(options, varargin)
            obj@polynomial.bases.T3Basis(options);
            obj.name = 'BesselY';
        end

        function par = alpha(~, n)
            par = 1./(2*n+1);
        end

        function par = beta(~, n)
            par = -(n == 0);
        end

        function par = gamma(~, n)
            par = -1./(2*n+1);
        end

        function par = eta(~, i, j)
            par = (i-j+1).*(i+j).*(i+1/2).*(-1).^(-1+i+j);
        end

        function bands = theta(~,n)
            %or bands = [[1./((1:n).*(1:2:2*n-1))'] ...
            bands = [1./((1:n).*(1:2:2*n-1))' ...
                     [0; 1./((1:n-1).*(2:n))'] ...
                     [0; 0; 1./((2:n-1).*(5:2:2*n-1))']];
        end
    end
end
