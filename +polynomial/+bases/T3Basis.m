classdef T3Basis
%T3Basis - Base class for the orthogonal polynomial bases
%
%   b = polynomial.bases.T3Basis(options) creates an orthogonal basis object b.
%
%input (optional):
%   options = polynomial.settings options (if not provided assume the session options)
%
%output:
%   b       = polynomial.bases.T3Basis object
%
% This class should not be called directly as it will fail.
% This is an abstract base class that it required to be inherited before being used.
%
%See also polynomial.settings.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    properties (SetAccess = protected, GetAccess = public)
        name          % Polynomial (possibly orthogonal) basis name.
        domain        % Domain [a b], interval of orthogonality of the basis.
        support       % Canonical support where the inner product is defined (e.g. [-1,1] for Jacobi)
        quadPrecision % Are we using quadruple precision?
        firstpiece    % Is this the first piece in a piecewise problem

        p1            % vector (1,2) with the components of polynomial of order 1: P1(x) = x*p1(1) + p1(2)
        x1            % vector (1,2) with the coordinates of x in the P1 basis (inverse of above)

                      % c1 and c0 are the constants for doing a linear transformations
        c0            % z = c1*x + c0 : for Jacobi families the intended domain is [-1, 1].
        c1            % constant for linear transformations [domain(1), domain(2)] -> intended domain
    end

    methods
        function obj = T3Basis(options, varargin)
            obj.quadPrecision = options.quadPrecision;
            if obj.quadPrecision
                obj.domain = polynomial.utils.quadprecision(options.domain);
            else
                obj.domain = options.domain;
            end
            obj.firstpiece = options.firstpiece;

            % derived from the recurrence relations
            obj.p1 = [-obj.beta(0)/obj.alpha(0) 1/obj.alpha(0)];

            obj.support = [-1,1];
            obj.c1 = 2/(obj.domain(2)-obj.domain(1));
            obj.c0 = -(obj.domain(1)+obj.domain(2))/(obj.domain(2)-obj.domain(1));

            A = [1 (obj.p1(2)*obj.c0+obj.p1(1)); 0 (obj.p1(2)*obj.c1)];
            obj.x1 = A\[0;1];
        end
    end

    % The following methods are abstract and should be implemented in the derived classes
    methods
        % For the eta function j->j-1 and i stays the same to apply the formulas from
        % "Explicit formulae for derivatives and primitives of orthogonal polynomials"
        % José M. A. Matos , Maria João Rodrigues , João Carrilho de Matos
        % arXiv:1703.00743v2 [math.NA] 20Dec2017

        % 3-terms recurrence relation:
        % x*P(n) = alpha(n)*P(n+1) + beta(n)*P(n) + gamma(n)*P(n-1)
        function val = alpha(~, ~)
            error('Should be implemented in derived class')
        end
        function val = beta(~, ~)
            error('Should be implemented in derived class')
        end
        function val = gamma(~, ~)
            error('Should be implemented in derived class')
        end
        % function related with the N matrix (differentiation operator)
        function val = eta(~, ~, ~)
            error('Should be implemented in derived class')
        end
        % function related with the O matrix (integration operator)
        function val = theta(~, ~)
            error('Should be implemented in derived class')
        end
    end

    methods
        function M = matrixM(obj, n)
            %matrixM - M matrix
            %   M = matrixM(obj, n)
            %   This function makes use of paper:
            %   Avoiding Similarity Transformations in the Tau method - 2015 -(José
            %   Matos et al.), and builds the M matrix in the basis used.
            %   Here, we also implemented the orthogonal shift.
            %
            %input:
            %   obj = basis used (basis object).
            %     n = order of the matrix
            %
            %output:
            %   M = matrix such that Ma = xPa.
            %
            %See also matrixN and matrixO.

            % Constants for shift the orthogonal interval.
            f2 = 1/obj.c1;
            f3 = -obj.c0/obj.c1;

            e0 = 0:n-1;
            e1 = 0:n-2;

            M = diag(f2*obj.alpha(e1)+0*e1, -1) + ...
                diag(f2*obj.beta(e0) + f3 + 0*e0) + ...
                diag(f2*obj.gamma(e1+1)+0*e1, 1);
        end

        function N = matrixN(obj, n)
            %matrixN - N matrix related with the differentiation.
            %   N = matrixN(obj, n) returns the N matrix.
            %
            %input:
            %   obj = basis used (basis object).
            %     n = order of the matrix
            %
            %output:
            %   N = matrix such that Na = diff(P)a (double matrix).
            %
            %See also matrixM and matrixO.

            if obj.quadPrecision
                N = polynomial.utils.quadprecision(zeros(n));
            else
                N = zeros(n);
            end

            for j=2:n
                N(1:j-1, j) = obj.c1*obj.eta(0:j-2, j);
            end
        end

        function O = matrixO(obj, n)
            %matrixO - O matrix related with integration.
            %   O = matrixO(obj, n)
            %   This function makes use of paper:
            %   Avoiding Similarity Transformations in the Tau method - 2015 -(José
            %   Matos et al.), and consists in to build the O matrix in the basis used.
            %   Here, we also implemented the orthogonal shift.
            %
            %input:
            %   obj = basis used (basis object).
            %     n = order of the matrix
            %
            %output:
            %   O = matrix such that Oa = intZa.
            %
            %See also matrixM and matrixN.

            % Constants for shift the orthogonal interval.
            f2 = 1/obj.c1;

            if obj.quadPrecision
                np = polynomial.utils.quadprecision(n);
            else
                np = n;
            end

            v = f2*obj.theta(np);
            O = diag(v(1:end-1,1), -1)+diag(v(:, 2))+diag(v(2:end, 3), 1);
        end

        function V = orth2powmatrix(obj, n)
            %orth2powmatrix - Orthogonal to power matrix.
            %   V = orth2powmatrix(obj, n) returns the V matrix
            %   such that XV = P, where X = [x^0, x^1, ..., x^n] is the power series and
            %   P = [P_1, P_2, ..., P_n] is the orthogonal basis.
            %
            %input:
            %   obj    = basis used (basis object).
            %     n    = order of the matrix
            %
            %output:
            %   V      = matrix such that XV = P (double matrix)
            %
            %See also pow2orthmatrix.

            if obj.quadPrecision
                V = polynomial.utils.quadprecision(zeros(n));
            else
                V = zeros(n);
            end

            V(1,1) = 1;
            V(1,2) = (obj.c0-obj.beta(0))/obj.alpha(0);
            V(2,2) = obj.c1/obj.alpha(0);
            for m = 2:n-1
                V(1,     m+1) = ((obj.c0-obj.beta(m-1))*V(1, m)-obj.gamma(m-1)*V(1, m-1))/obj.alpha(m-1);
                V(2:m+1, m+1) = (obj.c1*V(1:m, m)+(obj.c0-obj.beta(m-1))*V(2:m+1, m)-obj.gamma(m-1)*V(2:m+1, m-1))/obj.alpha(m-1);
            end
        end

        function W = pow2orthmatrix(obj, n)
            %pow2orthmatrix - Power to orthogonal matrix.
            %   W = pow2orthmatrix(obj) returns the inverse of matrix V, where V is such
            %   that XV = P, X = [x^0, x^1, ...] is the power series and
            %   P = [P_1, P_2, ...] is the orthogonal basis.
            %   This is more stable than to compute V^-1, inv(V) or I\V.
            %
            %input:
            %   obj = basis used (basis object).
            %     n = order of the matrix
            %
            %output:
            %   W = inverse of V (double matrix)
            %
            %See also orth2powmatrix.

            if obj.quadPrecision
                W = polynomial.utils.quadprecision(zeros(n));
            else
                W = zeros(n);
            end

            M = obj.matrixM(n);
            W(1,1) = 1;
            for i = 2:n
                W(:, i) = M*W(:, i-1);
            end
        end

        function v = polyval(obj, a, x, n)
            %polyval - Orthogonal evaluation.
            %   f = polyval(obj, a, x, n) returns the value of
            %   a polynomial evaluated (orthogonally) at x: f = sum(a_iP_i). Similar
            %   to polyval, this formulation allows to obtain more accurate results
            %   than Matlab's "f = polyval(a'*P(end:-1:1), x)".
            %
            %inputs:
            %   obj    = used basis (basis object)
            %   a      = coefficients (double vector).
            %   x      = input for eval (double scalar, vector or matrix).
            %   n      = order of the matrix
            %
            %output:
            %   v      = sum(a_iP_i(xx)), i = 1:length(a)
            %
            %See also polyvalm and polyvalb.

            % n is the polynomial order that is one less than the number of terms
            if nargin == 3
                n = length(a) -1;
            end

            if obj.quadPrecision
                P0 = polynomial.utils.quadprecision(ones(size(x)));
            else
                P0 = ones(size(x));
            end

            v = a(1)*P0;
            if n == 0; return; end

            % linear transformation from the domain
            z = obj.c1*x + obj.c0;

            % we are taking into account the linear transformation
            % or else the code would be simply: P1 = p1(2)*x + p1(1);
            P1 = obj.p1(2)* z + obj.p1(1);
            v = v + a(2)*P1;
            if n == 1; return; end

            for k = 1:n-1
                P2 = ((z-obj.beta(k)).*P1 - obj.gamma(k)*P0)/obj.alpha(k);
                v = v + a(k+2)*P2;
                P0 = P1;
                P1 = P2;
            end
        end

        function v = polyvalm(obj, a, x, n)
            %polyvalm - Orthogonal evaluation for matrices.
            %   f = polyvalm(x, a, M, n) returns the value of
            %   a polynomial evaluated (orthogonally) at M: f = sum(a_iP_i).
            %
            %inputs:
            %   obj    = used basis (basis object)
            %   a      = coefficients (double vector).
            %   M      = input for evaluation (sparse matrix).
            %   n      = order of the matrix
            %
            %output:
            %   v      = sum(a_iP_i(M)), i = 1:length(a)
            %
            %See also polyval and polyvalb.

            [p, q] = size(x);
            if p ~= q
                error ('polynomial.basis.polyvalm: M argument must be a square matrix');
            end

            if ~(isvector (a) || isempty (a))
                error ('polynomial.basis.polyvalm: polynomial coefficients must be a vector');
            end

            % n is the polynomial order that is one less than the number of terms
            if nargin == 3
                n = length(a) -1;
            end

            if obj.quadPrecision
                id = polynomial.utils.quadprecision(eye(size(x,1)));
            else
                id = eye(size(x,1));
            end
            P0 = id;
            v  = a(1)*P0;
            if n == 0; return; end

            % linear transformation from domain
            z = obj.c1*x + obj.c0*id;

            % we are taking into account the linear transformation
            % or else the code would be simply: P1 = p1(1)*x + p1(2)* P0;
            P1 = obj.p1(2)* z + obj.p1(1)*id;
            v  = v + a(2)*P1;
            if n == 1; return; end

            for k = 1:n-1
                P2 = ((z-obj.beta(k)*id)*P1 - obj.gamma(k)*P0)/obj.alpha(k);
                v = v + a(k+2)*P2;
                P0 = P1;
                P1 = P2;
            end
        end

        function v = polyvalmM(obj, a, n)
            %polyvalmM - Orthogonal evaluation for matrices M.
            %   v = polyvalmM(x, n, a) returns the value of
            %   a polynomial evaluated (orthogonally) at matrix M.
            %   M is the matrix associated with the multiplication
            %   operator in the Tau method: v = sum(a_iP_i).
            %   This function exists because it is possible to use specialized
            %   formulas that take into account the errors that happen
            %   due to the discretization of an infinite operator.
            %   The idea of this function is similar to the relation between
            %   functions log and log1p where the later evaluates log more
            %   accurately in the neighborhood of zero.
            %
            %inputs:
            %   obj    = used basis (basis object)
            %     a    = coefficients (double vector).
            %     n    = order of the matrix
            %
            %output:
            %   v      = sum(a_iP_i(M)), i = 1:length(a)
            v = polyvalm(obj, a, obj.matrixM(n));
        end

        function V = polyvalb(obj, x, n, dmin, dmax)
            %polyvalb - Evaluates the first n orthogonal polynomials at point x.
            %   f = polyvalb(obj, x) returns a vector of values such that
            %   c.polyvalb(x) * c.coeff is the value of a polynomial evaluated at x.
            %   With other optional arguments it returns not only the function but
            %   but also the values of derivatives of the basis polynomials at point x.
            %   This function is useful, for instance, to build the
            %   C block for the initial/boundary conditions.
            %
            %inputs:
            %   obj    = used basis (basis object)
            %   x      = input for evaluation (double scalar).
            %   n      = number of columns
            %
            %input(optional):
            %   dmin   = minimum derivative order (0 by default)
            %   dmax   = maximum derivative order (0 by default)
            %
            %output:
            %   f      = [P0(x), P1(x), ... Pn(x)] (double vector).
            %
            %See also polyval and polyvalm.

            if nargin == 1
                error('polynomial.basis.polyvalb: requires at least another argument')
            end
            if nargin <= 3
                dmin = 0;
            end
            if nargin <= 4
                dmax = dmin;
            end
            if dmin < 0 || dmax < 0
                error('polynomial.basis.polyvalb: requires at least another argument')
            end
            if dmax < dmin
                V = zeros(0, n);
                return
            end

            [p, q] = size(x);
            if p ~=1 && q ~= 1
                error ('polynomial.basis.polyvalb: argument should be either a scalar or a vector');
            end

            % argument must be a row vector
            if q == 1 && p > 1
                x = x';
                q = p;
            end

            if obj.quadPrecision
                v = polynomial.utils.quadprecision(zeros(q, n));
            else
                v = zeros(q, n);
            end

            P0 = ones(size(x));
            v(:,1) = P0; % polynomial with 1 term - degree 0

            if n > 1
                % transformation from the domain
                z = obj.c1*x + obj.c0;

                % we are taking into account the linear transformation
                % or else the code would be simply: P1 = p1(2)*x + p1(1);
                P1 = obj.p1(2)* z + obj.p1(1);
                v(:,2) = P1; % polynomial with 2 terms - degree 1
            end

            for k = 1:n-2
                P2 = ((z-obj.beta(k)).*P1 - obj.gamma(k)*P0)/obj.alpha(k);
                v(:,k+2) = P2;
                P0 = P1;
                P1 = P2;
            end

            % Evaluate derivatives now
            if obj.quadPrecision
                V = polynomial.utils.quadprecision(zeros(q*(dmax-dmin +1), n));
            else
                V = zeros(q*(dmax-dmin+1), n);
            end
            for k=0:(dmin-1)
                v = v*obj.matrixN(n);
            end
            V(1:q, :) = v;
            for k=(dmin+1):dmax
                v = v*obj.matrixN(n);
                V((k-dmin)*q+(1:q),:) = v;
            end
        end

        % Points where to evaluate the functions (ideally those shoud be the
        % zeros of the orthogonal polynomial
        function [x, w] = nodes(obj, n)
            %nodes - Legendre points in [a, b].
            %   x = nodesLegendreP(n, domain) returns the Legendre points in the domain
            %   [a, b]. This function was adapted from Greg von Winckel algorithm.
            %
            %input:
            %   obj    = used basis (basis object)
            %   n      = number of points in [a, b] (integer)
            %
            %output:
            %   x      = Legendre points in [a, b] (double vector)
            %   w      = Legendre weigts in [a, b] (double vector)
            %
            %See also nodesChebyshevT, nodeschebyshevU.

            a = obj.domain(1);
            b = obj.domain(2);
            n = n-1;
            N1 = n+1;
            N2 = n+2;
            xu = linspace(-1, 1, N1)';

            % Initial guess
            y = cos((2*(0:n)'+1)*pi/(2*n+2))+(0.27/N1)*sin(pi*xu*n/N2);

            % Legendre-Gauss Vandermonde Matrix
            L = zeros(N1, N2);

            % Compute the zeros of the N+1 Legendre Polynomial using the recursion
            % relation and the Newton-Raphson method
            y0 = 2;

            % Iterate until new points are uniformly within epsilon of old points
            while max(abs(y-y0)) > eps
                L(:, 1) = 1;
                L(:, 2) = y;

                for k = 2:N1
                    L(:, k+1) = ((2*k-1)*y.*L(:, k)-(k-1)*L(:, k-1))/k;
                end

                % Derivatives of L
                Ld = (N2)*(L(:, N1)-y.*L(:, N2))./(1-y.^2);

                % Update
                y0 = y;

                % Newton-Raphson iteration
                y = y0-L(:, N2)./Ld;
            end

            % Shift from [-1, 1] to [a, b]
            x = sort(((a*(1-y)+b*(1+y))/2));

            if nargout>1
                % Compute weights
                w = (b-a)./((1-y.^2).*Ld.^2)*(N2/N1)^2;
            end
        end

        function y = product(obj, p, q)
            %orthopolyprod - Product of orthogonal polynomials.
            %   y = orthopolyprod(p, q) returns the coefficients of product p*q, where
            %   p and q are both the vector of coefficients in orthogonal basis, i.e.
            %
            %   P(x) = p(1)*P_0(x) + ... + p(m+1)*P_m(x) and
            %   Q(t) = q(1)*P_0(x) + ... + q(n+1)*P_n(x).
            %
            %   Then, the result will be the vector of coefficients y such that
            %
            %   P(x)*Q(x) = f(1)*P_0(x) + ... + f(m+n+1)*P_{m+n+1}(x).
            %
            %inputs:
            %   p = vector of coefficients in orthogonal basis.
            %   q = vector of coefficients in orthogonal basis.
            %
            %output:
            %   f = vector of coefficients in orthogonal basis (P(x)*Q(x)).
            %
            %examples:
            %   p = [7/4, 11/2, -7/4, -1/2]; q = p;
            %   a = (1:8)./(2:4:30); b = ones(1, 8)/2; c = (0:7)./(2:4:30);
            %   orthopolyprod(p, q, a, b, c)
            %
            %See also chebypolyprod and legpolyprod.

            % p and q must be line vectors
            [l, c] = size(p);
            if c == 1 && l >= 1
                p = p';
            end

            [l, c] = size(q);
            if c == 1 && l >= 1
                q = q';
            end

            m = min([length(p), length(q)])-1;
            n = max([length(p), length(q)])-1;
            p = [p zeros(1, n)];
            p = p(1:n+1);
            q = [q zeros(1, n)];
            q = q(1:n+1);
            a = obj.alpha(0:2*n) + zeros(1, 2*n+1);
            b = obj.beta(0:2*n) + zeros(1, 2*n+1);
            c = obj.gamma(0:2*n) + zeros(1, 2*n+1);
            L = zeros(2*n+1, 2*n+1, 2*n+1);
            for k = 1:2*n+1
                L(k, 1, k) = 1;
                L(1, k, k) = 1;
                L(k, 2, k) = (b(k)-b(1))/a(1);
                L(2, k, k) = L(k, 2, k);
            end
            for k = 1:2*n
                L(k+1, 2, k) = c(k+1)/a(1);
                L(2, k+1, k) = L(k+1, 2, k);
                L(k, 2, k+1) = a(k)/a(1);
                L(2, k, k+1) = L(k, 2, k+1);
            end
            for j = 2:2*n
                for i = j+1:2*n
                    L(i, j+1, :) = (c(i)*L(i-1, j, :) - c(j)*L(i, j-1, :) + ...
                                   (b(i)-b(j))*L(i, j, :) + a(i)*L(i+1, j, :))/a(j);
                end
                for i = j+2:2*n
                    L(j+1, i, :) = L(i, j+1, :);
                end
            end
            % y = p*q
            y = zeros(1, 2*n+1);
            for i = 0:n
                % P_i*P_i
                for k = 1:2*i+1
                    y(k) = y(k)+p(i+1)*q(i+1)*L(i+1, i+1, k);
                end
                for j = 0:i-1 % P_i*P_{j}
                    for k = 1:i+j+1
                        y(k) = y(k)+(p(i+1)*q(j+1)+p(j+1)*q(i+1))*L(i+1, j+1, k);
                    end
                end
            end
            y = y(1:m+n+1);
        end

        function [cn, esys, eeval, edom] = interp1f(obj, fx, n)
            %interp1f - Orthogonal polynomial interpolation.
            %   cn    = interp1f(basis, fx, poly, n) interpolates an expression
            %
            %inputs:
            %   obj   = polynomial.basis
            %   fx    = function to interpolate (function handle).
            %
            %inputs(optional):
            %   poly  = polynomial where to evaluate a composed function fx(poly(..))
            %   n     = on how many points do we want to interpolate the function
            %
            %output:
            %   cn    = interpolation coefficients (double vector).
            %   esys  = error in the linear system (double vector).
            %   eeval = (max.) error in the evaluation (double scalar).
            %   edom  = error in the full domain (double vector).
            %
            %See also interporth2 and interporth2inc.

            if ~isa(fx, 'function_handle') && ~isa(fx, 'polynomial.Polynomial1')
                error('polynomial.basis.interp1: 2nd argument must either be a function handle or a polynomial.Polynomial1')
            end

            x = obj.nodes(n);
            A = obj.polyvalb(x, n);

            % let us try to vectorize the function fx even if it is not
            b_success = 0;
            try
                b = fx(x);
                b_success = 1;
            catch
            end
            if ~b_success || ~isvector(b) || size(A, 1) ~= size(b, 1)
                b = arrayfun(fx, x);
            end

            % Solve the system.
            cn = A\b;

            if nargout > 1 % Error in the system solution.
                esys = max(abs(A*f - b));
            end

            if nargout > 3 % we need edom to get eeval
                [eeval, edom] = mad(obj, f, fx, 2*n);
            end
        end

        function cn = interp1(obj, fx, n)
            %interp1 - Orthogonal polynomial interpolation.
            %   cn    = interp1(basis, fx) interpolates an expression
            %
            %input:
            %   obj   = polynomial.basis object.
            %   fx    = function to interpolate (function handle).
            %   n     = number of coefficients
            %
            %output:
            %   cn    = interpolation coefficients (double vector).
            %
            %See also interporth2 and interporth2inc.

            if isnumeric(fx)  % fx is a constant function
                cn = zeros(n,1);
                cn(1) = fx;
                return
            end

            % we need to catch the cases where fx does not support polynomials
            try
                x = polynomial.Polynomial1(obj);
                y = fx(x);
                cn = y.coeff;
                % even if the output is a polynomial its degree can not be larger
                % than what we need
                if length(f) <= n
                    cn = [cn; zeros(n-length(f),1)];
                    return
                end
            end

            % interpolate the function to get a polynomial of the right degree
            cn = obj.interp1f(fx, n);
        end

        function cn = interp1p(obj, fx)
            %interp1p - return the coefficients of the polynomial.Polynomial1
            % that approximate function fx on orthogonal *basis*
            %
            %input(required):
            %  obj  = an orthogonal basis
            %   fx  = function handle of the function to be interpolated
            %
            %output:
            %    cn = coefficients of the orthogonal polynomial of the interpolation
            try
                x = polynomial.Polynomial1(obj);
                y = fx(x)+0*x;
                cn = y.coeff;
            catch
                cn = obj.interp1fp(fx);
            end
        end

        function cn = interp1fp(obj, fx)
            %interp1 - return the coefficients of the polynomial.Polynomial1
            % that approximate function fx on orthogonal *basis*
            %
            %input(required):
            %   obj = an orthogonal basis
            %    fx = function handle of the function to be interpolated
            %
            %output:
            %    cn = coefficients of the orthogonal polynomial of the interpolation

            n = 16;
            nd = 32;
            interpMaxDim = polynomial.settings.session('interpMaxDim');
            interpRelTol = polynomial.settings.session('interpRelTol');
            % for parity reasons if nd == n ou nd == 1 we repeat the procedure
            while n < interpMaxDim && nd >= n - 1
                n = n * 2;
                f = obj.interp1f(fx, n);
                f(abs(f)/max(abs(f)) < interpRelTol) = 0;
                nd = polynomial.utils.significantTerms(f);
            end
            if n >= interpMaxDim
                warning(['basis.interp1fp: did not converge within the optional parameters, ' ...
                         'you can fiddle with interpRelRol or interpMaxDim from polynomial.settings '])
            end
            cn = f(1:nd);
        end

        function r = systemRow(obj, cnd, n, nequations)
            %systemLine - returns a row with the coefficients of the polynomial to satisfy the condition.
            %
            %   r = condition.systemRow(basis);
            %
            %input:
            %   obj        = polynomial.basis object
            %   cnd        = condition object
            %     n        = number of columns
            %   nequations = number of equations in the problem
            %
            %output:
            %   r          = row vector

            C = zeros(length(cnd.coeff), n*nequations);

            for j = 1:length(cnd.coeff)
                if cnd.point(j) < obj.domain(1) || cnd.point(j)> obj.domain(2)
                    continue
                end
                if cnd.point(j) == obj.domain(1) && ~obj.firstpiece
                    continue
                end
                idx = (cnd.var(j)-1)*n + (1:n);
                C(j,idx) = cnd.coeff(j) * obj.polyvalb(cnd.point(j), n, cnd.order(j), cnd.order(j));
            end
            r = sum(C, 1);
        end

        function [C, b] = systemBlock(obj, conds, n, nequations)
            %systemBlock - returns a row with the coefficients of the polynomial to satisfy the condition.
            %
            %   r = basis.systemBlock(obj, conds, n, nequations);
            %
            %input:
            %   obj        = polynomial.basis object
            %   cnd        = cell array of tau.{eig,ode}.condition objects
            %   n          = n-1 is the polynomial degree
            %   nequations = number of equations in the problem
            %
            %output:
            %   C          = matrix block corresponding to the conditions part
            %   b          = independent terms corresponding to the conditions part

            nu = size(conds, 1);
            if nu == 0
                C = [];
                b = [];
                return
            end

            depth = max(cellfun(@(c) c.n, conds));
            C = cell(depth,1);

            for j=1:depth
                    C{j} = zeros(nu, n*nequations);
            end
            b = zeros(nu,1);

            for k=1:nu
                C{1}(k, :) = obj.systemRow(conds{k}{1}, n, nequations);
                for j = 2:conds{k}.n
                    C{j}(k, :) = obj.systemRow(conds{k}{j}, n, nequations);
                end
                b(k) = conds{k}{1}.value;
            end
            if length(C) == 1
                C = C{1};
            end
        end
    end

    methods (Access = public)
        function [eeval, edom] = mad(obj, f, fx, steps)
            %mad - maximum absolute deviation between polynomial f and function fx.
            %    [eeval, edom] = mad(obj, f, fx)
            %    evaluates the maximum absolute deviation between polynomial f an function fx.
            %
            %input:
            %   obj = basis used
            %     f = polynomial to be evaluated
            %    fx = function to be evaluated
            %
            %output:
            %   eeval = (max.) error in the evaluation (double scalar).
            %   edom  = error in the full domain (double vector).
            points = linspace(obj.domain(1), obj.domain(2), steps);
            edom = zeros(steps, 2);
            edom(:, 1) = points;
            edom(:, 2) = f(points) - fx(points);
            eeval = max(abs(edom(:, 2)));
        end
    end
end
