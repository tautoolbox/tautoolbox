classdef PowerX < polynomial.bases.T3Basis
%PowerX - Base class for the orthogonal bases of the Tau Toolbox
%   b = polynomial.bases.PowerX(options) creates an orthogonal basis object b.
%
%input (optional):
%   options = polynomial.settings options (if not provided assume the session options)
%
%output:
%   b       = polynomial.bases.PowerX basis object
%
%example:
%   b = polynomial.bases.PowerX();
%
%See also polynomial.settings.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    methods
        function obj = PowerX(options, varargin)
            obj@polynomial.bases.T3Basis(options);
            obj.name = 'PowerX';
        end

        function par = alpha(~, ~)
            par = 1;
        end

        function par = beta(~, ~)
            par = 0;
        end

        function par = gamma(~, ~)
            par = 0;
        end

        function par = eta(~, i, j)
            par = (j-1)*((j-2) == i);
        end

        function bands = theta(~,n)
            bands = [1./(1:n)' zeros(n,1) zeros(n,1)];
        end
    end
end
