classdef LegendreP < polynomial.bases.T3Basis
%LegendreP - Base class for the orthogonal bases of the Tau Toolbox
%   b = polynomial.bases.LegendreP(options) creates an orthogonal basis object b.
%
%input (optional):
%   options = polynomial.settings options (if not provided assume the session options)
%
%output:
%   b       = polynomial.bases.LegendreP basis object
%
%example:
%   b = polynomial.bases.LegendreP();
%
%See also polynomial.settings.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    methods
        function obj = LegendreP(options, varargin)
            obj@polynomial.bases.T3Basis(options);
            obj.name = 'LegendreP';
        end

        function par = alpha(~, n)
            par = (n+1)./(2*n+1);
        end

        function par = beta(~, ~)
            par = 0;
        end

        function par = gamma(~, n)
            par = n./(2*n+1);
        end

        function par = eta(~, i, j)
            par = 2*mod(i+j+1,2).*(i+1/2);
        end

        function bands = theta(~,n)
            bands = [1./(1:2:2*n-1)' ...
                     zeros(n,1) ...
                     [0; 0; -1./(5:2:2*n-1)']];
        end

        function y = product(~, p, q)
            %prodruct - Product of Legendre polynomials.
            %   y = legpolyprod(p, q) returns the coefficients of product p*q, where
            %   p and q are both the vector of coefficients in Legendre basis, i.e.
            %
            %   P(x) = p(1)*L_0(x) + ... + p(m+1)*L_m(x) and
            %   Q(t) = q(1)*L_0(x) + ... + q(n+1)*L_n(x).
            %
            %   Then, the result will be the vector of coefficients y such that
            %
            %   P(x)*Q(x) = f(1)*L_0(x) + ... + f(m+n+1)*L_{m+n+1}(x).
            %
            %inputs:
            %   p = vector of coefficients in Legendre basis.
            %   q = vector of coefficients in Legendre basis.
            %
            %output:
            %   f = vector of coefficients in Legendre basis (P(x)*Q(x)).
            %
            %See also chebypolyprod.

            % p and q must be lines.
            [l, c] = size(p);
            if c == 1 && l >= 1
                p = p';
            end

            [l, c] = size(q);
            if c == 1 && l >= 1
                q = q';
            end

            m = min([length(p), length(q)])-1;
            n = max([length(p), length(q)])-1;
            p = [p zeros(1, n)];
            p = p(1:n+1);
            q = [q zeros(1, n)];
            q = q(1:n+1);
            B = [1 1/2 zeros(1, 4*n-1)];
            for k = 1:4*n-1
                % B_{k+1} = (k/(k+1))B_{k-1}
                B(k+2) = k*B(k)/(k+1);
            end

            % y = p*q
            y = zeros(1, 2*n+1);
            for i = 0:n
                % P_i*P_i
                l = zeros(1, 2*n+1);
                l(1:2:1+2*i) = (1:4:1+4*i)./(1+2*i:2:1+4*i); % k = 0
                for r = 0:2:2*i
                    l(r+1) = l(r+1)*B(2*i-r+1)*B(r+1)^2/B(2*i+r+1);
                end
                y = y+p(i+1)*q(i+1)*l;
                for k = 1:n-i
                    % P_i*P_{i+k}
                    l = zeros(1, 2*n+1);
                    l(1+k:2:1+2*i+k) = (1+2*k:4:1+4*i+2*k)./(1+2*i+2*k:2:1+4*i+2*k);
                    for r = k:2:2*i+k
                        l(r+1) = l(r+1)*B(2*i+k-r+1)*B(r-k+1)*B(r+k+1)/B(2*i+r+k+1);
                    end
                    y = y+(p(i+1)*q(i+k+1)+p(i+k+1)*q(i+1))*l;
                end
            end
            y = y(1:m+n+1);
        end
    end
end
