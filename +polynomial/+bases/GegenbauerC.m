classdef GegenbauerC < polynomial.bases.T3Basis
%GegenbauerC - Base class for the orthogonal bases of the Tau Toolbox
%   b = polynomial.bases.GegenbauerC(options) creates an orthogonal basis object b.
%
%input (optional):
%   options = polynomial.settings options (if not provided assume the session options)
%
%output:
%   b       = polynomial.bases.Gegenbauer basis object
%
%example:
%   b = polynomial.bases.GegenbauerC();
%
%See also polynomial.settings.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    properties (SetAccess = private)
        par    % parameters: par.alpha
    end

    methods
        function obj = GegenbauerC(options, varargin)
            obj@polynomial.bases.T3Basis(options);
            obj.name = 'GegenbauerC';
            par__ = polynomial.bases.GegenbauerC.defaults__();

            % process extra parameters
            if nargin > 1
                p = inputParser();
                p.addParameter('alpha', par__.alpha, @(x) isscalar(x) && x >= 0 && x <= 1);

                p.parse(varargin{:});
                par__ = p.Results;
            end
            obj.par = par__;
        end

        function par = alpha(obj, n)
            par = (n+1)./(n+obj.par.alpha)/2;
        end

        function par = beta(~, ~)
            par = 0;
        end

        function par = gamma(obj, n)
            %par = (n+2*obj.par.alpha-1)./(n+obj.par.alpha)/2;
            par = 1 - obj.alpha(n);
        end

        function par = eta(obj, i, j)
            par = 2*mod(i+j+1,2).*(i+obj.par.alpha);
        end

        function bands = theta(obj,n)
            bands = [1./(2*obj.par.alpha+(0:2:2*(n-1)))' ...
                     zeros(n,1) ...
                     [0; 0; -1./(2*obj.par.alpha+(4:2:2*(n-1)))']];
        end
    end

    methods (Static = true, Access = private)
        function varargout = defaults__ (varargin)
        %polynomial.bases.GegenbauerC.defaults__ - GegenbauerC default parameters.
        %   polynomial.bases.GegenbauerC.__() gets the settings defaults used in GegenbauerC basis
        %
        %output (optional):
        %   val = value of each parameter

            persistent defaultPar;

            % defaultSettings is not defined so set all properties to the default values
            if isempty(defaultPar)
                defaultPar.alpha = 0.5;
            end

            if isempty(varargin)
                if nargout == 0
                    disp(defaultPar);
                elseif nargout == 1
                    varargout{1} = defaultPar;
                end
            end
        end
    end
end
