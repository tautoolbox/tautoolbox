classdef ChebyshevU < polynomial.bases.T3Basis
%ChebyshevU - Base class for the orthogonal bases of the Tau Toolbox
%   b = polynomial.bases.ChebyshevU(options) creates an orthogonal basis object b.
%
%input (optional):
%   options = polynomial.settings options (if not provided assume the session options)
%
%output:
%   b       = polynomial.bases.ChebyshevU basis object
%
%example:
%   b = polynomial.bases.ChebyshevU();
%
%See also polynomial.settings.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    methods
        function obj = ChebyshevU(options, varargin)
            obj@polynomial.bases.T3Basis(options);
            obj.name = 'ChebyshevU';
        end

        function par = alpha(~, ~)
            par = 1/2;
        end

        function par = beta(~, ~)
            par = 0;
        end

        function par = gamma(~, ~)
            par = 1/2;
        end

        function par = eta(~, i, j)
            par = 2*mod(i+j-1,2).*(i+1);
        end

        function bands = theta(~,n)
            bands = [1./(2:2:2*n)' ...
                     zeros(n,1) ...
                     [0; 0; -1./(6:2:2*n)']];
        end

        function x = nodes(obj, n)
            x = cos((n:-1:1)*pi/(n+1))';
            x = (obj.domain(1)*(1-x)+obj.domain(2)*(1+x))/2;
        end
    end
end
