classdef LaguerreL < polynomial.bases.T3Basis
%LaguerreL - Base class for the orthogonal bases of the Tau Toolbox
%   b = polynomial.bases.LaguerreL(options) creates an orthogonal basis object b.
%
%input (optional):
%   options = polynomial.settings options (if not provided assume the session options)
%
%output:
%   b       = polynomial.bases.LaguerreL basis object
%
%example:
%   b = polynomial.bases.LaguerreL();
%
%See also polynomial.settings.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    methods
        function obj = LaguerreL(options, varargin)
            obj@polynomial.bases.T3Basis(options);
            obj.support = [0, inf];
            obj.name = 'LaguerreL';
        end

        function par = alpha(~, n)
            par = -n-1;
        end

        function par = beta(~, n)
            par = 2*n+1;
        end

        function par = gamma(~, n)
            par = -n;
        end

        function par = eta(~, ~, ~)
            par = -1;
        end

        function bands = theta(~,n)
            bands = [-ones(n, 1) ...
                     [0; ones(n-1, 1)] ...
                     zeros(n,1)];
        end
    end
end
