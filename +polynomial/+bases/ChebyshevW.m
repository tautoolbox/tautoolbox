classdef ChebyshevW < polynomial.bases.T3Basis
%ChebyshevW - Base class for the orthogonal bases of the Tau Toolbox
%   b = polynomial.bases.ChebyshevW(options) creates an orthogonal basis object b.
%
%input (optional):
%   options = polynomial.settings options (if not provided assume the session options)
%
%output:
%   b       = polynomial.bases.ChebyshevW basis object
%
%example:
%   b = polynomial.bases.ChebyshevW();
%
%See also polynomial.settings.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    methods
        function obj = ChebyshevW(options, varargin)
            obj@polynomial.bases.T3Basis(options);
            obj.name = 'ChebyshevW';
        end

        function par = alpha(~, ~)
            par = 1/2;
        end

        function par = beta(~, n)
            par = -1/2 .* (n == 0);
        end

        function par = gamma(~, ~)
            par = 1/2;
        end

        function par = eta(~, i, j)
            par = (2*mod(i+j-1,2).*(i+1/2)+j-1-i).*(-1).^(i+j);
        end

        function bands = theta(~,n)
            bands = [1./(2:2:2*n)' ...
                     [0; 1/2*1./((1:n-1).*(2:n))'] ...
                     [0; 0; -1./(4:2:2*n-2)']];
        end

        function x = nodes(obj, n)
            x = cos((n:-1:1)*pi/(n+1/2))';
            x = (obj.domain(1)*(1-x)+obj.domain(2)*(1+x))/2;
        end
    end
end
