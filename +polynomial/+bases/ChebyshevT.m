classdef ChebyshevT < polynomial.bases.T3Basis
%ChebyshevT - Base class for the orthogonal bases of the Tau Toolbox
%   b = polynomial.bases.ChebyshevT(options) creates an orthogonal basis object b.
%
%input (optional):
%   options = polynomial.settings options (if not provided assume the session options)
%
%output:
%   b       = polynomial.basis basis object
%
%example:
%   b = polynomial.bases.ChebyshevT();
%
%See also polynomial.settings.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    methods
        function obj = ChebyshevT(options, varargin)
            obj@polynomial.bases.T3Basis(options);
            obj.name = 'ChebyshevT';
        end

        function par = alpha(~, n)
            par = 1/2  + 1/2 .* (n == 0);
        end

        function par = beta(~, ~)
            par = 0;
        end

        function par = gamma(~, ~)
            par = 1/2;
        end

        function par = eta(~, i, j)
            par = 2*mod(i+j-1,2).*(j-1)./(1 + (i==0));
        end

        function bands = theta(~,n)
            bands = [[1; 1./(4:2:2*n)'] ...
                     zeros(n,1) ...
                     [0; 0; -1./(2:2:2*n-4)']];
        end

        function x = nodes(obj, n)
            x = cos(((n:-1:1)-1/2)*pi/n)';
            x = (obj.domain(1)*(1-x)+obj.domain(2)*(1+x))/2;
        end

        function f = product(~, p, q)
            %chebypolyprod - Product of Chebyshev polynomials.
            %   y = chebypolyprod(p, q) returns the coefficients of product p*q, where
            %   p and q are both the vector of coefficients in Chebyshev basis, i.e.
            %
            %   P(x) = p(1)*T_0(x) + ... + p(m+1)*T_m(x) and
            %   Q(t) = q(1)*T_0(x) + ... + q(n+1)*T_n(x).
            %
            %   Then, the result will be the vector of coefficients y such that
            %
            %   P(x)*Q(x) = f(1)*T_0(x) + ... + f(m+n+1)*T_{m+n+1}(x).
            %
            %   T (Chebyshev of first kind).
            %
            %inputs:
            %   p = vector of coefficients in ChebyshevT basis.
            %   q = vector of coefficients in ChebyshevT basis.
            %
            %output:
            %   f = vector of coefficients in Chebyshev basis (P(x)*Q(x)).
            %
            %See also legpolyprod.

            % p and q must be line vectors
            [l, c] = size(p);
            if c == 1 && l >= 1
                p = p';
            end

            [l, c] = size(q);
            if c == 1 && l >= 1
                q = q';
            end

            % p and q must have the same size
            m = min([length(p), length(q)])-1;
            n = max([length(p), length(q)])-1;
            p = [p zeros(1, n)];
            p = p(1:n+1);
            q = [q zeros(1, n)];
            q = q(1:n+1);

            % y = p*q
            f = zeros(1, 2*n+1);
            f(1) = p(1)*q(1)+p(2:n+1)*q(2:n+1)'/2;

            for k = 1:n
                % y_k
                f(k+1) = (p(1:n+1-k)*q(k+1:n+1)'+p(k+1:n+1)*q(1:n+1-k)' ...
                    + p(1:floor((k-1)/2)+1)*q(k+1:-1:k-floor((k-1)/2)+1)' ...
                    + p(k+1:-1:k-floor((k-1)/2)+1)*q(1:floor((k-1)/2)+1)')/2;
            end
            for k = n+1:2*n-1
                % y_k
                f(k+1) = (p(k-n+1:floor((k-1)/2)+1)*q(n+1:-1:k-floor((k-1)/2)+1)' ...
                    + p(n+1:-1:k-floor((k-1)/2)+1)*q(k-n+1:floor((k-1)/2)+1)')/2;
            end
            f(3:2:2*n-1) = f(3:2:2*n-1)+p(2:n).*q(2:n)/2;
            if n >= 1  % if n=0 then both p and q are scalars
                f(2*n+1) = p(n+1)*q(n+1)/2;
            end
            f = f(1:m+n+1);
        end
    end
end
