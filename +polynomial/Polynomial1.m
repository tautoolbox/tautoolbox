classdef Polynomial1
%polynomial - Class for orthogonal polynomials
%   yn = polynomial.Polynomial1(options) creates an object yn of type polynomial.Polynomial1
%
%input:
%   options (optional) = options (polynomial.settings object)
%
%output:
%   yn = polynomial.Polynomial1 object
%
%examples:
%   yn = polynomial.Polynomial1();
%   plot(yn)
%
%See also polynomial.basis and polynomial.settings.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    properties (SetAccess = private, GetAccess = public)
        basis         % Orthogonal polynomial basis.
        domain        % Domain [a b], interval of orthogonality of the basis.
        n             % Number of coefficients (degree+1)
        pieces        % partition used
        nequations    % Number of equations.
        quadPrecision % Are we using quadruple precision?
        colorPieces   % should pieces be colored in plots?
        steps         % number of steps used to descretize an interval
        coeff         % Coefficients.
    end

    properties (Dependent)
        degree        % polynomial degree
        npieces       % number of pieces (partition intervals).
        coef          % coefficients (an alias for coeff property)
        power_coeff   % coefficients of the polynomial in the power basis
    end

    methods (Access = public)
        function obj = Polynomial1(varargin)
            if nargin > 2
                error('polynomial.Polynomial1: wrong number of arguments')
            end

            % copy constructor
            if nargin == 1 && isa(varargin{1}, 'polynomial.Polynomial1')
                obj = varargin{1};
                return
            end

            % construct polynomial from basis
            if nargin == 1 && isa(varargin{1}, 'polynomial.bases.T3Basis')
                obj.basis = varargin(1);
                obj.domain = varargin{1}.domain;
                obj.n = 2;
                obj.nequations = 1;
                obj.pieces = obj.domain;
                obj.quadPrecision = varargin{1}.quadPrecision;
                obj.colorPieces = polynomial.settings.session('colorPieces');
                obj.steps = polynomial.settings.session('steps');
                obj.coeff = varargin{1}.x1;
                return
            end

            if nargin == 0 % default constructor (no arguments)
                fx = @(x) x;
                options = polynomial.settings();
            elseif nargin == 2
                if isa(varargin{2}, 'polynomial.settings')
                    if isa(varargin{1}, 'polynomial.settings')
                        error('polynomial.Polynomial1: only a polynomial.settings argument is allowed')
                    end
                    options = varargin{2};
                else
                    error('polynomial.Polynomial1: 2nd argument must be of class polynomial.settings')
                end
            elseif ~isa(varargin{1}, 'polynomial.settings')
                options = polynomial.settings();
            end

            if nargin >= 1
                if isa(varargin{1}, 'polynomial.settings')
                    fx = @(x) x;
                    options = varargin{1};
                elseif isa(varargin{1}, 'polynomial.Polynomial1')
                    fx = varargin{1};
                    % TODO: there is an opportunity for optimization here
                    %       if the basis and domain are the same make
                    %       `obj = fx` and return
                elseif isa(varargin{1}, 'function_handle')
                    fx = varargin(1); % function to approximate
                elseif ischar(varargin{1})
                    fx = {str2func(['@(x) ' vectorize(varargin{1})])}; % function to approximate
                elseif iscellstr(varargin{1})
                    fx = varargin{1};
                    fx = cellfun(@(x) str2func(['@(x) ' vectorize(x)]), fx, 'UniformOutput', false);
                elseif iscell(varargin{1})
                    fx = varargin{1};
                elseif isnumeric(varargin{1})
                    if ~isscalar(options.pieces)
                        if any(options.pieces([1, end]) ~= options.domain)
                            warning('polynomial.Polynomial1: the proposed partition does not agree with the domain');
                            options.pieces = 1;
                        end
                    end

                    if isempty(varargin{1})  % null polynomial
                        fx = zeros(1, options.nequations, options.npieces);
                    else
                        fx = varargin{1};    % polynomial coefficients
                        options.pieces = size(fx, 3);
                    end
                    obj.nequations = size(fx, 2);
                else
                    error('polynomial.Polynomial1: %s can not be the 1st argument', class(varargin{1}))
                end
            end

            obj.domain = options.domain;
            obj.quadPrecision = options.quadPrecision;
            obj.colorPieces = options.colorPieces;
            obj.steps = options.steps;

            if ~isscalar(options.pieces)
                if any(options.pieces([1, end]) ~= options.domain)
                    warning('polynomial.Polynomial1: the proposed partition does not agree with the domain');
                    options.pieces = 1;
                end
            end

            if isscalar(options.pieces)
                np = options.pieces + 1;
                obj.pieces = linspace(obj.domain(1), obj.domain(2), np);
            else
                obj.pieces = options.pieces;
            end

            obj.basis = cell(obj.npieces, 1);

            noptions = options;
            noptions.pieces=1;
            for k = 1:obj.npieces
                noptions.domain = obj.pieces(k:k+1);
                obj.basis{k} = polynomial.basis(noptions);
            end

            if isnumeric(fx)
                % Should we trim the input?
                obj.n          = size(fx, 1);

                nd = polynomial.utils.significantTerms(fx);
                obj.coeff = fx(1:nd, :, :);
                obj.n = nd;
                return
            end

            if iscell(fx)
                obj.nequations = length(fx);
                tmp_coeff = cell(obj.nequations, obj.npieces);
                if length(fx) == 1 && obj.nequations ~= 1
                    fx(1:obj.nequations) = fx;
                end
                for i = 1:obj.nequations
                    for j = 1:obj.npieces
                        tmp_coeff{i,j} = obj.basis{j}.interp1fp(fx{i});
                    end
                end
                obj.n = max(max(cellfun (@(x) size(x,1), tmp_coeff)));
                obj.coeff = zeros(obj.n, obj.nequations, obj.npieces);
                if obj.quadPrecision
                    obj.coeff = polynomial.utils.quadprecision(obj.coeff);
                end
                for i = 1:obj.nequations
                    for j = 1:obj.npieces
                        nd = size(tmp_coeff{i,j}, 1);
                        obj.coeff(1:nd,i,j) = tmp_coeff{i,j};
                    end
                end
                return
            end

            % By default use the identitity function for all components
            obj.n = 2;
            obj.nequations = options.nequations;

            obj.coeff = zeros(obj.n, obj.nequations, obj.npieces);
            if obj.quadPrecision
                obj.coeff = polynomial.utils.quadprecision(obj.coeff);
            end

            for j=1:obj.npieces
                for i=1:obj.nequations
                    obj.coeff(:,i,j) = obj.basis{j}.x1;
                end
            end
        end

        function varargout = subsref (obj, idxs)

            switch idxs(1).type
                case '()'
                    if length(idxs(1).subs) > 3
                        error('polynomial.Polynomial1: only one or two arguments are allowed')
                    end
                    x = idxs(1).subs{1};
                    if min(min(x)) < obj.domain(1) || max(max(x)) > obj.domain(2)
                        warning('polynomial.Polynomial1: evaluating points outside the domain')
                    end
                    varargout = {obj.polyval(idxs(1).subs{:})};
                case '{}'
                    % allow either one or two indices:
                    % 1) component (required);
                    % 2) pieces (optional);
                    if isempty(idxs(1).subs) || length(idxs(1).subs) > 2
                        error('polynomial.Polynomial1: only one or two indices are allowed')
                    end

                    subs = idxs(1).subs;
                    if (ischar(subs{1}) || isstring(subs{1})) && strcmp(subs{1}, ':')
                        comps = 1:obj.nequations;
                    else
                        comps = subs{1};
                    end
                    if min(comps) < 1 || max(comps) > obj.nequations
                        error('polynomial.Polynomial1: component - index out of range')
                    end

                    if length(subs) > 1
                        if (ischar(subs{2}) || isstring(subs{2})) && strcmp(subs{2}, ':')
                            pieces_ = 1:obj.npieces;
                        else
                            pieces_ = subs{2};
                        end
                        if min(pieces_) < 1 || max(pieces_) > obj.npieces
                            error('polynomial.Polynomial1: pieces - index out of range')
                        end
                        if length(pieces_)>1 && ~all(diff(pieces_) == 1)
                            error('polynomial.Polynomial1: pieces should be contiguous')
                        end
                    else
                        pieces_ = 1:obj.npieces;
                    end

                    varargout = cell(1, length(comps));
                    for k = 1:length(comps)
                        obj2 = obj;
                        obj2.nequations = 1;
                        if length(pieces_) ~= obj.npieces
                            obj2.domain = obj2.pieces([1,end]);
                            obj2.pieces = obj.pieces(pieces_(1):pieces_(end)+1);
                        end
                        % todo: evaluate signficant terms
                        % nd = polynomial.utils.significantTerms(obj.coeff(:, comps(k), :));
                        obj2.coeff = obj.coeff(:, comps(k), pieces_);
                        varargout{k} = obj2;
                    end

                case '.'
                    varargout = {builtin('subsref', obj, idxs)};
                    return
            end

            if length(idxs) > 1
                if length(varargout) == 1
                    varargout = {varargout{:}.subsref(idxs(2:end))};
                else
                    error('polynomial.Polynomial1: Intermediate brace ''{}'' indexing produced a comma-separated list with %d values, but it must produce a single value when followed by subsequent indexing operations.', length(varargout))
                end
            end
        end

        function n = end(obj, k, n)
            switch k
                case 1
                    n = obj.nequations;
                case 2
                    n = obj.npieces;
            end
        end

        function obj = ctranspose(obj)
            %' - Differentiation operator for polynomial.Polynomial1 objects.
            %    ctranspose(obj) returns the derivative of the polynomial.Polynomial1 variable.

            obj = diff(obj);
        end

        function obj = diff(obj, order)
            %diff - Differentiation of a polynomial.Polynomial1 object for a given order.
            %       diff(obj, order) returns the n-th order derivative of obj.
            %
            %input:
            %   obj (required)   = polynomial.Polynomial1 object
            %   order (optional) = derivative order (integer) (default = 1)
            %
            %output:
            %   obj = polynomial.Polynomial1 object
            %
            %example:
            %   p = polynomial.Polynomial1(@(x) x.^2); p_prime = diff(p);
            %
            %See also integral.

            if nargin == 1
                order = 1;
            end

            for k = 1:order
                if obj.n == 1 % the derivative of constant is zero
                    obj.coeff = zeros(1, obj.nequations, obj.npieces);
                    return
                end

                old_coeff = obj.coeff;
                obj.coeff = zeros(obj.n - 1, obj.nequations, obj.npieces);
                for j = 1:obj.npieces
                    Nd = obj.basis{j}.matrixN(obj.n);
                    for k1 = 1:obj.nequations
                        dv = Nd*old_coeff(:, k1, j);
                        obj.coeff(:, k1, j)=dv(1:end-1);
                    end
                end
                obj.n = obj.n-1;
            end
        end

        function obj = integral(obj, a, b)
            %integral - Integration of a polynomial.Polynomial1 object.
            %       (It considers the primitive at zero to be zero)
            %       integral(obj) returns the indefinite integral of obj.
            %       integral(obj, a, b) returns the definite integral between a and b.
            %
            %input:
            %   obj (required) = polynomial.Polynomial1 object
            %   a  = lower limit for the definite integral (integer)
            %   b  = upper limit for the definite integral (integer)
            %
            %output:
            %   obj = polynomial.Polynomial1 object for indefinite integral and
            %         real value for definite integral
            %
            %example:
            %   p = polynomial.Polynomial1(@(x) x.^2); primitive_p = integral(p);
            %
            %See also diff.

            old_coeff = [obj.coeff; zeros(1, size(obj.coeff,2), size(obj.coeff,3))];
            obj.coeff = zeros(obj.n + 1, obj.nequations, obj.npieces);
            for j = 1:obj.npieces
                Od = obj.basis{j}.matrixO(obj.n+1);
                for k = 1:obj.nequations
                    obj.coeff(:,k,j) = Od*old_coeff(:,k,j);
                end
            end
            obj.n = obj.n+1;

            % zeroing the constant in the primitive
            for j = 1:obj.npieces
                for k = 1:obj.nequations
                    const = obj.basis{j}.polyval(obj.coeff(:,k,j), 0);
                    %const = obj.polyval(0);
                    obj.coeff(1, k, j) = obj.coeff(1, k, j) - const;
                end
            end

            % definite integral
            if nargin > 1
                % overwrite object with the solution (real value)
                val = zeros(1, size(obj.coeff,2));
                for k = 1:obj.nequations
                    val(k) = obj.polyval(b) - obj.polyval(a);
                end
                obj = val;
            end
        end

        function obj = plus(obj, obj2)
            %+ - Sum operator for polynomial.Polynomial1 objects.
            %    plus(obj, obj2) returns obj + obj2.

            if isa(obj,'polynomial.Polynomial1') && isa(obj2,'polynomial.Polynomial1')
                if size(obj.coeff,1) > size(obj2.coeff,1)
                    obj2.coeff(size(obj.coeff,1),1,1) = 0;
                elseif size(obj.coeff,1) < size(obj2.coeff,1)
                    obj.coeff(size(obj2.coeff,1),1,1) = 0;
                    obj.n = obj2.n;
                end
                obj.coeff = obj.coeff + obj2.coeff;
                nd = polynomial.utils.significantTerms(obj.coeff);
                if nd ~= obj.n
                    obj.n = nd;
                    obj.coeff = obj.coeff(1:nd,:);
                end
            elseif isa(obj2,'double')
                if ~isscalar(obj2)
                    polynomial.utils.errorOp('+', class(obj), polynomial.utils.numclass(obj2));
                end
                obj.coeff(1,:,:) = obj.coeff(1,:,:) + obj2;
            elseif isa(obj,'double')
                if ~isscalar(obj)
                    polynomial.utils.errorOp('+', polynomial.utils.numclass(obj), class(obj2));
                end
                obj1 = obj;
                obj = obj2;
                obj.coeff(1,:,:) = obj2.coeff(1,:,:) + obj1;
            else
                polynomial.utils.errorOp('+', class(obj), class(obj2));
            end
        end

        function obj = uplus(obj)
            %+ - Unary plus operator for polynomial.Polynomial1 objects.
            %    uplus(obj) returns +obj1.coeff.

            % nothing to be done since +obj == obj
        end

        function obj = uminus(obj)
            %- - Unary minus operator for polynomial.Polynomial1 objects.
            %    uminus(obj) returns -obj

            obj.coeff = -obj.coeff;
        end

        function obj = minus(obj, obj2)
            %minus - Subtraction operator for polynomial.Polynomial1 objects.
            %    minus(obj, obj2) returns obj - obj2.

            if isa(obj,'polynomial.Polynomial1') && isa(obj2,'polynomial.Polynomial1')
                if size(obj.coeff,1) > size(obj2.coeff,1)
                    obj2.coeff(size(obj.coeff,1),:,:) = 0;
                elseif size(obj.coeff,1) < size(obj2.coeff,1)
                    obj.coeff(size(obj2.coeff,1),:,:) = 0;
                    obj.n = obj2.n;
                end
                obj.coeff = obj.coeff - obj2.coeff;
                nd = polynomial.utils.significantTerms(obj.coeff);
                if nd ~= obj.n
                    obj.n = nd;
                    obj.coeff = obj.coeff(1:nd, :);
                end
            elseif isa(obj2,'double')
                if ~isscalar(obj2)
                    polynomial.utils.errorOp('-', class(obj), polynomial.utils.numclass(obj2));
                end
                obj.coeff(1,:,:) = obj.coeff(1,:,:) - obj2;
            elseif isa(obj,'double')
                if ~isscalar(obj)
                    polynomial.utils.errorOp('-', polynomial.utils.numclass(obj), class(obj2));
                end
                obj1 = obj;
                obj = obj2;
                obj.coeff = -obj.coeff;
                obj.coeff(1,:,:) = obj1 + obj.coeff(1,:,:);
            else
                polynomial.utils.errorOp('-', class(obj), class(obj2));
            end
        end

        function obj = times(obj, obj2)
            obj = mtimes(obj, obj2);
        end

        function obj = mtimes(obj, obj2)
            %* - Product operator for polynomial.Polynomial1 objects.
            %    mtimes(obj, obj2) returns obj * obj2

            if isa(obj,'polynomial.Polynomial1') && isa(obj2,'polynomial.Polynomial1')
                n_ = size(obj.coeff,1);
                if obj2.n > 1 % this only applies to polynomials of degree >= 1
                    obj.coeff(size(obj.coeff,1)+size(obj2.coeff,1)-1,1,1)=0;
                    obj.n = obj.n + obj2.n - 1;
                end
                for i=1:obj.nequations
                    for j=1:obj.npieces
                        obj.coeff(:,i,j) = obj.basis{j}.product(obj.coeff(1:n_,i,j), obj2.coeff(:,i,j))';
                    end
                end
            elseif isa(obj2,'double')
                if ~isscalar(obj2)
                    polynomial.utils.errorOp('*', class(obj), polynomial.utils.numclass(obj2));
                end
                obj.coeff = obj2*obj.coeff;
                if obj2 == 0
                    obj.coeff = obj.coeff(1,:);
                end
            elseif isa(obj,'double')
                if ~isscalar(obj)
                    polynomial.utils.errorOp('*', polynomial.utils.numclass(obj), class(obj2));
                end
                obj1 = obj;
                obj = obj2;
                obj.coeff = obj1*obj.coeff;
                if obj1 == 0
                    obj.coeff = obj.coeff(1,:);
                end
            else
                obj = obj2 * obj;
            end
        end

        function obj = mrdivide(obj, obj2)
            %* - Division operator for polynomial.Polynomial1 objects.
            %    f = mrdivide(obj, obj2) returns obj/obj2.

            if isa(obj,'polynomial.Polynomial1') && isa(obj2,'double')
                if ~isscalar(obj2)
                    polynomial.utils.errorOp('/', class(obj), polynomial.utils.numclass(obj2));
                end
                obj.coeff = obj.coeff/obj2;
            else
                polynomial.utils.errorOp('/', class(obj), class(obj2));
            end
        end

        function obj = power(obj, order)
            obj = mpower(obj, order);
        end

        function obj = mpower(obj, order)
            %^ - Power operation for polynomial.Polynomial1 object.
            %    mpower(obj, order) returns the power obj^order.

            if order == 0
                obj.coeff = ones(1, obj.nequations, obj.npieces);
                return
            end
            if order == 1
                return
            end

            pow = obj;
            for k=2:order
                pow = obj*pow;
            end
            obj = pow;
        end

        function obj = cos(obj)
         obj.coeff = obj.basis{1}.interp1fp(@(x) cos(obj.polyval(x)));
         obj.n = length(obj.coeff);
        end

        function obj = cosh(obj)
         obj.coeff = obj.basis{1}.interp1fp(@(x) cosh(obj.polyval(x)));
         obj.n = length(obj.coeff);
        end

        function obj = sin(obj)
         obj.coeff = obj.basis{1}.interp1fp(@(x) sin(obj.polyval(x)));
         obj.n = length(obj.coeff);
        end

        function obj = sinh(obj)
         obj.coeff = obj.basis{1}.interp1fp(@(x) sinh(obj.polyval(x)));
         obj.n = length(obj.coeff);
        end

        function obj = sqrt(obj)
         obj.coeff = obj.basis{1}.interp1fp(@(x) sqrt(obj.polyval(x)));
         obj.n = length(obj.coeff);
        end

        function obj = exp(obj)
         obj.coeff = obj.basis{1}.interp1fp(@(x) exp(obj.polyval(x)));
         obj.n = length(obj.coeff);
        end

        function f = linspace(obj, n)
            %linspace - Linear space for polynomial.Polynomial1 object.
            %   f = linspace(obj, n) overwrites the matlab function with same name.
            %
            %inputs:
            %   obj = polynomial.Polynomial1 object
            %   n = number of points in the domain (integer).
            %
            %output:
            %   f = linspace in the obj.domain (double vector).

            if nargin == 1
                n = max(100, obj.steps);
            end

            f = linspace(obj.domain(1), obj.domain(2), n);
        end

        function plot(obj, varargin)
            %plot - Plot the function represented by a polynomial.Polynomial1 object.

            %todo: return graphics handle

            if ~isempty(varargin) && (isa(varargin{1}, 'polynomial.Polynomial1') || isa(varargin{1}, 'function_handle'))
                if obj.nequations > 1
                    error('polynomial.Polynomial1: plot if there are two polynomials the first should be scalar')
                end
                if length(varargin) > 1 && (isa(varargin{2}, 'polynomial.Polynomial1') || isa(varargin{2}, 'function_handle'))
                    fy = varargin{1};
                    fz = varargin{2};
                    varargin = varargin(3:end);
                    plot_dim = 3;
                else
                    fy = varargin{1};
                    varargin = varargin(2:end);
                    plot_dim = 2;
                end
            else
                plot_dim = 1;
            end

            if plot_dim < 3 && ishold() == false
                cla('reset')
                polynomial.utils.color('reset')
                hold('on');
                holdon = false;
            else
                holdon = true;
            end

            %colorargs = {};

            if plot_dim > 1
                xp = obj.polyval(obj.domain(1));
                if isa(fy, 'function_handle')
                    yp = fy(xp);
                else
                    yp = fy.polyval(xp);
                end
                if plot_dim == 3
                    if isa(fz, 'function_handle')
                        zp = fz(xp);
                    else
                        zp = fz.polyval(xp);
                    end
                end
            else
                xp = obj.domain(1);
                yp = obj.polyval(xp);
            end

            if ~obj.colorPieces
                compColor = cell(1, obj.nequations);
                for e = 1:obj.nequations
                    compColor{e} = polynomial.utils.color();
                end
            end

            for s = 1:obj.npieces
                np = max(2,floor(obj.steps/obj.npieces));  % # points
                x = linspace(obj.pieces(s), obj.pieces(s+1), np);
                for e = 1:obj.nequations
                    if plot_dim > 1
                        xx = [xp, obj.basis{s}.polyval(obj.coeff(:,e,s), x)];
                        if isa(fy, 'function_handle')
                            yy = [yp, fy(x)];
                        else
                            yy = [yp, fy.polyval(x)];
                        end
                        if plot_dim == 3
                            if isa(fz, 'function_handle')
                                zz = [zp, fz(x)];
                            else
                                zz = [zp, fz.polyval(x)];
                            end
                        end
                    else
                        xx = [xp, x];
                        yy = [yp(e), obj.basis{s}.polyval(obj.coeff(:,e,s), x)];
                    end
                    if obj.colorPieces
                        colorargs = {'color', polynomial.utils.color()};
                    else
                        colorargs = {'color', compColor{e}};
                    end
                    if plot_dim == 3
                        plot3(xx, yy, zz, colorargs{:}, varargin{:});
                        zp(e) = zz(end);
                    else
                       plot(xx, yy, colorargs{:}, varargin{:});
                    end
                    yp(e) = yy(end);
                end
                xp = xx(end);      % last x value (to ensure continuity in the plot)
            end

            xlim(obj.domain);

            if holdon == false
                hold off
            end
        end

        function f = polyval(obj, x, comps)
            % Evaluate the orthogonal polynomial at the selected points
            %  f = polyval(obj, x) returns the value of the polynomial associated with
            %      object obj at point x (where x is a numeric object)
            %
            %input (required):
            %   obj            = polynomial.Polynomial1 object)
            %   x              = numeric value where the polynomial will be evaluated.
            %
            %input (optional):
            %   comps          = components to be returned (array of indices)
            %
            %output:
            %   f              = polynomial values.
            %
            % The optional argument is only taken if the problem has more than one equation.
            % If the component is out of bounds a warning will be issued and the first
            % component will be returned.
            %
            % See also plot.

            % Case user decide about this order of variables.

            if obj.npieces == 1 && obj.nequations == 1
                f = obj.basis{1}.polyval(obj.coeff, x);
                return;
            end

            if nargin == 2
                comps = 1:obj.nequations;
            elseif min(comps) < 1 || max(comps) > obj.nequations
                warning('polynomial.Polynomial1.polyval: component out of bounds, sellecting the first');
                comps = 1:obj.nequations;
            end

            [m, n_] = size(x);
            % sort the sequence to allow all points from the same piece to be evaluated together
            [s, ind] = sort(x(:));
            [~, rind] = sort(ind); % rind is the reverse index to revert the sorting order

            f = zeros(m, n_, length(comps));
            for i=1:length(comps)
                k = comps(i);
                cnt = 1;                      % counter of values to be evaluated
                piece = 1;                    % counter of pieces
                results = zeros(1,length(s));
                while piece <= obj.npieces
                    cb = 0;
                    while cnt <= length(s) && (s(cnt) <= obj.pieces(piece+1) || piece == obj.npieces)
                        cb = cb + 1;
                        cnt = cnt + 1;
                    end
                    if cb ~= 0
                        subinterval = (cnt-cb):cnt-1;
                        results(subinterval) = obj.basis{piece}.polyval(obj.coeff(:,k, piece), s(subinterval));
                    end
                    if cnt == length(s) + 1
                        break;
                    end
                    piece = piece + 1;
                end

                f(:,:,i) = reshape(results(rind), m, n_);
            end
            if m == 1 && length(comps) ~= 1
                f = squeeze(f)';
            else
                f = squeeze(f);
            end
        end

        function V = polyvalm(obj, M)
            % obj.polyvalm - evaluate the orthogonal polynomial at matrix M
            %  V = polyvalm(obj, M) evaluate the orthogonal polynomial at matrix M
            %
            %input (required):
            %   obj            = polynomial.Polynomial1 object
            %   M              = matrix where the polynomial is evaluated
            %
            %output:
            %   V              = orthogonal polynomial at matrix M
            %
            % See also plot.
            V = obj.basis{1}.polyvalm(obj.coeff, M);
        end

        % display methods
        function disp(obj)
            %disp - Display the contents of the polynomial.Polynomial1 object.

            mat_size = size(obj.coeff);
            if length(size(obj.coeff)) == 2 %or if ndims(obj.coeff) == 2
                trueSize = sprintf('%dx%d', mat_size(1), mat_size(2));
            else
                trueSize = sprintf('%dx%dx%d', mat_size(1), mat_size(2), mat_size(3));
            end

            iname = inputname(1);
            if isempty(iname)
                iname = 'y';
            end

            if obj.nequations == 1
                str = sprintf('  %s object with properties:\n', class(obj));
            elseif obj.nequations == 2
                str = sprintf('  %s object, 2 components [%s{1},%s{2}], with properties:\n', ...
                              class(obj), iname, iname);
            elseif obj.nequations == 3
                str = sprintf('  %s object, 3 components [%s{1},%s{2},%s{3}], with properties:\n', ...
                              class(obj), iname, iname, iname);
            else
                str = sprintf('  %s object, %d components [%s{1},...,%s{%d}], with properties:\n', ...
                               class(obj), obj.nequations, iname, iname, obj.nequations);
            end

            str = [str, sprintf('          basis: \t %s\n', obj.basis{1}.name)];
            if obj.npieces == 1
                str = [str, sprintf('         degree: \t %d\n', obj.n-1)];
                str = [str, sprintf('         domain: \t [%g, %g]\n', ...
                       double(obj.domain(1)), double(obj.domain(2)))];
            else
                str = [str, sprintf('         domain: \t [%g, %g]\n', ...
                       double(obj.domain(1)), double(obj.domain(2)))];
                str = [str, sprintf('         degree: \t %d\n', obj.n-1)];
            end

            if obj.npieces ~= 1
                str = [str, sprintf('     nb. pieces: \t %d\n', obj.npieces)];
            end
            if obj.quadPrecision
                str = [str, sprintf('quad. precision: \t yes\n')];
            end
            str = [str, sprintf('          coeff: \t [%s %s]\n', ...
                   trueSize, class(obj.coeff))];
            fprintf(str);
        end
    end

    methods (Access = private)
        function lb = subinterval(obj, x)
            %subinterval - return the partition in obj where x is
            %
            %input:
            %   obj = polynomial.Polynomial1 object
            %   x   = value to find in the partition of obj
            %
            %output:
            %   lb  = partition number where x lies

            if x == obj.domain(1)
                lb = 1;
                return
            end

            lb = 1;
            ub = obj.npieces+1;

            while ub - lb > 1
                mp = floor((lb+ub)/2);
                if x >= obj.pieces(mp)
                    lb = mp;
                else
                    ub = mp;
                end
            end
        end
    end

    methods
        function d = get.degree(obj)
            d = obj.n -1;
        end

        function np = get.npieces(obj)
            np = length(obj.pieces) - 1;
        end

        function c = get.coef(obj)
            c = obj.coeff;
        end
        function pc = get.power_coeff(obj)
            pc = obj.basis{1}.orth2powmatrix(obj.n)*obj.coeff;
        end

        function val = coeff_n(obj, n)
            if obj.n > n
                val = [];
                return
            end

            val = obj.coeff;
            if obj.n < n
                val(n, obj.nequations, obj.npieces) = 0;
            end
        end
    end

    methods (Static = true)
        function obj = from_power_coeff(pc, options)
            %from_power_coeff - return a polynomial.Polynomial1 with built from the coefficients
            %     of a polynomial power series
            %
            %input(required):
            %  power_coeff = coefficients of the power series
            %
            %input(optional):
            %  options = polynomial.settings object
            %
            %output:
            %     obj  = polynomial.Polynomial1
            if nargin == 1
                options = polynomial.settings();
            end
            basis = polynomial.basis(options);
            obj = polynomial.Polynomial1(basis.pow2orthmatrix(size(pc, 1))*pc, options);
        end
    end
end
