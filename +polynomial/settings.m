classdef settings
%settings - Class to store the orthogonal polynomial settings
%   options = polynomial.settings('param1', value1, 'param2', value2, ...)
%             creates an object with the orthogonal polynomial settings.
%
%input parameters:
%   basis   = orthogonal polynomial basis
%            'ChebyshevT' (1st kind) (default), 'ChebyshevU' (2nd kind),
%            'ChebyshevV' (3rd kind), 'ChebyshevW' (4th kind),
%            'LegendreP', 'HermiteH', 'LaguerreL', 'GegenbauerC',
%            'BesselY' and 'PowerX'.
%
%   domain  = [a b] ([-1 1] default)
%
%output:
%   options = settings object (polynomial.settings object).
%
%examples:
%   options = polynomial.settings('basis', 'ChebyshevU', 'degree', 20);
%   % all the other options take the default values set for the session.
%
%See also polynomial.settings.session and polynomial.settings.default.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    properties
        % Orthogonal polynomial options
        basis          % Name of polynomial basis (and extra parameters)
        domain         % integration domain
        quadPrecision  % boolean that sets if quadprecision is used
        nequations     % number of equations to solve
        pieces         % partition used to solve the problem (if integer use evenly spaced intervals)
        firstpiece     % in case there is more than one partition is this the first?
        colorPieces    % should pieces be colored in plots?
        steps          % number of steps used to descretize an interval
    end

    properties (Dependent)
        npieces        % number of pieces (partitions)
    end

    methods
        function obj = settings(varargin)
            % deal with the command version first
            % possible alternatives are: session; default and reset
            if nargin == 0 || strcmp(varargin{1}, 'session')
                obj = polynomial.settings(polynomial.settings.session__());
                return
            end

            if strcmp(varargin{1}, 'defaults')
                obj = polynomial.settings(polynomial.settings.defaults__());
                return
            end

            if strcmp(varargin{1}, 'reset')
                obj = polynomial.settings.session(polynomial.settings.defaults());
                return
            end

            % copy constructor
            if isa(varargin{1}, 'polynomial.settings')
                obj = varargin{1};
                return
            end

            % build object from a structure
            if isstruct(varargin{1})
                for fn = intersect(fieldnames(varargin{1}), properties('polynomial.settings'))'
                    obj.(fn{1}) = varargin{1}.(fn{1});
                end
                for fn = setdiff(properties('polynomial.settings'),fieldnames(varargin{1}))'
                    obj.(fn{1}) = polynomial.settings.session(fn{1});
                end
                return
            end

            % build object from the arguments lists as parameters
            p = polynomial.settings.parser(polynomial.settings.session__());
            p.parse(varargin{:});

            for fn = fieldnames(p.Results)'
                obj.(fn{1}) = p.Results.(fn{1});
            end
        end

        function obj = set.basis(obj, value)
            if ischar(value) || isstring(value)
                basis__ =  value;
            elseif  iscell(value)
                basis__ = value{1};
            else
                basis__ = '';
            end
            if (polynomial.isBasis(basis__))
                obj.basis = value;
            elseif ~isempty(obj.basis)
                warning('polynomial.settings: basis "%s" unknown, no change done', value)
            else
                obj.basis = polynomial.settings.session__('basis');
                warning('polynomial.settings: basis "%s" unknown, using default value "%s"', value, obj.basis)
            end
        end

        function obj = set.domain(obj, value)
            if ~isnumeric(value) || ~isvector(value) || length(value) ~= 2
                error('polynomial.settings: wrong value for polynomial.settings domain')
            end
            obj.domain = value;
        end

        function obj = set.pieces(obj, value)
            if ~isnumeric(value) || ~isvector(value)
                warning('polynomial.settings: pieces must be either a scalar or a vector, setting default of 1 piece')
                value = 1;
            end

            % ensure that the pieces are a properly set (in increasing order)
            if ~isscalar(value) && any(sort(value) ~= value)
                warning('polynomial.settings: pieces are unsorted, sorting them')
                value = sort(value);
            end

            obj.pieces = value;
        end

        function obj = set.firstpiece(obj, value)
            if ~islogical(value)
                warning('polynomial.settings: firstpiece must be a boolean, setting it to true')
                obj.firstpiece = true;
            else
                obj.firstpiece = value;
            end
        end

        function obj = set.colorPieces(obj, value)
            if ~islogical(value) || ~isscalar(value)
                obj.colorPieces = false;
                warning('polynomial.settings: colorPieces must be a boolean, setting it to false')
            else
                obj.colorPieces = value;
            end
        end

        function np  = get.npieces(obj)
            if isscalar(obj.pieces)
                np = obj.pieces;
            else
                np = length(obj.pieces) - 1;
            end
        end

        function obj  = set.npieces(obj, ~)
            %obj.npieces = n;
            %obj.pieces = linspace();
        end

        function obj = set.quadPrecision(obj, value)
            if ~islogical(value) || ~isscalar(value)
                obj.quadPrecision = false;
                warning('polynomial.settings: quadPrecision must be a boolean, setting it to false')
            else
                obj.quadPrecision = value;
            end
        end

        function obj = set.nequations(obj, value)
            if value ~= floor(value) || value <= 0
                warning('polynomial.settings: nequations must be positive and integer, setting default value')
                obj.nequations = polynomial.settings.session__('nequations');
            else
                obj.nequations = value;
            end
        end

        function obj = set.steps(obj, value)
            if value ~= floor(value) || value <= 0
                warning('polynomial.settings: steps must be positive and integer, setting default value')
                obj.steps = polynomial.settings.session__('steps');
            else
                obj.steps = value;
            end
        end
    end

    methods
        function disp(obj)
            disp    ('Orthogonal basis/polynomial')
            if iscell(obj.basis)
                basis__ = obj.basis{1};
            else
                basis__ = obj.basis;
            end
            fprintf ('           basis:  %s\n', basis__)
            fprintf ('          domain:  %s\n', mat2str(obj.domain))
            fprintf ('   quadPrecision:  %s\n', mat2str(obj.quadPrecision))
            fprintf ('      nequations:  %d\n', obj.nequations)
            fprintf ('          pieces:  %s\n', mat2str(obj.pieces))
            fprintf ('      firstpiece:  %s\n', mat2str(obj.firstpiece))
            fprintf ('     colorPieces:  %s\n', mat2str(obj.colorPieces))
            fprintf ('           steps:  %d\n', obj.steps)
        end
    end

    methods (Static = true)
        function varargout = defaults()
        %polynomial.settings.defaults - Orthogonal default settings.
        %   polynomial.settings.defaults() gets the  default settings used with orthogonal polynomials.
        %
        %   -- options = polynomial.settings.defaults()
        %
        %output (optional):
        %   val = polynomial.settings object with the default values
            if nargout > 0
                varargout{1} = polynomial.settings('defaults');
                return
            end
            obj = polynomial.settings.defaults__();

            disp    ('polynomial.settings defaults')
            disp    ('============================')
            disp    ('The format for each setting is:')
            disp    ('      optionName:  [default], type, allowed values')
            disp    ('')

            disp    ('Orthogonal basis/polynomial:')
            fprintf ('           basis:  ["%s"], switch:{one of polynomial bases}\n', obj.basis)
            fprintf ('          domain:  [%s], interval\n', mat2str(obj.domain))
            fprintf ('   quadPrecision:  [%s]\n', mat2str(obj.quadPrecision))
            fprintf ('      nequations:  %d\n', obj.nequations)
            fprintf ('          pieces:  %s\n', mat2str(obj.pieces))
            fprintf ('      firstpiece:  [%s], boolean\n', mat2str(obj.firstpiece))
            fprintf ('     colorPieces:  [%s], boolean\n', mat2str(obj.colorPieces))
            fprintf ('           steps:  [%d], scalar, integer>0\n', obj.steps)
            disp    ('')

            disp    ('Interpolation (only for session values):')
            fprintf ('    interpRelTol:  [%g], scalar, >0\n', obj.interpRelTol)
            fprintf ('    interpMaxDim:  [%d], scalar, integer>0\n', obj.interpMaxDim)

        end

        function varargout = session(varargin)
        %polynomial.settings.session - Orthogonal polynomials session settings.
        %   polynomial.settings.session() gets the  default settings used with orthogonal polynomials
        %
        %   To read from the session settings
        %   -- options = polynomial.settings.session()
        %   -- value   = polynomial.settings.session('property')
        %
        %   To write in the session settings
        %   -- [opt] = polynomial.settings.session(options)
        %      options is a polynomial.settings object that is copied to the session settings
        %   -- [opt] = polynomial.settings.session('property_1', value_1, ...., 'property_n', value_n)
        %
        %   Options only available in session and not on class instances:
        %   -- interpRelTol   % maximum relative tolerance for function interpolation
        %   -- interpMaxDim   % maximum number of iterations used in function interpolation

        %
            sessionSettings = polynomial.settings.session__();
            if nargin == 1
                if ~isa(varargin{1}, 'polynomial.settings')
                    varargout{1} = polynomial.settings.session__(varargin{1});
                    return
                else
                    for fn = fieldnames(polynomial.settings.session__())'
                        polynomial.settings.session__(fn{1}, varargin{1}.(fn{1}));
                    end
                end
            elseif rem(nargin, 2) == 0
                for k = 1:2:nargin
                    sessionSettings.(varargin{k}) = varargin{k+1}; % validate value
                    polynomial.settings.session__(varargin{k}, sessionSettings.(varargin{k}));
                end
            else
                error('polynomial.settings.session: the number of arguments should either be 1 or even')
            end
            if nargout > 0
                varargout{1} = polynomial.settings('session');
            else
                disp    (polynomial.settings('session'))
                disp    ('')
                disp    ('Interpolation (only for session values):')
                fprintf ('    interpRelTol:  %g\n', sessionSettings.interpRelTol)
                fprintf ('    interpMaxDim:  %g\n', sessionSettings.interpMaxDim)
            end
        end

        function varargout = reset()
        %polynomial.settings.reset - reset orthogonal polynomials session settings to default values.
        %   polynomial.settings.reset() resets the session settings to the default values.
        %
        %   Similar to polynomial.settings('reset') but it

            polynomial.settings.session(polynomial.settings('defaults'));
            if nargout == 1
                varargout{1} = polynomial.settings();
            end
        end
    end

    methods (Static = true, Access = protected)
        function varargout = defaults__ (varargin)
        %polynomial.settings.defaults__ - orthogonal polynomials settings defaults.
        %   polynomial.settings.defaults__() gets the settings defaults used with orthogonal polynomials
        %
        %   -- options = polynomial.settings.defaults()
        %
        %output (optional):
        %   val = value of each property

            persistent defaultSettings;

            % defaultSettings is not defined so set all properties to the default values
            if isempty(defaultSettings)
                defaultSettings.basis = 'ChebyshevT';
                defaultSettings.domain = [-1, 1];
                defaultSettings.quadPrecision = false;
                defaultSettings.pieces = 1;
                defaultSettings.npieces = 1;
                defaultSettings.firstpiece = true;
                defaultSettings.colorPieces = true;
                defaultSettings.nequations = 1;
                defaultSettings.steps = 100;
                defaultSettings.interpRelTol = 100*eps;
                defaultSettings.interpMaxDim = 2^13;
            end
            varargout{1} = defaultSettings;
        end

        function varargout = session__(varargin)
            persistent sessionSettings;
            if isempty(sessionSettings)
                sessionSettings = polynomial.settings.defaults__();
            end

            if isempty(varargin)
                varargout{1} = sessionSettings;
                return
            elseif length(varargin) == 1
                varargout{1} = sessionSettings.(varargin{1});
                return
            elseif length(varargin) == 2
                sessionSettings.(varargin{1}) = varargin{2};
                varargout{1} = sessionSettings;
                return
            end
        end

        function p = parser(sessionSettings)
            p = inputParser ();

            p.KeepUnmatched = true;
            p.addParameter('basis',          sessionSettings.basis)
            p.addParameter('domain',         sessionSettings.domain)
            p.addParameter('quadPrecision',  sessionSettings.quadPrecision)
            p.addParameter('nequations',     sessionSettings.nequations)
            p.addParameter('pieces',         sessionSettings.pieces)
            p.addParameter('firstpiece',     sessionSettings.firstpiece)
            p.addParameter('colorPieces',    sessionSettings.colorPieces)
            p.addParameter('steps',          sessionSettings.steps)
        end
    end
end
