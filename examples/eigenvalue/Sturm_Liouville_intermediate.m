%% Sturm-Liouville Eigenvalue problem (LEP)
% Sturm-Liouville 4th order
%
% -y''''(x) + lambda*y(x) = 0
%
% y(0)=0, y(1)=0, y'(0)=0, y''(1)=0

% set the problem
equation = {@(x,y) diff(y,4), @(x,y) y};
domain = [0, 1];
conditions = @(y) {y(0); y(1); y(0)'; y(1)''};
options = tau.settings('degree', 48, 'kind', 'eig');
problem = tau.problem(equation, domain, conditions, options);

% solve the problem
[lambda, V, R] = tau.eig.LEP(problem, 'solver', 'qr');

% plot Tau residual associated with the smallest eigenvalue in magnitude
figure()
plot(R{1})

% algebraic eigenvalue problem relative error
k = 6; % first 6
algrelerror = zeros(k, 1);
T = tau.eig.matrix('T', problem);
for i=1:k
    algrelerror(i) =  norm(T{1}*V{i}.coeff+lambda(i)*T{2}*V{i}.coeff)./abs(lambda(i));
end

% compute the error for a known relation with lambda
error_fun = @(z) abs(tanh(z.^0.25)-tan(z.^0.25));
error = error_fun(lambda(1:k));

% titles = {'eigenvalue'; 'value'; 'alg. rel. error'; 'error'};
% Table = table((1:k)', lambda(1:k), algrelerror, error, 'VariableNames',titles)
fprintf('eigenvalue  value      r. error  error \n')
fprintf('\t %d \t %12.3f  %3.2e  %3.2e \n', [1:k; lambda(1:k)'; algrelerror'; error'])
