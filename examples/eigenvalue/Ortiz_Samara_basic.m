%% Ortiz Samara (QEP)
% Quadratic eigenvalue problem
%
% y''+(lambda+lambda^2*x^2)*y = 0
%
% y(-1)=0, y(1)=0

% set the problem
equation = '-diff(y, 2)=lambda*y+lambda^2*x^2*y';
domain = [-1 1];
conditions = {'y(-1)=0'; 'y(1)=0'};
options = tau.settings('degree', 10, 'kind', 'eig', 'neigs', 10);
problem = tau.problem(equation, domain, conditions, options);

% solve with quadeig
lambdaq = tau.eig.QEP(problem);

% solve with polyeig
lambdap = tau.eig.PEP(problem);

% compute characteristic polynomial roots (requires symbolic toolbox)
try
    M = tau.matrix('M', problem);
    N = tau.matrix('N', problem);
    C = tau.matrix('C', problem);
    nu = size(C, 1);
    n = size(M, 1);
    syms L
    D = N^2+L*eye(n)+L^2*M^2;
    n = problem.n;
    T = [C; D(1:n-nu, 1:n)];
    lambdad = roots(sym2poly(det(T)));
    [~,p] = sort(abs(lambdad));
    lambdad = lambdad(p);

    % plot results
    figure
    plot(lambdad, 'bo');
    hold on;
    plot(lambdaq, 'r+');
    plot(lambdap, 'gx');
    hold off;

    % errors
    k = 10; lamQEP = zeros(k, 1); lamPEP = lamQEP;
    for i=1:k
        lamQEP(i) = norm(lambdaq(i) - lambdad(i))/norm(lambdad(i));
        lamPEP(i) = norm(lambdap(i) - lambdad(i))/norm(lambdad(i));
        fprintf('%19.15f \t %5.2e \t %5.2e \n',lambdad(i),lamQEP(i),lamPEP(i));
    end
catch
    fprintf('symbolic toolbox or package not available, some results are not shown\n')
end
