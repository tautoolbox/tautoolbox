%% Sturm-Liouville Eigenvalue problem (LEP)
% Sturm-Liouville 4th order
%
% -y''''(x) + lambda*y(x) = 0
%
% y(0)=0, y(1)=0, y'(0)=0, y''(1)=0

% set the problem
equation = {@(x,y) diff(y,4), @(x,y) y};
domain = [0, 1];
conditions = @(y) {y(0); y(1); y(0)'; y(1)''};
options = tau.settings('basis', 'ChebyshevT', 'kind', 'eig');
problem = tau.problem(equation, domain, conditions, options);

n = problem.n;
nu = problem.nconditions;

% create matrices C, D and build T
C = tau.matrix('C', problem);
D = tau.matrix('D', problem);
T = cell(2, 1);
T{1} = [C; D{1}];
T{2} = [zeros(nu, size(D{2}, 2)); D{2}];

% solve the problem using Schur QR
S =  T{1}(1:nu, 1:nu)\T{1}(1:nu, nu+1:end);
A =  T{1}(nu+1:n, nu+1:end)-T{1}(nu+1:n, 1:nu)*S;
B =  T{2}(nu+1:n, nu+1:end)-T{2}(nu+1:n, 1:nu)*S;
[V, lambda] = eig(-A\B);
V = tau.utils.normalize([-S*V; V]);
lambda = 1./diag(lambda); [~,p] = sort(abs(lambda)); lambda = lambda(p);
V = V(:, p);

disp('first 6 eigenvalues:')
disp(lambda(1:6))
