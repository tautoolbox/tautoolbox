%% Coffey-Evans equation (LEP)
% Sturm-Liouville 2nd order
%
% -y''(x)+(beta*sin(2x))^2-2*beta*cos(2x)*y(x)=lambda*y(x),
%
% pi/2<x<pi/2, y(-pi/2)=y(pi/2)=0.

% set the problem
equation = '-diff(y, 2)+(30^2*sin(2*x)*sin(2*x)-2*30*cos(2*x))*y=lambda*y';
domain = [-pi/2 pi/2];
conditions = {'y(-pi/2)=0'; 'y(pi/2)=0'};
options = tau.settings('degree', 106, 'domain', domain, 'kind', 'eig');
problem = tau.problem(equation, domain, conditions, options);

n = problem.n;
nu = problem.nconditions;

% build blocks
N = tau.eig.matrix('diff', problem);
C = tau.eig.matrix('cond', problem);
beta = 30;
yn = tau.polynomial(@(x) beta^2*sin(2.*x).*sin(2.*x)-2*beta*cos(2.*x), options);
basis = polynomial.basis(tau.settings('degree', n-1, 'kind', 'eig'));
D = -N^2+basis.polyvalmM(yn.coeff, n);

% build terms
T = cell(2, 1);
T{1} = [C; D(1:n-nu, 1:n)]; % independent term
T{2} = [zeros(nu, n); -eye(n-nu, n)]; % term on lambda

[V, D] = eig(T{1}, -T{2}); lambda = diag(D);
[~, p] = sort(abs(lambda)); lambda = lambda(p);
lambda = lambda(1:end-nu);
V = V(:,p);
V = tau.utils.normalize(V);

% plot set of first eigenvalues
figure()
plot(lambda(1:30),'ko'); hold on;
plot(3,lambda(3), 'b+'), plot(4, lambda(4), 'mx'), plot(5, lambda(5), 'g*')
hold off;
xlabel('$k$', 'interpreter', 'latex')
ylabel('$\lambda_k$', 'interpreter', 'latex')
