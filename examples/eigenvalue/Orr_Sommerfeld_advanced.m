%% Orr-Sommerfeld (LEP)
% References:
% - Steven A. Orszaga, Accurate solution of the Orr-Sommerfeld stability
% equation, J. Fluid Mech. (1971), wol. 50, part 4, pp. 659-703.
% - F. Tisseur and N. J. Higham, Structured Pseudospectra for
% Polynomial Eigenvalue Problems, with Applications,
% SIAM J. Matrix Anal. Appl., 23(1):187-208, 2001.

% SCENARIO 1: largest eigenvalue: 0.23752649 + 0.00373967i (ORZAG)
% alpha = 1; R = 10000;
% SCENARIO 2: critical Reynolds number for instability of plane Poiseuille
% flow, lambda = 0.264002
alpha = 1.02056; R= 5772.22;

tic
% build the problem
domain = [-1 1];
equation = 'diff(y,4)=lambda*y';
conditions = {'y(-1)=0'; 'y(1)=0'; 'y''(-1)=0'; 'y''(1)=0'};
options = tau.settings('degree', 100, 'kind', 'eig', 'basis', 'ChebyshevT');
problem = tau.problem(equation, domain, conditions, options);

% Get building blocks: derivative operator
M = tau.matrix('M', problem);
N = tau.matrix('N', problem);
C = tau.matrix('C', problem);
[nu, n] = size(C);

% Coefficient matrices
M2 = M^2; M2 = M2(1:n,1:n);
N4 = N^4; N4 = N4(1:n,1:n);
N2 = N^2; N2 = N2(1:n,1:n);
D = N4+((-1i*R*alpha-2*alpha^2)*eye(n)+1i*R*alpha*M2)*N2+ ...
    (alpha^4-2*1i*R*alpha+1i*R*alpha^3)*eye(n)-1i*R*alpha^3*M2;
A0 = [C(:,1:n); D(1:n-nu,:)];           % independent term
A1 = zeros(n);
D1 = 1i*R*alpha*(N2-alpha^2*eye(n));
A1(nu+1:end,:) = D1(1:n-nu,:);          % term in lambda
t1 = toc;
fprintf('building time   : %g \n', t1)

% solve the problem
tic;
S =  A0(1:nu, 1:nu)\A0(1:nu, nu+1:end);
A =  A0(nu+1:n, nu+1:end)-A0(nu+1:n, 1:nu)*S;
B =  A1(nu+1:n, nu+1:end)-A1(nu+1:n, 1:nu)*S;
[V, lambda] = eig(-A\B); V = [-S*V; V];
    if is_octave() || ~verLessThan('matlab', '9.1')
        V = V./vecnorm(V);
    else
        V = V./repmat(vecnorm(V), size(V,1), 1);
    end
lambda = 1./diag(lambda); [~,p] = sort(abs(lambda)); lambda = lambda(p);
V = V(:, p);
t2 = toc;
fprintf('resolution time : %g \n', t2)
fprintf('total time      : %g \n', t1+t2)

%% plot
figure()
plot(-1i*alpha*lambda,'o'); axis([-1 0 -1 -0.2]);
xlabel('Re($\lambda$)', 'interpreter', 'latex')
ylabel('Imag($\lambda$)', 'interpreter', 'latex')
legend({'$\lambda=-i\alpha\omega$'}, 'interpreter', 'latex', ...
        'location', 'northwest')
