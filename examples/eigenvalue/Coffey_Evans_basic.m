%% Coffey-Evans equation (LEP)
% Sturm-Liouville 2nd order
%
% -y''(x)+(beta*sin(2x))^2-2*beta*cos(2x)*y(x)=lambda*y(x),
%
% pi/2<x<pi/2, y(-pi/2)=y(pi/2)=0.

% This problem has eigenvalues occurring in close triples, and as \beta
% increases, the clusters of eigenvalues become tighter

% set the problem
equation = '-diff(y, 2)+(30^2*sin(2*x)*sin(2*x)-2*30*cos(2*x))*y=lambda*y';
domain = [-pi/2 pi/2];
conditions = {'y(-pi/2)=0'; 'y(pi/2)=0'};
options = tau.settings('degree', 76, 'kind', 'eig');
problem = tau.problem(equation, domain, conditions, options);

% solve the problem
lambda = tau.eig.LEP(problem, 'solver', 'qz');

lambda(1:10)
