%% Sturm-Liouville Eigenvalue problem (LEP)
% Sturm-Liouville 4th order
%
% -y''''(x) + lambda*y(x) = 0
%
% y(0)=0, y(1)=0, y'(0)=0, y''(1)=0

% set the problem
equation = 'diff(y,4)=lambda*y';
domain = [0, 1];
conditions = {'y(0)=0'; 'y(1)=0'; 'y''(0)=0'; 'y''''(1)=0'};
problem = tau.problem(equation, domain, conditions);

% solve the problem
lambda = tau.solve(problem); % or lambda = tau.eig.LEP(problem);
disp('first 6 eigenvalues:')
disp(lambda(1:6))