%% Coffey-Evans equation (LEP)
% Sturm-Liouville 2nd order
%
% -y''(x)+(beta*sin(2x))^2-2*beta*cos(2x)*y(x)=lambda*y(x),
%
% pi/2<x<pi/2, y(-pi/2)=y(pi/2)=0.

% set the problem
beta = 30;
equation = {@(x,y) -diff(y, 2)+(beta^2*sin(2*x)*sin(2*x)-2*beta*cos(2*x))*y, @(x,y) y};
domain = [-pi/2 pi/2];
conditions = @(y) {y(-pi/2); y(pi/2)};

options = tau.settings('degree', 76, 'kind', 'eig');
problem = tau.problem(equation, domain, conditions, options);

% solve the problem
[lambda, V, R] = tau.eig.LEP(problem, 'solver', 'qz');

% plot set of first eigenvalues
figure()
plot(lambda(1:30),'ko'); hold on;
plot(3,lambda(3), 'b+'), plot(4, lambda(4), 'mx'), plot(5, lambda(5), 'g*')
hold off;
xlabel('$k$', 'interpreter', 'latex')
ylabel('$\lambda_k$', 'interpreter', 'latex')

% plot eigenfunctions 3 to 5 (first cluster)
figure(), hold on;
plot(V{3},'color','b','linestyle', '-');
plot(V{4},'color','g','linestyle',':');
plot(V{5},'color','m','linestyle','-.');
legend('$v_3$','$v_4$','$v_5$','interpreter','latex')
hold off;
