%% Ortiz Samara (QEP)
% Quadratic eigenvalue problem
%
% y''(x)+(lambda+lambda^2*x^2)*y(x) = 0
%
% y(-1)=0, y(1)=0

% set the problem
equation = {@(x,y) -diff(y, 2), @(lambda) lambda,    @(x,y) y, ...
                                @(lambda) lambda.^2, @(x,y) x^2*y};
domain = [-1 1];
conditions = @(y) {y(-1); y(1)};
problem = tau.problem(equation, domain, conditions);

% solve with quadeig
[lambdaq, ~, Rq] = tau.eig.QEP(problem);

% solve with polyeig
[lambdap, ~, Rp] = tau.eig.PEP(problem);

% show a few eigenvalues
disp('  quadeig   polyeig')
disp([lambdaq(1:20), lambdap(1:20)])

% plot tau residuals
figure()
hold on
plot(Rq{1})
plot(Rq{2})
hold off
title('tau residual: quadeig'); legend('$\lambda_1$','$\lambda_2$', 'interpreter', 'latex')
figure()
hold on
plot(Rp{1})
plot(Rp{2})
hold off
title('tau residual: polyeig'); legend('$\lambda_1$','$\lambda_2$', 'interpreter', 'latex')
