%% Loaded string (REP converted to GEP and PEP)
% Boundary value problem describing the eigenvibration of a string
% with a load of mass $m$ attached by an elastic spring of stiffness $k$,
%
% -y''(x) = lambda*y(x), 0<x<1
%
% -y'(1) = phi(lambda)*y(1), y(0)=0
%
% where
%
% phi(lambda) = sigma*m*lambda/(lambda-sigma),
% lambda in (sigma,infty), sigma = k/m,
% k and m given positive numbers (default m = k = 1).
%
% This REP is converted to GEP (or PEP):
%
% y(0)=0, -lambda*y'(1)-lambda*y(1)+y'(1)=0
%
% Reference:
% S. I. Solov"ev. Preconditioned iterative methods for a class of nonlinear
% eigenvalue problems. Linear Algebra & Applications, 415 (2006), pp. 210-229.

% set the problem
equation = {@(x,y) -diff(y, 2), @(x,y) y};
domain = [0 1];
conditions = { @(y) y(0); ...
              {@(y) - y(1)', @(lambda) lambda, @(y) -y(1)'-y(1)}};
options = tau.settings('degree', 41, 'kind', 'eig');
problem = tau.problem(equation, domain, conditions, options);

% independent terms
C = tau.matrix('C', problem);
D = tau.matrix('D', problem);
A0 = [C{1}; D{1}];
A1 = [C{2}; D{2}];

% solve the using nleigs
% ... polynomial matrices
BN = {};
% ... nonlinear matrices
CN = cell(1,2);
CN{1} = A0; CN{2} = A1;
% ... nonlinear functions
fN = cell(1,2);
fN{1} = @(x) 1; fN{2} = @(x) x;
% ... nlep structure
A = struct('B', {BN}, 'C', {CN}, 'f', {fN});
% ... parameters and options
sigma = [60, 70];
Xi = 1.;
opts = struct('maxit', 150, 'leja', 2, 'tol', 1e-8, 'isfunm', false, ...
              'v0', ones(problem.n, 1));
% ... compute solution
lambdan = nleigs(A, sigma, Xi, opts);
[~, p] = sort(abs(lambdan)); lambdan = lambdan(p);

% solve generelized eigenvalue problem
lambdag = eig(CN{1}, -CN{2});
[~,p] = sort(abs(lambdag));
lambdag = lambdag(p);
% remove eigenvalue 1 if it exists
p = abs(lambdag-1)>eps; lambdag = lambdag(p);
lambdag = lambdag(lambdag>sigma(1) & lambdag<sigma(2));

% solve polynomial eigenvalue problem
lambdap = polyeig(CN{1}, CN{2});
[~,p] = sort(abs(lambdap));
lambdap = lambdap(p);
% remove eigenvalue 1 if it exists
p = abs(lambdap-1)>eps; lambdap = lambdap(p);
lambdap = lambdap(lambdap>sigma(1) & lambdap<sigma(2));

% show (third) eigenvalue between sigma
format long
disp('polyn. eigenproblem   gen. eigenproblem   nonlinear eigenproblem')
disp([lambdag lambdap lambdan]);
