%% Loaded string (REP)
% Boundary value problem describing the eigenvibration of a string
% with a load of mass $m$ attached by an elastic spring of stiffness $k$,
%
% -y''(x) = lambda*y(x), 0<x<1
%
% -y'(1) = phi(lambda)*y(1), y(0)=0
%
% where 
%
% phi(lambda) = sigma*m*lambda/(lambda-sigma),
% lambda in (sigma,infty), sigma = k/m, 
% k and m given positive numbers (default m = k = 1).
%
% Reference:
% S. I. Solov"ev. Preconditioned iterative methods for a class of nonlinear
% eigenvalue problems. Linear Algebra & Applications, 415 (2006), pp. 210-229.

% set the problem
equation = {@(x,y) -diff(y, 2), @(lambda) lambda, @(x,y) y};
domain = [0 1];
conditions = {{@(y) -y(1)', @(lambda) lambda/(lambda-1), @(y) -y(1)}; ...
               @(y) -y(0)};
options = tau.settings('degree', 101, 'kind', 'eig');
problem = tau.problem(equation, domain, conditions, options);

% independent terms
C = tau.matrix('C', problem); [nu, n] = size(C{1});
D = tau.matrix('D', problem);

% solve with nleigs external library
% ... polynomial matrices
BN = {};
% ... nonlinear matrices and functions
CN{1} = [C{1}; D{1}]; fN{1} = @(x) 1;                 % lambda independent terms 
CN{2} = [zeros(nu, n); D{2}]; fN{2} = @(x) x;         % lambda dependent terms
CN{3} = [C{2}; zeros(n-nu, n)]; fN{3} = @(x) x/(x-1); % lambda/(lambda-1) dependent terms
% ... nlep structure
A = struct('B', {BN}, 'C', {CN}, 'f', {fN});
% ... parameters and options
sigma = [60, 70];
Xi = 1.;
opts = struct('maxit', 150, 'leja', 2, 'tol', 1e-8, 'isfunm', false, 'v0', ones(n, 1));
% ... compute solution
[X, lambdan] = nleigs(A, sigma, Xi, opts);
[~, p] = sort(abs(lambdan));
lambdan = lambdan(p);

% show (third) eigenvalue and compute residual
G = @(z) CN{1} + z*CN{2} + z/(z-1)*CN{3};
k = 1; v = X(:,k);
res(k) = norm(G(lambdan(k))*v)/norm(G(lambdan(k)), 'fro');

disp('eig                      residual');
fprintf('%15.14e \t %6.2e \n', lambdan(k), res(k))  