% Solving a nonlinear Fredholm integro-differential equation:
% y' + y - fred(y^2) = 1/2*(e^(-2)-1)
% y(0) = 1
% Exact solution: y(x) = exp(-x)

equation = {@(x,y,yo) diff(y)+y-2*fred(y,@(x,t) yo(t)), ...
            @(x,yo) -integral(yo^2, 0, 1)+(exp(-2)-1)/2};
domain = [0, 1];
condition = @(y) y(0) - 1;
problem = tau.problem(equation, domain, condition);

yn = tau.solve(problem);

x = linspace(yn); %#ok<LTARG>
figure()
plot(x, abs(yn(x) - exp(-x)))
title ('Absolute error')

