% Solving a nonlinear Fredholm integro-differential equation:
% y'-fred(x^3*y^2) = 1-1/3*x^3
% y(0) = 0
% Exact solution: y(x) = x

equation = {@(x,y,yo) y' - 2*fred(y, @(x,t) x^3*yo(t)), ...
            @(x,yo) -x^3*integral(yo^2, 0, 1)+1-x^3/3};
domain = [0, 1];
condition = @(y) y(0);
problem = tau.problem(equation, domain, condition);

yn = tau.solve(problem);

x = linspace(domain(1), domain(2));
figure()
plot(x, abs(yn(x) -x))
title('Absolute error')

