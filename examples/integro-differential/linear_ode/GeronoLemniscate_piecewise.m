% A system of differential equations with multipoint conditions
% Weisstein, Eric W. "Eight Curve." From MathWorld--A Wolfram Web Resource. https://mathworld.wolfram.com/EightCurve.html

ode = {@(x,y) y{1}'' + y{1}; ...
       @(x,y) y{2}'' + 4*y{2}};
domain = [-pi, pi];
conditions = @(y) {y{1}(-pi)+1;
                   y{1}(-pi/2);
                   y{2}(-pi/2);
                   y{2}(-pi/4)+1/2};
options = tau.settings('degree', 21, 'pieces', 2);

problem = tau.problem(ode, domain, conditions, options);
yn = tau.solve(problem);

t=linspace(yn);
y1=yn{1}(t);
y2=yn{2}(t);

figure()
subplot(2,1,1)
semilogy(t,abs(cos(t)-y1),t,abs(sin(t).*cos(t)-y2), 'LineWidth', 1.2)
grid on;
title('Absolute error')
xlabel('$t$', 'interpreter','latex')
ylabel('$|y_n-y\{n\}|$', 'interpreter','latex')
legend('$n=1$','$n=2$', 'interpreter','latex','Location','Best')

subplot(2,1,2)
plot(y1,y2, 'LineWidth', 2);
title('Tau approximation')
xlabel('$y_1(t)$', 'interpreter','latex')
ylabel('$y_2(t)$', 'interpreter','latex')
xlim([-1,1])
ylim([-.5,.5])
