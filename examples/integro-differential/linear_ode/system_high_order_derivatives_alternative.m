% Example of a system of differential equations with order higher than the first
% in all the equations

ode = {@(x,y) y{1}'' + y{2}'' + y{1}  + y{2};
       @(x,y) y{1}'' - y{2}'' - y{1}' - y{2}'};
domain = [0, 2*pi];
conditions = {@(y) y{1}(0);         % y1(0)  = 0
              @(y) y{2}(0) - 1;     % y2(0)  = 1
              @(y) diff(y,1,0) - 1; % y1'(0) = 1
              @(y) diff(y,2,0)};    % y2'(0) = 0

options = tau.settings('basis', 'LegendreP', 'degree', 30);
problem = tau.problem(ode, domain, conditions, options);

[yn, info, residual, tauresidual, Cauchy_error] = tau.solve(problem);
solution = tau.polynomial({@sin; @cos}, problem.options);

figure()
subplot(2,2,1)
plot(yn-solution);
title('Absolute error')

subplot(2,2,2)
plot(residual)
title('Residual error')

subplot(2,2,3)
plot(tauresidual)
title('Tau error')

subplot(2,2,4)
plot(Cauchy_error)
title('Cauchy error')

