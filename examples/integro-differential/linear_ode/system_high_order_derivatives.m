% Example of a system of differential equations with order higher than the first
% in all the equations

ode = {@(x,y) y{1}'' + y{2}'' + y{1}  + y{2};
       @(x,y) y{1}'' - y{2}'' - y{1}' - y{2}'};
domain = [0, 2*pi];
conditions = @(y) { y{1}(0);           % y1(0)  = 0
                    y{2}(0) - 1;       % y2(0)  = 1
                    diff(y{1},0) - 1;  % y1'(0) = 1
                    diff(y{2},0)};     % y2'(0) = 0

options = tau.settings('basis', 'LegendreP', 'degree', 20);
problem = tau.problem(ode, domain, conditions, options);
yn= tau.solve(problem);

% The solution of this problem is known and allow us to assess the quality
% of the approximation
solution = tau.polynomial({@sin; @cos}, problem.options);

figure()
x = linspace(yn, 600);
plot(x, abs(yn(x)-solution(x)), 'LineWidth', 2);
title('Absolute error')

