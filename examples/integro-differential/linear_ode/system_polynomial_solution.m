% Example of a system of differential equations with polynomial solution

odeSystem = {@(x,y) y{2}-diff(y{1}), @(x) 0; ...
             @(x,y) y{3}-diff(y{2}), @(x) 0; ...
             @(x,y) (x^2+1)*diff(y{3})-(x^2+3*x)*diff(y{2})+5*x*y{2}-5*y{1}, @(x) 60*x^2-10};
domain = [-1, 1];
conditions = @(y) {y{1}(-1)-4; ...
                   y{2}(1)-2; ...
                   y{3}(0)};
options = tau.settings('degree', 15);

problem = tau.problem(odeSystem, domain, conditions, options);
[yn, info, residual, tau_residual, cauchy_error] = tau.solve(problem);

ye = tau.polynomial({@(x) x.^5-3*x+2; @(x) 5*x.^4-3; @(x) 20*x.^3}, options);

figure()
subplot(2,2,1)
plot(yn-ye);
title('Absolute error')

subplot(2,2,2)
plot(residual)
title('Residual error')

subplot(2,2,3)
plot(tau_residual)
title('Tau error')

subplot(2,2,4)
plot(cauchy_error)
title('Cauchy error')

