%% Example of a stiff problem (signal function)
%
% $$\lambda \frac{d^2}{dx^2}y + 2x\frac{d}{dx}y = 0 $$
%
% $$ y(-1)=erf \left(-\frac{1}{\sqrt{\lambda}}\right),$$
%
% $$ y(1) =erf \left(\frac{1}{\sqrt{\lambda}}\right) $$
%
% Specify parameter, problem and conditions
lambda = 1e-8;
ode = @(x,y) lambda*y'' + 2*x*y';

conditions = @(y) { y(-1) - erf(-1/sqrt(lambda));
                    y( 1) - erf( 1/sqrt(lambda))};

options = tau.settings('basis', 'LegendreP', 'degree', 501, 'pieces', 16);
problem = tau.problem(ode, [-1, 1], conditions, options);

% Solve the problem.
yn = tau.solve(problem);
figure();
plot(yn);
title('Approximate solution')

% Compare the results with the exact solution
solution = @(x) erf(x/sqrt(lambda));
figure();
xx = linspace(yn); 
semilogy(xx, abs(yn(xx)-solution(xx)));
title('Absolute Error')

