%% Example of a stiff problem (signal function)
%
% $$\lambda\cosh(x)\frac{d^2}{dx^2}y+2x\exp(x)\frac{d}{dx}y = \frac{2x(\exp(2x)-1)}{\exp\left({\frac{x(\lambda+x)}{\lambda}}\right)\sqrt{\lambda\pi}} $$
%
% $$ y(-1)=erf \left(-\frac{1}{\sqrt{\lambda}}\right),$$
%
% $$ y(1) =erf \left(\frac{1}{\sqrt{\lambda}}\right) $$
%

% Specify parameter, problem, domain and conditions.
lambda = 10^-8;
ode = { @(x,y) lambda*cosh(x)*diff(y, 2) + 2*x*exp(x)*diff(y, 1), ...
        @(x) 2*x*(exp(2*x)-1)/exp(x*(lambda+x)/lambda)/sqrt(lambda*pi)};
domain = [-1, 1];
conditions = @(y) { y(-1) - erf(-1/sqrt(lambda));
                    y( 1) - erf( 1/sqrt(lambda))};

options = tau.settings('basis', 'ChebyshevT', 'degree', 500, 'pieces', 16);
problem = tau.problem(ode, domain, conditions, options);

% Solve the problem
yn = tau.solve(problem);
figure()
plot(yn)

% Compare the results with the exact solution
exact_solution = @(x) erf(x/sqrt(lambda));

figure()
xx = linspace(yn);
semilogy(xx, abs(yn(xx)-exact_solution(xx)))
title('Absolute error')
