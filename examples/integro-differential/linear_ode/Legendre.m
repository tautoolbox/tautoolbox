% Example of a differential equation solved by the Tau method using Schur complements

% Problem and conditions.
a = 0.4;
equation = @(x,y) (a^4 - 4*a^3*x + 4*a^2*x^2 + 2*a^2 - 4*a*x + 1)* y'' -a*(-2*a*x+a^2+1)*y'-2*a^2*y;
domain = [-1, 1];
conditions = @(y) {y(-1) - 1/(1+a); y(1) - 1/(1-a)};
options = tau.settings('basis', 'ChebyshevT', 'degree', 50);
problem = tau.problem(equation, domain, conditions, options);

% Approximate solution using the Schur Tautoolbox solver
yn = tau.ode.Schur.solve(problem, 'precision', eps);

% Evaluate the true error since the solution is known
figure()
x = linspace(yn);
semilogy(x, abs(yn(x)-(1-2*a*x+a^2).^(-0.5)));
title('Absolute error')
