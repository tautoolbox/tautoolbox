% Two linear systems of ode to obtain the butterfly curve
% Barile, Margherita and Weisstein, Eric W. "Butterfly Curve." From MathWorld--A Wolfram Web Resource.
% https://mathworld.wolfram.com/ButterflyCurve.html

ode_u =  {@(t,u) u{1}' - u{2} + sin(t)*u{1};
          @(t,u) u{2}' + u{1} + cos(t)*u{1}};
cond_u =  @(u) {u{1}(0); u{2}(0)-exp(1)};

ode_v =  {@(t,v) v{1}'' - 2*v{2}' + 15*v{1};
          @(t,v) v{2}'' + 2*v{1}' + 15*v{2}};

cond_v =  @(v) {v{1}(0); diff(v{1},0)-v{2}(0);
              v{2}(0)+2; diff(v{2},0)};

options = tau.settings('basis', 'LegendreP', 'degree', 20, 'pieces', 5);
domain = [0, 2*pi];

prob_u = tau.problem(ode_u, domain, cond_u, options);
[u, info_u, residual_u, tauresidual_u, cauchyerror_u] = tau.solve(prob_u);

prob_v = tau.problem(ode_v, domain, cond_v, options);
[v, info_v, residual_v, tauresidual_v, cauchyerror_v] = tau.solve(prob_v);

t=linspace(u, 600);
x=u{1}(t)+v{1}(t);
y=u{2}(t)+v{2}(t);

u1=sin(t).*exp(cos(t)); v1=-2*sin(t).*cos(4*t);
u2=cos(t).*exp(cos(t)); v2=-2*cos(t).*cos(4*t);

figure()
subplot(1,2,1)
plot(x,y,'LineWidth',2);
title('TauToolbox solution')
subplot(1,2,2)
plot(u1+v1,u2+v2,'LineWidth', 2);
title('Exact solution')

figure()

subplot(2,2,1)
plot(t,abs(u1-u{1}(t)),t,abs(u2-u{2}(t)), 'LineWidth', 2);
title('Absolute error of the first system')

subplot(2,2,2)
plot(residual_u, 'LineWidth', 2);
title('Residual error of the first system')

subplot(2,2,3)
plot(tauresidual_u, 'LineWidth', 2);
title('Tau residual of the first system')

subplot(2,2,4)
plot(cauchyerror_u, 'LineWidth', 2);
title('Cauchy error of the first system')

figure()

subplot(2,2,1)
plot(t,abs(v1-v{1}(t)),t,abs(v2-v{2}(t)), 'LineWidth', 2);
title('Absolute error of the second system')

subplot(2,2,2)
plot(residual_v, 'LineWidth', 2);
title('Residual error of the second system')

subplot(2,2,3)
plot(tauresidual_v, 'LineWidth', 2);
title('Tau residual of the second system')

subplot(2,2,4)
plot(cauchyerror_v, 'LineWidth', 2);
title('Cauchy error of the second system')

figure()
semilogy(t,abs(u1+v1-x),t,abs(u2+v2-y), 'LineWidth', 2);
title('Absolute error of the solution (u+v)')
