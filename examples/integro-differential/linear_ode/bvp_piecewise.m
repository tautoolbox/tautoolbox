% Example of a differential equation with conditions
% inside the integration domain solved in 5 pieces

equation = @(x,y) y''+y;
domain = [0, 2*pi];
conditions = @(y) {y(pi/3)-0.5; diff(y, pi/2) + 1};
options = tau.settings('basis', 'LegendreP', 'degree', 20, 'pieces', 5);

problem = tau.problem(equation, domain, conditions, options);
[yn, info, residual, tauresidual] = tau.solve(problem);

disp(info)

figure();
plot(yn);
xlabel('$x$', 'Interpreter', 'Latex')
ylabel('$y_{20}(x)$', 'Interpreter', 'Latex')
title('Approximate solution')

figure();
xlabel('$x$', 'Interpreter', 'Latex')
plot(residual)
title('Residual')

figure();
u = linspace(yn);
% compare result with the exact solution
semilogy(u, abs(yn(u)-cos(u)));
xlabel('$x$', 'Interpreter', 'Latex')
ylabel('$|\cos(x)-y_{20}(x)|$', 'Interpreter', 'Latex')
title('Absolute error')

figure()
plot(tauresidual)
xlabel('$x$', 'Interpreter', 'Latex')
ylabel('$\tau(x)$', 'Interpreter', 'Latex')
title('Tau residual')
