% Example of a stiff problem solved by the Tau method using Schur complements

% Problem and conditions.
ode = @(x,y) diff(y,10)+cosh(x)*diff(y,8)+x^2*diff(y,6)+x^4*diff(y,4)+cos(x)*diff(y,2)+x^2*y;
domain = [-1, 1];
conditions = @(y) { y(-1);            % = 0
                    y( 1);            % = 0
                    diff(y,1,-1) - 1; % = 0
                    diff(y,1, 1) - 1; % = 0
                    diff(y,2,-1);     % = 0
                    diff(y,2, 1);     % = 0
                    diff(y,3,-1);     % = 0
                    diff(y,3, 1);     % = 0
                    diff(y,4,-1);     % = 0
                    diff(y,4, 1)};    % = 0

options = tau.settings('basis', 'ChebyshevT', 'degree', 25);
problem = tau.problem(ode, domain, conditions, options);

% Approximate solution.
yn = tau.ode.Schur.solve(problem, 'initialLU', 12, 'precision', 1e-12, 'maxDim', 500);

figure()
plot(yn)
