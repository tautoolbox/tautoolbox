% Example of a differential equation with conditions given at the boundary
equation = @(x,y) y'' + y;
domain = [0, 2*pi];
conditions = @(y) {y(0)-1; diff(y, 2*pi)};
options = tau.settings('degree', 20, 'basis', 'ChebyshevU');
problem = tau.problem(equation, domain, conditions, options);

% solution via tau method
[yn, info, residual, tauresidual, Cauchy_error] = tau.solve(problem);

figure()
plot(yn)
title('Approximate solution')

figure();
u = linspace(yn);
% compare result with the exact solution
subplot(2,2,1)
semilogy(u, abs(yn(u)-cos(u)), 'LineWidth', 1.6);
title('Absolute error')

% plot residual
subplot(2,2,2)
plot(residual)
title('Residual')

% plot tau residual
subplot(2,2,3)
plot(tauresidual)
title('Tau residual')

% plot tau residual
subplot(2,2,4)
plot(Cauchy_error)
title('Cauchy error')

