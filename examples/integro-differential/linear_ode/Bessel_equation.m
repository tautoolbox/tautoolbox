% Approximating Bessel functions Jn in a long domain

domain = [0, 10^3];
options = tau.settings('basis', 'ChebyshevT', 'pieces', 100);

% To approximate J0
nu=0;
equation = @(x,y) x^2*y'' + x*y' + (x^2-nu^2)*y;
conditions = @(y) {y(0)-1; diff(y, 0)}; % initial values for J0
problem = tau.problem(equation, domain, conditions, options);

% numerical solution by tau-toolbox
yn = tau.solve(problem);

u=linspace(yn, 5000);

figure()
% absolute error
semilogy(u, abs(yn(u)-besselj(nu,u)), 'Linewidth', 1.2);

% To approximate J1
nu=1;
equation = @(x,y) x^2*y'' + x*y' + (x^2-nu^2)*y;
conditions = @(y) {y(0); diff(y, 0)-1/2}; % initial values for J1
problem = tau.problem(equation, domain, conditions, options);

% numerical solution by using Tau's method
yn = tau.solve(problem);

% Compare numerical solution with the exact solution
u=linspace(yn, 5000);

hold on
semilogy(u, abs(yn(u)-besselj(nu,u)), 'Linewidth', 1.2);
title('Absolute Errors')
legend('J_0','J_1','Location','Best')
xlabel('x','interpreter','latex')
ylabel('$|y_n(x)-J_\nu(x)|$','interpreter','latex')
grid on
hold off
