% Example of a stiff problem

% problem specification
ode = @(x,y) 1e-7*y'' + (cos(x)-0.8)*(-2*x*y'+y);
domain = [-1, 1];
conditions = @(y) {y(-1)-1;
                   y(1)-1};
options = tau.settings('basis', 'LegendreP', 'degree', 100, 'pieces', 50);
problem = tau.problem(ode, domain, conditions, options);

% tau solution
yn = tau.solve(problem);

figure()
plot(yn);
