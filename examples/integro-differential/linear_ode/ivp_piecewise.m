% Example of a differential equation with initial value conditions solved in 5 pieces

equation = @(x,y) y'' + y;
domain = [0, 2*pi];
conditions = @(y) {y(0) - 1;
                   diff(y,0)};
options = tau.settings('degree', 20, 'basis', 'LegendreP', 'pieces', [0, 3, 4, 5, 2*pi]);
problem = tau.problem(equation, domain, conditions, options);

% solution via tau method
[yn, info] = tau.solve(problem);
figure()
plot(yn)
title('Approximate solution')

% compare result with the exact solution
figure();
u = linspace(yn); 
semilogy(u, abs(yn(u)-cos(u)));
title('Absolute error')
