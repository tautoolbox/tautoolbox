% Example of a nonlinear differential equation solved in 5 pieces
% Solving the problem:
% diff(y) -y^2 = 0, y(0) = 1/2, y(x) = 1/(2-x).

ode = {@(x,y,yo) y' - 2*yo*y, @(x,yo) -yo^2};
domain = [0, 1];
condition = @(y) y(0) - 1/2;
options = tau.settings('basis', 'LegendreP', 'degree', 20, 'pieces', 5);
problem = tau.problem(ode, domain, condition, options);

yn = tau.ode.nonlinear.solve(problem, 'tol', 1e-14);

figure()
plot(yn-tau.polynomial(@(x) 1./(2-x), problem.options))
