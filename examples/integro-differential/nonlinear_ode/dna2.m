% Example of 10 nonlinear system of equations
equation = {...
    @(x,y,yo) y{1}'-y{2}+yo{4}*y{3}+yo{3}*y{4}, @(x,yo) yo{3}*yo{4};
    @(x,y,yo) y{2}'-y{3}, @(x,yo) 0;
    @(x,y,yo) y{3}'-y{4}, @(x,yo) 0;
    @(x,y,yo) y{4}'-y{5}, @(x,yo) 0;
    @(x,y,yo) y{5}'-y{6}, @(x,yo) 0;
    @(x,y,yo) y{6}'-y{7}, @(x,yo) 0;
    @(x,y,yo) y{7}'-y{8}, @(x,yo) 0;
    @(x,y,yo) y{8}'-y{9}, @(x,yo) 0;
    @(x,y,yo) y{9}'-y{10}, @(x,yo) 0;
    @(x,y,yo) y{10}'-y{1}, @(x,yo) 0};
domain = [0, 1];

conditions = { ...
    @(y) y{1}(0) - rand(1);
    @(y) y{2}(0) - rand(1);
    @(y) y{3}(0) - rand(1);
    @(y) y{4}(0) - rand(1);
    @(y) y{5}(0) - rand(1);
    @(y) y{6}(0) - rand(1);
    @(y) y{7}(0) - rand(1);
    @(y) y{8}(0) - rand(1);
    @(y) y{9}(0) - rand(1);
    @(y) y{10}(0) - rand(1)};
options = tau.settings('degree', 17, 'basis', 'LegendreP');
problem = tau.problem(equation, domain, conditions, options);

yn = solve(problem);
plot(yn)
