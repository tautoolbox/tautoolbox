% Solving the Lorenz's system of nonlinear differential equations.
% y1' - sigma(y2 - y1) = 0
% y2' - rho*y1 + y1*y3 + y2 = 0
% y3' - y1*y2 + beta*y3 = 0

sigma = 10;
rho   = 28;
beta  = 8/3;

% Specify problem, conditions and exact solution.
ode = {@(x,y,yo) y{1}' - sigma*(y{2}-y{1}), @(x,yo) 0; ...
       @(x,y,yo) y{2}' - rho*y{1} + yo{3}*y{1} + yo{1}*y{3} + y{2}, @(x,yo) yo{1}*yo{3}; ...
       @(x,y,yo) y{3}' + beta*y{3} - yo{2}*y{1} - yo{1}*y{2} + y{2}, @(x,yo) -yo{1}*yo{2}};
domain     = [0, 100];
conditions = @(y) { y{1}(0) + 8;           % y1(0)  = -8
                    y{2}(0) + 7;           % y2(0)  = -7
                    y{3}(0) - 29};         % y3(0)  = 29
options    = tau.settings('degree', 50, 'basis', 'LegendreP', 'pieces', 200);

problem = tau.problem(ode, domain, conditions, options);
%yn= tau.solve(problem, 'solver', 'nonlinear', 'tol', 1e-12);
yn= tau.solve(problem);
plot(yn)

