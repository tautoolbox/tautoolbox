% Example of a nonlinear differential equation (stiff problem)

% Specify problem, conditions and exact solution.
ode        = {@(x,y,yo) y' + (3*yo^2 - 2*yo)*y, @(x,yo) 2*yo^3 - yo^2};
domain     = [0, 1400];
conditions = @(y) y(0) - 1/700;
options    = tau.settings('degree', 20, 'basis', 'LegendreP', 'pieces', 200);

problem = tau.problem(ode, domain, conditions, options);

% Compute the approximate solution.
yn = tau.ode.solve(problem);

figure()
plot(yn)
title('Approximate solution')

try
    if is_octave()
        pkg load specfun
    end
catch
    fprintf('lambertw function not defined\n')
    fprintf('For Octave users notice that you need to install and load the specfun package\n')
    return
end

solution = @(x) 1./(lambertw(0,((1/700)^(-1)-1)*exp(((1/700)^(-1)-1)-x))+1);
figure()
xx = linspace(yn); %#ok<LTARG>
semilogy(xx, abs(yn(xx) - solution(xx)));
title('Absolute error')

