% Example of a nonlinear system of differential

% Specify problem, conditions and options to be used to get an approximated solution.
ode = { @(x,y,yo) y{1}' + 40*y{1} - 0.16*yo{3}*y{1} - 40*y{2} - 0.16*yo{1}*y{3}, ...
        @(x,yo) -0.16*yo{1}*yo{3}; ...
        @(x,y,yo) -55*y{1} + yo{3}*y{1} + y{2}' - 20*y{2} + yo{1}*y{3}, ...
        @(x,yo) yo{1}*yo{3}; ...
        @(x,y,yo) -yo{2}*y{1} + 0.65*2*yo{1}*y{1} - yo{1}*y{2}+y{3}' - 1.833*y{3}, ...
        @(x,yo) -yo{1}*yo{2} + 0.65*yo{1}^2};
conditions = {@(y) y{1}(0)+1; @(y) y{2}(0)-2; @(y) y{3}(0)-3};
options = tau.settings('basis', 'LegendreP', 'degree', 10, 'pieces', 160);

% Compute the approximate solution.
problem = tau.problem(ode, [0, 10], conditions, options); 
yn = solve(problem);

plot(yn)

x = linspace (yn, 2400);
X = yn(x);
plot3(X(1,:), X(2,:), X(3,:))
