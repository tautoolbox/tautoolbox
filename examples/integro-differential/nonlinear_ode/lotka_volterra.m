% Solving the Lokta Volterra's system of nonlinear differential equations.
% y1' - alpha*y1 + beta*y1*y2 = 0
% y2' + gamma*y2 - delta*y1*y2 = 0

% Parameters, domain, step and initial conditions.
alpha = 2;
beta  = 1;
gamma = 1.7;
delta = 0.5;

ode = { @(x,y,yo) y{1}' - alpha*y{1} + beta*yo{1}*y{2}  + beta*yo{2}*y{1}, ...
        @(x,yo) beta*yo{1}*yo{2}; ...
        @(x,y,yo) y{2}' + gamma*y{2} - delta*yo{1}*y{2}  - delta*yo{2}*y{1}, ...
        @(x,yo) -delta*yo{1}*yo{2}};
conditions = @(y) {y{1}(0)-3;  ... % y1(0)=3
                   y{2}(0)-1};     % y2(0)=1

domain = [0, 100];
options = tau.settings('degree', 50, 'pieces', 200);

% Compute the approximate solution.
problem = tau.problem(ode, domain, conditions, options); 
yn = solve(problem);

plot(yn{:})
