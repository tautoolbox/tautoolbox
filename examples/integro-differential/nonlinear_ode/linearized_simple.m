% Example of a nonlinear differential equation
% Solving the problem:
% y' = 1/(1+y^2) <=> (1+y^2) y' = 1, y(0) = 0.

ode = {@(x,y,yo) (1+yo^2)*y' + 2*yo*yo'*y, @(x,yo) 1+2*yo^2*yo'};
domain = [-1, 1];
condition = @(y) y(0);
options = tau.settings('degree', 60);
problem = tau.problem(ode, domain, condition, options);

yn = tau.solve(problem);

figure()

% In this particular case it is possible to get an analytical solution
sol = @(x) -((sqrt(9*x^2+4)+3*x)^(2/3)*(2^(1/3)*sqrt(9*x^2+4)-3*2^(1/3)*x)-2^(5/3)*(sqrt(9*x^2+4)+3*x)^(1/3))/4;
plot(yn-tau.polynomial(sol, problem.options))
