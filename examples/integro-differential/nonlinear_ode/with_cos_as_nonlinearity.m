% Example of a nonlinear differential equation
% Solving the problem:
% diff(y) + cos(y*x^2) = cos(x) + cos(sin(x)*x^2), y(0) = 0, y(x) = sin(x).

% Specify the linearized problem and conditions.
ode = {@(x,y,yo) y' - x^2 * sin(x^2*yo)*y, ...
       @(x,yo) cos(x)+cos(x^2*sin(x))-cos(x^2*yo)-x^2*yo*sin(x^2*yo)};
domain = [0 2];
conditions = @(y) y(0);  % y(0) = 0
options = tau.settings('degree', 20, 'basis', 'ChebyshevT');
problem = tau.problem(ode, domain, conditions);

% Approximate solution.
yn = tau.solve(problem);

figure()
plot(yn)

figure()
plot(yn-tau.polynomial(@sin, problem.options))
