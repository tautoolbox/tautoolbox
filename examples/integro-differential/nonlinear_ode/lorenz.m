% Example of a nonlinear system of differential solved in 90 pieces

% Specify problem, conditions and exact solution.
ode = { @(x,y,yo) y{1}' - 10*y{2}  +  10*y{1}, ...
        @(x,yo) 0; ...
        @(x,y,yo) y{2}' - 28*y{1}  + yo{3}*y{1} + yo{1}*y{3} + y{2}, ...
        @(x,yo) yo{1}*yo{3}; ...
        @(x,y,yo) y{3}' + 8/3*y{3}  - yo{2}*y{1} - yo{1}*y{2}, ...
        @(x,yo) -yo{1}*yo{2} };
conditions = @(y) {y{1}(0)+8;  ... % y1(0)=-8
                   y{2}(0)+7;  ... % y2(0)=-7
                   y{3}(0)-16};    % y3(0)=16
options = tau.settings('basis', 'LegendreP', 'degree', 10, 'pieces', 90);

% Compute the approximate solution.
problem = tau.problem(ode, [0, 30], conditions, options); 
yn = solve(problem);

x = linspace (yn, 2400);
X = yn(x);
plot3(X(1,:), X(2,:), X(3,:))
