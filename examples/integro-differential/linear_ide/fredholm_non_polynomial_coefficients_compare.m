% Equation:
%   exp(x)*y'' + cos(x)*y' + sin(x)*y + fred(y, @(x,t) exp((x+1)*t)) =
%   = (cos(x)+sin(x)+exp(x))*exp(x) + 2*sinh(x+2)/(x+2)
%
% Boundary conditions:
%   y(1)+y(-1)=exp(1)+exp(-1)
%   y(1)+y(-1)-y'(-1)=exp(1)
%
% Solution:
%   y(x) = exp(x)

equation = {@(x,y) exp(x)*diff(y,2)+cos(x)*diff(y)+sin(x)*y+fred(y,@(x,t) exp((x+1)*t)), ...
            @(x) (cos(x)+sin(x)+exp(x))*exp(x)+2*sinh(x+2)/(x+2)};
domain   = [-1, 1];
conditions = @(y) {y(1)+y(-1)-exp(1)-exp(-1); ...
                   y(1)+y(-1)-diff(y,-1)-exp(1)};

figure()
for i = 5:3:20
    options = tau.settings('degree', i, 'basis', 'ChebyshevU');
    problem = tau.problem(equation, domain, conditions, options);
    yn = tau.solve(problem);

    xx = linspace(yn); %#ok<LTARG>
    yy = yn(xx);
    semilogy(xx, abs(yy - exp(xx)));

    xr = yn.domain(1) + (yn.domain(2)-yn.domain(1))*rand(1);
    text(xr, abs(exp(xr)-yn(xr)), [' n = ' ...
            num2str(i)], 'Backgroundcolor', [1, 1, 1], 'Edgecolor', 'k')
    hold on;
end
