% Example of a integro-differential equation
equation = {@(x,y) -y'' + x*y' + y - fred(y, @(x,t) x^2*t^3) + volt(y, @(x,t) x), ...
            @(x) 1/8*x^5-3/2*x^3-123/140*x^2+5*x-1};
domain = [0, 1];
conditions = @(y) {y(0)+1;
                   y(1)+1/2};

options = tau.settings('basis', 'LegendreP', 'degree', 20);
problem = tau.problem(equation, domain, conditions, options);

% Approximate solution.
yn = tau.solve(problem);

figure()
plot(yn)
