% Example of an integral system of equations
ode = { @(x,y) y{1}-volt(y{1}, @(x,t) sin(x-t)-1) - volt(y{2},@(x,t) 1-t*cos(x)), ...
        @(x) -0.5*(x-2)*sin(x)-x*cos(x)^2+(sin(x)+2)*cos(x)-1; ...
        @(x,y) y{2}-volt(y{1})-volt(y{2},@(x,t) x-t), ...
        @(x) -x + sin(x)};

domain = [0, 1];
conditions = [];
options = tau.settings('basis', 'ChebyshevT', 'degree', 15);
problem = tau.problem(ode, domain, conditions, options);
yn = tau.solve(problem);

figure()
solution = tau.polynomial({@cos; @sin}, problem.options);
plot(yn-solution);
title('Error')
legend('cos', 'sin')
