% Example of a integro-differential equation
options = tau.settings('basis', 'ChebyshevT', 'degree', 20);

equation = {@(x,y) cos(x^4)*y - diff(y) + volt(y, @(x,t) x*(t+1)), ...
            @(x) cos(x^4)*(x-1)/(x+1)-2/(x+1)^2+x^2*(x/2-1) };
domain = [0,1];
condition = @(y) y(0)+1;
problem = tau.problem(equation, domain, condition, options);

% Approximate solution.
yn = tau.solve(problem);

% exact solution: (x-1)/(x+1)
xx = linspace(yn); 
yy = yn(xx);

figure()
semilogy(xx, abs(yy-(xx-1)./(xx+1)))
title('Absolute error')
