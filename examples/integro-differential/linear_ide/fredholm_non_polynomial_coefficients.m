% Equation:
%   exp(x)*y'' + cos(x)*y' + sin(x)*y + fred(y, @(x,t) exp((x+1)*t)) =
%   = (cos(x)+sin(x)+exp(x))*exp(x) + 2*sinh(x+2)/(x+2)
%
% Boundary conditions:
%   y(1)+y(-1)=exp(1)+exp(-1)
%   y(1)+y(-1)-y'(-1)=exp(1)
%
% Solution:
%   y(x) = exp(x)

equation = {@(x,y) exp(x)*diff(y,2)+cos(x)*diff(y)+sin(x)*y+fred(y,@(x,t) exp((x+1).*t)), ...
            @(x) (cos(x)+sin(x)+exp(x)).*exp(x)+2*sinh(x+2)./(x+2)};

conditions = @(y) {y(1)+y(-1)-exp(1)-exp(-1);
                   y(1)+y(-1)-diff(y,-1)-exp(1)};

options = tau.settings('basis', 'ChebyshevT', 'degree', 20);
problem = tau.problem(equation, [-1, 1], conditions, options);

% Solve the integro-differential problem.
yn = tau.solve(problem);

% Compare Tautoolbox's approximation with the exact solution:
figure();
xx = linspace(yn); %#ok<LTARG>
semilogy(xx, abs(yn(xx) - exp(xx)));
xlabel('x');
ylabel('Absolute error');
