% Example of an integro-differential equation
% Equation:
%   y' - y - fred(y, @(x,t) 1/(x+exp(t))) =
%   = -log((x+exp(1))/(x+1))
%
% Boundary condition:
%   y(0)=1
%
% Solution:
%   y(x) = exp(x)

ode = {@(x,y) diff(y) - y - fred(y, @(x,t) 1/(x + exp(t))), ...
       @(x) -log((x+exp(1))/(x+1))};
condition = @(y) y(0) - 1;
options = tau.settings('basis', 'ChebyshevU', 'degree', 20);
problem = tau.problem(ode, [0, 1], condition, options);

% Approximate solution.
yn = tau.solve(problem);

% Compare Tautoolbox approximation with the exact solution
figure()
x = linspace(yn); %#ok<LTARG>
semilogy(x, abs(exp(x)-yn(x)))
