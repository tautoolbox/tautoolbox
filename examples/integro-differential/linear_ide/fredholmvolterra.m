% Example of a integro-differential system of equations
equation = {@(x,y) diff(y{1}) + fred(y{1}, @(x,t) exp(x+t)) + x*y{2} - (x+x^2)*volt(y{2}, @(x,t) sin(x-t)), ...
            @(x) cos(x)+exp(x)+x*(cos(x)-exp(x))-(x^2+x)*(cos(x)/2-exp(x)/2+sin(x)/2+(x*sin(x))/2)+(exp(1)*exp(x)*(exp(1)-cos(1)+sin(1)))/2;
            @(x,y) x^2*y{1} - x*volt(y{1}, @(x,t) 2*x+sin(t))-3*diff(y{2})-fred(y{2}, @(x,t) x-t), ...
            @(x) cos(1)+sin(1)+3*exp(x)+3*sin(x)-x*(x/2-2*x*(cos(x)-1)+2*x*(exp(x)-1)-(cos(x)*sin(x))/2-(exp(x)*(cos(x)-sin(x)))/2+1/2)-x*sin(1)+x^2*(exp(x)+sin(x))+x*(exp(1)-1)-2};

domain = [0, 1];
conditions = @(y) { y{1}(0)-1;
                    y{2}(0)};
options = tau.settings('degree', 20, 'basis', 'ChebyshevT');
problem = tau.problem(equation, domain, conditions, options);

% Approximate solution.
yn = tau.solve(problem);

figure()
plot(yn);
