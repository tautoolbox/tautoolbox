%% Wikipedia integro-differential equation example
% $$ u'(x)+2u(x)+5\int_0^t u(t) dt = 1, u(0)=0 $$

% tau solution
equation = {@(x,y) y' + 2*y + 5*volt(y), @(x) 1};
domain = [0,5];
condition = @(y) y(0);
options = tau.settings('basis', 'ChebyshevT', 'degree', 20, 'pieces', 3);
problem = tau.problem(equation, domain, condition, options);
yn = tau.solve(problem);

exact_solution = @(x) 0.5*exp(-x).*sin(2*x);
u = linspace(yn);

figure()
semilogy(u, abs(yn(u)-exact_solution(u)));
title('Absolute error')
