% Solve example 1 from paper "A MATRIX FORMULATION ..." 2002 ICNAAM
equation = {@(x,y) y' + y - volt(y, @(x,t) x*(1+2*x)*exp(t*(x-t))), @(x) 2*x+1};
condition = @(y) y(0)-1;
options = tau.settings('basis', 'ChebyshevT', 'degree', 20);
problem = tau.problem(equation, [0, 1], condition, options);

yn = tau.solve(problem);

figure()
points = linspace(yn); %#ok<LTARG>
semilogy(points, abs(yn(points)-exp(points.^2)))
title('Absolute error')
