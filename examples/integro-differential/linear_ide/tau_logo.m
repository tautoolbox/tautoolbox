%% Tau logo
% Wikipedia integro-differential equation
% $$ u'(x)+2u(x)+5\int_0^t u(t) dt = 1, u(0)=1 $$

equation = {@(x,u) u' + 2*u + 5*volt(u), @(x) 1};
domain = [0 5];
condition = @(u) u(0) - 1;
problem = tau.problem(equation, domain, condition);
u = tau.solve(problem);

figure()
plot(u)
