% Example of a integral system of equation
ode = {@(x,y) y{1}-volt(y{1},@(x,t) (x-t)^3) - volt(y{2},@(x,t) (x-t)^2), ...
       @(x) -1/3*x^4-1/3*x^3+x^2+1; ...
       @(x,y) y{2}-volt(y{1},@(x,t) (x-t)^4) - volt(y{2},@(x,t) (x-t)^3), ...
       @(x) -1/420*x^7-1/4*x^5-1/4*x^4-x^3+x+1};
domain = [0, 1];
conditions = [];
options = tau.settings('basis', 'LegendreP', 'degree', 8);
problem = tau.problem(ode, domain, conditions, options);

yn = tau.solve(problem);
solution = tau.polynomial({@(x) 1+x^2;@(x) 1+x-x^3}, problem.options);

figure()
plot(yn-solution);
