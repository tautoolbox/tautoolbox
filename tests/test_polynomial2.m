function test_suite=test_polynomial2()
    try % assignment of 'localfunctions' is necessary in Matlab >= 2016
        test_functions=localfunctions();
    catch % no problem; early Matlab versions can use initTestSuite fine
    end
    initTestSuite;
end

function test_polynomial2_constructor()
    % default argument -> x+y
    fx = tau.polynomial2();
    assertEqual(fx.coeff, [0 1; 1 0])

    % construct from a function
    fx = tau.polynomial2(@(x,y) x^2);
    f1 = tau.polynomial(@(x) x^2);
    assertEqual(fx.coeff, f1.coeff)

    % construct from a function
    fx = tau.polynomial2(@(x,y) x*y);
    assertEqual(fx.coeff, [0 0; 0 1])

    % construct from a tau.polynomial
    f1 = tau.polynomial();
    fx = tau.polynomial2(f1);
    assertEqual(fx.coeff, f1.coeff);
    fy = tau.polynomial2(f1, 2);
    assertEqual(fx.coeff, f1.coeff');
end

