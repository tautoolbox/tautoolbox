n = 50;
options = tau.settings('pieces',1,'basis','ChebyshevT','degree',n);

operator1 = @(x,y) y' - 3*x^2*y;
a = 0; b = 2;
domain1 = [a b];
condition1 = @(y) y(0) - 1;

problem1 = tau.problem(operator1, domain1, condition1, options);
[yn,yinfo,yres,ycauchy]=tau.solve(problem1);

%operator2 = ['2/(' num2str(b-a) ')*y''-3*((', num2str(b-a),' *x+', num2str(b+a) ,')/2)^2*y=0'];
operator2 = @(x,y) 2/(b-a)*y'-3*((b-a)*x+(b+a))^2/4*y;
domain2 = [-1 1];
condition2 = @(y) y(-1) - 1;

problem2 = tau.problem(operator2, domain2, condition2, options);
[zn, zinfo,zres,ztaures]=tau.solve(problem2);

t1 = linspace(a,b);
t2 = linspace(-1,1);

norm(polyval(yn,t1)-zn(t2))
figure(1);
plot(yn); e1=norm(abs(yn(t1)-exp(t1.^3)))
figure(2);
plot(zn); e2=norm(abs(zn(t2)-exp(t1.^3)))
