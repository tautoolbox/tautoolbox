tau.settings.session('steps', 600, 'interpRelTol', eps);

ye2 = tau.polynomial(@exp, tau.settings('pieces', 2));
ye = tau.polynomial(@exp);
y1 = tau.solve(tau.problem(@(x,y) y'-y, [-1,1], @(y) y(0)-1));
y2 = tau.solve(tau.problem(@(x,y) y'-y, [-1,1], @(y) y(0)-1, tau.settings('pieces', 2)));
y2p = tau.ode.linear.piecewise.equation(tau.problem(@(x,y) y'-y, [-1,1], @(y) y(0)-1, tau.settings('pieces', 2)));

xx = linspace(-1,1,tau.settings.session('steps'));
plot(xx, exp(xx)-y1(xx), xx, exp(xx)-y2(xx))
hold on
plot(xx, 0*xx)
plot(ye-y1)
plot(ye2-y2)
plot(ye2-y2p)
