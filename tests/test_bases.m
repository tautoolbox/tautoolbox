function test_suite=test_bases()
    try % assignment of 'localfunctions' is necessary in Matlab >= 2016
        test_functions=localfunctions();
    catch % no problem; early Matlab versions can use initTestSuite fine
    end
    initTestSuite;
end

function test_bases_basis_factory()
    assertTrue(iscellstr(polynomial.basis('all')))
    assertTrue(iscellstr(polynomial.basis('supported')))
    assertTrue(iscellstr(polynomial.basis('experimental')))

    % assertError(polynomial.basis('xx'))
    % assertError(polynomial.basis({1}))

    assertTrue(isa(polynomial.basis(), 'polynomial.bases.T3Basis'))
    assertTrue(isa(polynomial.basis(tau.settings()), 'polynomial.bases.T3Basis'))

    bases = polynomial.basis('all');
    for k=1:length(bases)
        options = tau.settings('basis', bases{k});
        assertTrue(isa(polynomial.basis(options), 'polynomial.bases.T3Basis'))
        assertTrue(isa(polynomial.basis(options), ['polynomial.' bases{k}]))
    end
end

function test_bases_O_inverse_N()
    n = 100; % degree

    bases = polynomial.basis('all');

    for k=1:length(bases)
        options = tau.settings('basis', bases{k});
        b = polynomial.basis(options);
        N = b.matrixN(n+1); % matrix of size (n+1,n+1)
        O = b.matrixO(n+1); % matrix of size (n+1,n+1)
        NO = N*O;
        ON = O*N;
        assertElementsAlmostEqual(eye(n),NO(1:n,1:n))
        assertElementsAlmostEqual(eye(n),ON(2:n+1,2:n+1))
    end
end

function test_eval()
    % Create basis with the intended options:
    n = 11;  % degree  10
    options = tau.settings('basis', 'ChebyshevT', 'domain', [1 2]);
    basis = polynomial.basis(options);

    % Evaluate a vector vec in a linear combination sum(a_iP_i(x)):
    vec = 0:0.001:1;
    assertEqual(size(vec), size (basis.polyval([1 3 2 4 3 2 1], vec)))

    % Evaluate the value of the basis polynomials at the point given:
    val = 0.5;
    assertEqual(length(basis.polyvalb(val, n)), n)

    % evaluate matrix polynomial operation
    A = [1 2 3;1 4 2;1 2 3];
    assertEqual(size(A), size(basis.polyvalm([1 2 3 4 3 2 1], A)))

    % Evaluate basis on the elements of matrix A:
    A = [1 2 3;3 2 1;1 2 3];
    res = basis.polyval([0 0 1], A);
    assertEqual(res(1,:), res(3,:));
    assertEqual(res(1,:), diag(res)');
end
