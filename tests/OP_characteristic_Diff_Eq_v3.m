% TEST characteristic differential equation
% Using the characteristic differential equation of orth. pol.
% g_2(x)y_k''+g_1(x)y_k'(x)+a_ky_k=0 (Abramowitz & Stegun, 22.1.3 and 22.6)
% check if:
% 1) y_k=P_k, with n=k+1, is recovered
% 2) g_2(M)N^2+g_1(M)N+diag(a_k)=0 equation is verified

%% parameters
poly_name = {'ChebyshevT'; 'ChebyshevU'; 'LegendreP'; 'LaguerreL'; 'HermiteH'; 'BesselY'};
g1 = ['(1-x^2)'; '(1-x^2)'; '(1-x^2)'; 'x      '; '1      '; 'x^2    '];
g2 = ['-x     '; '-3*x   '; '-2*x   '; '(1-x)  '; '-2*x   '; '(2*x+2)'];
ak = ['k^2     '; 'k*(k+2) '; 'k*(k+1) '; 'k       '; '2*k     '; '-k*(k+1)'];
x0 = ['y(-1)='; 'y(-1)='; 'y(-1)='; 'y(0)= '; 'y(0)= '; 'y(0)= '];
y0 = ['(-1)^k      '; '(k+1)*(-1)^k'; '(-1)^k      ';'1           ';...
    'v           '; '1           '];
x1 = ['y(1)= '; 'y(1)= '; 'y(1)= '; 'y''(0)='; 'y''(0)='; 'y''(0)='];
y1 = ['1        '; 'k+1      '; '1        ';...
      '-k       '; 'dv       '; 'k*(k+1)/2'];
G_MN = ['(eye(n)-M^2)*N^2-M*N+diag((0:n-1).^2)        ';
        '(eye(n)-M^2)*N^2-3*M*N+diag((0:n-1).*(2:n+1))';
        'N^2-M*(M*N)*N-2*M*N+diag((0:n-1).*(1:n))     ';
        'M*N^2+(eye(n)-M)*N+diag(0:n-1)               ';
        'N^2-2*M*N+2*diag(0:n-1)                      ';
        'M^2*N^2+2*(M+eye(n))*N-diag((0:n-1).*(1:n))  '];
degree = [3; 4; 50; 90; 420; 421]; % list for values of k
last_comp = zeros(length(degree),1);
first_comps = last_comp;
matrix_error = last_comp;
prec_family = [1e-14; 1e-14; 5e-10; 1e-12; 1e-10; 1e-5; 1e-5]; % problems with Laguerre and Legendre prec
check_type = 1; % 1 for detailed report

options = tau.settings();
%% tests
for m=1:size(poly_name,1)
    prec = prec_family(m);
    options.basis=poly_name{m};
    %fprintf([op_basis ' tests'])
    for j=1:length(degree)
        k = degree(j);
        n = k+1;
        options.degree = k;
        x = tau.polynomial(options);
        y = tau.operator(options);
        M = tau.matrix('M', options);
        N = tau.matrix('N', options);
        % problem
        eq = [deblank(g1(m,:)),'*diff(y,2)+',deblank(g2(m,:)),'*diff(y)+(',...
            num2str(eval(ak(m,:))),'*y) = 0'];
        v=((-1)^k+1)*(-2)^ceil(k/2)*prod(3:2:2*ceil(k/2)-1)/2;
        dv=((-1)^k-1)*(-2)^ceil(k/2)*prod(3:2:2*ceil(k/2)-1)/2;
        cond = {[deblank(x0(m,:)), num2str(eval(y0(m,:)))];
            [deblank(x1(m,:)), num2str(eval(y1(m,:)))]};
        G=eval(G_MN(m,:));
        % approximate Tau solution
        problem = tau.problem(eq, [-1, 1], cond, options);
        a = tau.solve(problem);

        % test approx. solution components
        last_comp(j) = max(a.coeff);
%        assert(abs(max(a.coeff)-1)<prec,...
%        '\n last component test failed: %s degree %d', poly_name(m, :), k)
        first_comps(j) = min(a.coeff);
%        assert(abs(min(a.coeff))<prec,...
%        '\n all but the last component test failed: %s degree %d', poly_name(m, :), k)

        % test operational matrices generation
        matrix_error(j) = norm(G);
%        assert(matrix_error(j)<prec,...
%            '\n matrix norm test failed: %s degree %d ', poly_name(m, :), k)
    end

    % report
    if (abs(sum(last_comp)-length(degree))<prec && ...
        abs(sum(first_comps))<prec && ...
        abs(sum(matrix_error))<prec)
        fprintf(': OK \n')
    end
    if (check_type)
        poly_name{m}
        prettyprint(table(degree, last_comp, first_comps, matrix_error))
    end
end
