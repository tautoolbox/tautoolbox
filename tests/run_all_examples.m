function run_all_examples()
% The purpose of this code is to run all code examples to ensure that they run

    % suppress annoying and useless warnings regarding zero values in a log plot
    if is_octave()
        neglogaxis = warning('query', 'Octave:negative-data-log-axis');
        warning('off', 'Octave:negative-data-log-axis');
        warning('off', 'Octave:legend:unimplemented-location');
        warning('off', 'OctSymPy:sym:rationalapprox');
    end

    examples = tau.example();
    for k =1:length(examples)
        fprintf('%d: %s\n', k, examples{k})
        tau.example(examples{k}, 'run')
    end

    % restore previous warning state
    if is_octave()
        warning(neglogaxis.state, 'Octave:negative-data-log-axis');
    end
end
