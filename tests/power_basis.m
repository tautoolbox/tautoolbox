x  = tau.polynomial();
options = tau.settings('basis', 'PowerX', 'degree', 4);

y = tau.operator(options); % y is a tau.operator object (unknown function)
a = diff(y, 2); % 2nd derivative of object y
b = int(y, 1);  % primitive of object y
b.mat*[3; -5; 1; 0; 0]; % primitive x^2-5x+3 --> x^3/3-5/2x^2+3x
problem = tau.problem({@(x,y) int(y), @(x) x.^3/3-5/2*x.^2+3*x}, [-1 1], @(y) y(-1)-9, options);
yn = tau.solve(problem);

