%
% - Test. This .m is a script.

options = tau.settings('basis', 'LegendreP', 'degree', 30);
domain = [1, 5];

%% y(x) = sin(x)
equation = {@(x,y) y'' + y};
cond     =  @(y) {y(1) - sin(1);
                 diff(y, 5) - cos(5)};
problem = tau.problem(equation, domain, cond, options);
a = tau.ode.Schur.solve(problem, 'precision', eps);
basis = polynomial.basis(options); % this can be reused below since we are using the same basis
basis.mad(a, @sin)

%% y(x) = sinh(x)
equation = {@(x,y) y'' - y};
cond     =  @(y) {y(1) - sinh(1);
                 diff(y, 5) - cosh(5)};
problem = tau.problem(equation, domain, cond, options);
a = tau.ode.Schur.solve(problem, 'precision', eps);
basis.mad(a, @sinh)

%% y(x) = cos(x)
equation = {@(x,y) y'' + y};
cond     =  @(y) {y(1) - cos(1);
                 diff(y, 5) + sin(5)};
problem = tau.problem(equation, domain, cond, options);
a = tau.ode.Schur.solve(problem, 'precision', eps);
basis.mad(a, @cos)

%% y(x) = cosh(x)
equation = {@(x,y) y'' - y};
cond     =  @(y) {y(1) - cosh(1);
                 diff(y, 5) - sinh(5)};
problem = tau.problem(equation, domain, cond, options);
a = tau.ode.Schur.solve(problem, 'precision', eps);
basis.mad(a, @cosh)

%% y(x) = exp(x)
equation = @(x,y) y' - y;
cond     = @(y) y(1) -exp(1);
problem = tau.problem(equation, domain, cond, options);
a = tau.ode.Schur.solve(problem, 'precision', eps);
basis.mad(a, @exp)

%% y(x) = ln(x)
equation = {@(x,y) x*y', @(x) 1};
cond     = @(y) y(1);
problem = tau.problem(equation, domain, cond, options);
a = tau.ode.Schur.solve(problem, 'precision', eps);
basis.mad(a, @log)
