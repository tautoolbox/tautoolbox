function varargout = tauPath()
%tauPath - Set the path to Tau Toolbox functions.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    persistent tau_path    % directory location of Tau Toolbox
    if isempty(tau_path)
        tau_path = fileparts(mfilename ('fullpath'));
    end

    if ~any(strcmp(path,tau_path))
        add = @(p) addpath(fullfile(tau_path, p));      % helper function

        % add external packages used by Tau Toolbox
        dir_list = {'external', 'external/nleigs'};
        addpath(tau_path);
        for i = 1:length(dir_list)
            add(dir_list{i});
        end

        % add compatibility layer for previous version of Matlab or Octave
        if ~exist('OCTAVE_VERSION', 'builtin') % using Matlab
            add('external/matlab');
            if verLessThan('matlab', '9.0') % 2016a
                add('compat/matlab/pre-2016a');
            end
            if verLessThan('matlab', '9.1') % 2016b
                add('compat/matlab/pre-2016b');
            end
            if verLessThan('matlab', '9.3') % 2017b
                add('compat/matlab/pre-2017b');
            end
            % if verLessThan('matlab', '9.5') % 2018b
            %     add('compat/matlab/pre-2018b');
            % end
            
%             % surpress underline warnings (due to overloaded functions)
%             if ~verLessThan('matlab', '9.8') % >=2020a
%                 s = settings;
%                 s.matlab.codeanalyzer.UnderlineOption.TemporaryValue = 1;
%             end
        else                                   % using Octave
            % warning ('off', 'backtrace');
            add('external/octave');
            % tablicious package already supports contains
            if ~exist('contains', 'file')
                add('compat/octave/all');
            end
            if compare_versions (OCTAVE_VERSION, '5.0.0', '<')
                add('compat/octave/pre-5.0.0');
            end
        end
    end
    if nargout == 1
        varargout{1} = tau_path;
    end
end
