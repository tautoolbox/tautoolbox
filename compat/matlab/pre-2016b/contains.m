function cnt = contains(str, pattern)
    cnt = ~isempty(strfind(str, pattern));
end
