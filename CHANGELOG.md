# Changelog

All the notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased: to be 0.92] - 2023-xx-xx

### Added

- Added a new namespace `orthogonal` where all orthogonal related classes, the basis and polynomial, were moved.
- There is a new class `polynomial.settings` used to build orthogonal basis and that is now a subset of `tau.settings`.
- Added a new function called `tau.polynomial` that using a `tau.settings` object returns the appropriate `polynomial.Polynomial1` object.
- Added the same output arguments to all integro-differential solvers.

### Changed

- Some functions were changed to deal with the creation of the new namespace, e.g., `tau.basis` -> `polynomial.basis`.
- The `polynomial.basis` do not have anymore an associated degree, thus it is necessary to pass the degree when asking for a given matrix.
- `tau.settings` inherits from `polynomial.settings` and thus it can be used in any place where `polynomial.settings` is used.
- Methods `conditions` and `system` from `polynomial.basis` were renamed to `systemBlock` and `systemRow` respectively.
- The errors returned from the Tau solvers (residual error, Tau residual and Cauchy errors) as optional output arguments are now all of `polynomial.Polynomial1`.
- To select a coordinate from an orthogonal polynomial or a Tau condition it should be used curly brackets and not parenthesis as it was before.
- Deprecate completely passing functions as strings, no example uses them anymore.

### Removed

- Most classes lost a `tau.settings` property called `options`.
- `polynomial.Polynomial1` do not solve algebraic linear systems anymore, that functionality was moved to `tau.utils.solve.`
- Removed `polynomial.Polynomial1` static methods `zeros`, replaced by `polynomial.Polynomial1([], options)`, and `ones`.

### Fixed

- Nonlinear solvers now work (previously they were disabled).
- `polynomial.utils.spy` had a bug where it mostly worked for square matrices.


## [0.91] - 2022-06-17

### Added

- Systems of linear integro-differential equations now work with higher than the first derivatives.
- Added more eigenvalue examples.
- Added support for more eigenvalue solvers.

### Changed

- Renamed `tau.dvar`, the class previously called `dtau`, was renamed to `tau.operator`.
- Examples have been converted to use anonymous functions to define the problems to be solved.
- Nonlinear problems can now be solved from the general solver.

### Removed

- Removed `tau.ivar`, the class previously called `itau`, was removed (wrapped inside `tau.operator` with the support of `tau.polynomial`).
- Removed `tau.eig.residualPlot` since the residual output is again a `tau.polynomial` it knows how to plot itself.

### Fixed

- Output the appropriated additional matrices in the eigenvalue problems.


## [0.90] - 2021-05-22

### Added

- Moved all the functionality to the `tau` namespace.
- Add solvers for eigenvalue integro-differential problems.
- The solvers for integro-differential problems all return an object of type `tau.polynomial`.
- The new class `tau.polynomial` that has replaced `ctau` now represents a polynomial of a given order:
    * It knows how to draw itself.
    * If the operation changes the order of the polynomial, like taking its square, the order will change (expand) accordingly.
- Several new classes were added to the Toolbox:
    * `tau.problem` holds all the elements that describe the problem to be solved and is passed to the solvers.
    * `tau.equation` holds the equations.
    * `tau.condition` describes the initial/boundary conditions or constraints to be satisfied.
    * `tau.settings` holds all the settings/options to be used in the solution of the problems.
    * `tau.bases.orthogonal` deals with the internal operations of each orthogonal basis.
- The orthogonal bases are implemented as classes and have been divided in two categories:
    * Supported
        + `ChebyshevT` Chebyshev of first kind.
        + `ChebyshevU` Chebushev of second kind.
        + `LegendreP` Legendre.
        + `GegenbauerC` ultraspherical polynomials.
        + `PowerX` power basis (although not technically an orthogonal basis it is useful to debug but not so much numerically).
    * Experimental
        + `BesselY` Bessel.
        + `ChebyshevV` Chebyshev of third kind.
        + `ChebyshevW` Chebushev of fourth kind.
        + `HermiteH` Hermite.
        + `LaguerreL` Laguerre.
- Added an helper function called `tau.matrix` that accepts either a basis or a problem to return the corresponding matrix used in the solution of the associated linear system (in the Tau method).
- Both equations and conditions can now be passed as anonymous functions and not just a char arrays/strings.
- Added new examples.
- Added new tests.
- The code now works also for Octave (minimum tested version is `5.1.0`, others can work but were not tested).

### Changed

- The code base was totally reformulated.
- Tau Toolbox does not use global variables anymore.
- The general solver was renamed to `tau.solve`:
    * only works with `tau.problem` objects;
    * `tau.eig.solve` solves eigenvalue problems;
    * `tau.ode.solve` solves integro-differential problems;
    * `tau.ode.piecewise.` just like the above but solves in a piecewise way.
- The following auxiliary classes were renamed:
    * `ctau` -> `tau.polynomial`
    * `dtau` -> `tau.dvar`
    * `itau` -> `tau.ivar`
- Other auxiliary classes like `etau` and `rtau` were removed.
- The conditions to solve systems of integro-differential problems can be linear combinations of the functions and its derivatives taken at any point of the integration domain.

### Removed

- The `tau` function that existed before has been removed.
- It is no longer possible to pass a char array as the argument of the different functions. If you need use cell arrays instead.
- The following auxiliary classes were removed:
    * `etau`
    * `rtau`

### Fixed

- The solution, using a Tau solver, is a `tau.polynomial`, for single equation, systems of equations and for piecewise solutions.

## [0.1.0] - 2017-05-17

### Added

First release of the project.
