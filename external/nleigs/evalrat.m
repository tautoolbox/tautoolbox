function r = evalrat( sigma,xi,beta,z )
%EVALRAT Evaluate nodal rational function at the points z

r = ones(size(z))/beta(1);
for j = 1:length(sigma)
    r = r .* (z - sigma(j)) ./ (1 - z/xi(j)) / beta(j+1);
end
