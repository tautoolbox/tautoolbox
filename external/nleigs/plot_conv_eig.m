function [ResIn,ResOut,LamIn,LamOut] = ...
    plot_conv_eig(Lam,Res,Sigma,plotopts1,plotopts2)

if nargin == 2
    Sigma = [];
    plotopts1 = '-k';
    plotopts2 = '-k';
elseif nargin == 3
    plotopts1 = '-k';
    plotopts2 = ':k';
elseif nargin == 4
    plotopts2 = ':k';
end

n = size(Lam,1);
LamC = zeros(n);
ResC = zeros(n);

% first iteration
LamC(1,1) = Lam(1,1);
ResC(1,1) = Res(1,1);

% next iterations
for i = 2:n
    sLam = Lam(1:i,i);
    sRes = Res(1:i,i);
    for j = 1:i-1
        [~,ii] = min(abs(sLam-LamC(j,i-1)));
        LamC(j,i) = sLam(ii);
        ResC(j,i) = sRes(ii);
        sLam(ii) = [];
        sRes(ii) = [];
    end
    LamC(i,i) = sLam;
    ResC(i,i) = sRes;
end

z = inpolygon(real(LamC(:,end)),imag(LamC(:,end)),real(Sigma),imag(Sigma));
ResIn = ResC(z,:);
ResOut = ResC(~z,:);
LamIn = LamC(z,:);
LamOut = LamC(~z,:);

for i = 1:n
    if isempty(Sigma)
        semilogy(i:n,ResC(i,i:n),plotopts1); hold on;
    elseif z(i)
        semilogy(i:n,ResC(i,i:n),plotopts1); hold on;
    elseif ~isempty(plotopts2)
        semilogy(i:n,ResC(i,i:n),plotopts2); hold on;
    end
end
xlabel('iteration');
ylabel('residual');
