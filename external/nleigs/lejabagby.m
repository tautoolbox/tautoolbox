function [a,b,beta] = lejabagby(A,B,C,m,keepA,forceInf)
%LEJABAGBY generate Leja-Bagby points (a,b) on (A,B), with
%  scaling factors beta such that the uniform norm on the control set C is
%  1. Greedy search for a minimum is performed on B. If keepA is true then
%  the points in the output a will be exactly those of A, otherwise the
%  points in a are also chosen via greedy search on A. If forceInf is a
%  positive integeger, the the first forceInf poles in b will be infinity.

if nargin < 5,
    keepA = 0;
end

if nargin < 6,
    forceInf = 0;
end

if min(abs(B)) < 1e-9,
    warning('LEJABAGBY: There is at least one pole candidate in B being nearby zero. Consider shifting your problem for stability.');
end


a(1) = A(1);
if forceInf > 0,
    b(1) = inf;
else
    b(1) = B(1);
end

sA = 0*A + 1;
sB = 0*B + 1;
sC = 0*C + 1;

beta(1) = 1;
for j = 1:m-1,
    
    sA = sA .* ((A-a(j)) ./ (1 - A/b(j)));
    sC = sC .* ((C-a(j)) ./ (1 - C/b(j)));
    sB = sB .* ((B-a(j)) ./ (1 - B/b(j)));
    
    if keepA,
        a(j+1) = A(j+1);
    else
        [mA,indA] = max(abs(sA));
        a(j+1) = A(indA);
    end
    [mB,indB] = min(abs(sB));
    
    if forceInf > j,
        b(j+1) = inf;
    else
        b(j+1) = B(indB);
    end
    beta(j+1) = max(max(abs(sC)));
    
    
    % treat single point case
    if beta(j+1) < eps,
        beta(j+1) = 1;
    end
    
    sA = sA / beta(j+1);
    sB = sB / beta(j+1);
    sC = sC / beta(j+1);
    
end
