function varargout = nleigs(varargin)
%NLEIGS  Find a few eigenvalues and eigenvectors of a NLEP
%   lambda = NLEIGS(NLEP,Sigma,[Xi]) returns a vector of eigenvalues of the
%   nonlinear eigenvalue problem NLEP inside the target set Sigma. NLEP is a
%   structure representing the nonlinear eigenvalue problem as a function
%   handle:
%     NLEP.Fun:  function handle A = NLEP.Fun(lambda)
%     NLEP.n:    size of A
%   or as a sum of constant matrices times scalar functions:
%     NLEP.B:    cell array of matrices of the polynomial part
%     NLEP.C:    cell array of matrices of the nonlinear part
%     NLEP.L:    cell array of L-factors of matrices C (optional)
%     NLEP.U:    cell array of U-factors of matrices C (optional)
%     NLEP.f:    cell array of the nonlinear scalar or matrix functions
%   Sigma is a vector containing the points of a polygonal target set in the
%   complex plane. Xi is a vector containing a discretization of the singularity
%   set. In case the singularity set is omitted Xi is ste to Inf.
%
%   [X,lambda,res] = NLEIGS(NLEP,Sigma,[Xi]) returns a matrix X of eigenvectors
%   and a vector of corresponding eigenvalues of the nonlinear eigenvalue
%   problem NLEP inside the target set Sigma. The vector res contains the
%   corresponding residuals.
%
%   [X,lambda,res] = NLEIGS(NLEP,Sigma,Xi,options) sets the algorithm's
%   parameters to the values in the structure options:
%     options.disp:     level of display
%                       [ {0} | 1 | 2 ]
%     options.maxdgr:   max degree of approximation
%                       [ positive integer {100} ]
%     options.minit:    min number of iter. after linearization is converged
%                       [ positive integer {20} ]
%     options.maxit:    max number of total iterations
%                       [ positive integer {200} ]
%     options.tolres:   tolerance for residual
%                       [ positive scalar {1e-10} ]
%     options.tollin:   tolerance for convergence of linearization
%                       [ positive scalar {1e-11} ]
%     options.v0:       starting vector
%                       [ vector array {randn(n,1)} ]
%     options.funres:   function handle for residual (Lambda[vector], X[matrix])
%                       [ @(Lambda,X) {norm(A(lam)*x)} ]
%     options.isfunm:   use matrix functions
%                       [ boolean {true} ]
%     options.static:   static version of nleigs
%                       [ boolean {false} ]
%     options.leja:     use of Leja-Bagby points
%                       [ 0 (no) | {1} (only in expansion phase) | 2 (always) ]
%     options.nodes:    prefixed interpolation nodes (only with leja is 0 or 1)
%                       [ vector array | {[]} ]
%     options.reuselu:  reuse of LU-factorizations of A(sigma)
%                       [ 0 (no) | {1} (only after conv. lin) | 2 (always) ]
%     options.blksize:  block size for pre-allocation
%                       [ positive integer {20} ]
%
%   [X,lambda,res,info] = NLEIGS(NLEP,Sigma,[Xi]) also returns a structure info:
%     info.Lam:    matrix of Ritz values in each iteration 
%     info.Res:    matrix of residuals in each iteraion
%     info.sigma:  vector of interpolation nodes
%     info.xi:     vector of poles
%     info.beta:   vector of scaling parameters
%     info.nrmD:   vector of norms of generalized divided differences (in
%                  function handle case) or maximum of absolute values of
%                  scalar divided differences in each iteration (in matrix
%                  function case)
%
%   See also EIG, EIGS, NLEIGSPLOT.

%   Reference:
%   S. Guettel, R. Van Beeumen, K. Meerbergen, and W. Michiels. NLEIGS: A class
%   of fully rational Krylov methods for nonlinear eigenvalue problems. SIAM J.
%   Sci. Comput., 36(6), A2842–A2864, 2014.
%
%   Roel Van Beeumen
%   April 5, 2016

%% Check inputs
[funA,Ahandle,B,BB,pf,C,CC,f,iL,L,LL,U,n,p,q,r,Sigma,leja,nodes,Xi,tollin,...
    tolres,maxdgr,minit,maxit,isfunm,static,v0,reuselu,funres,b,computeD,...
    resfreq,verbose] = checkInputs(varargin{:});

%% Initialization
if computeD
    D = cell(1,b+1);
end
if static
    V = zeros(n,1);
elseif Ahandle
    V = zeros((b+1)*n,b+1);
else
    if r == 0 || b < p
        V = zeros((b+1)*n,b+1);
    else
        V = zeros(p*n+(b-p+1)*r,b+1);
    end
end
H = zeros(b+1,b);
K = zeros(b+1,b);
if nargout > 3
    Lam = zeros(b);
    Res = zeros(b);
    nrmD = zeros(b+1,1);
end

%% Discretization of Sigma --> Gamma & Leja-Bagby points
if leja == 0 % use no leja nodes
    if isempty(nodes)
        error(['Interpolation nodes must be provided via ''options.nodes'' ',...
            'when no Leja-Bagby points (options.leja == 0) are used.']);
    end
    gamma = discretizepolygon(Sigma);
    if static
        sigma = repmat(nodes(:),ceil((maxit+maxdgr+2)/length(nodes)),1);
    else
        sigma = repmat(nodes(:),ceil((max(maxit,maxdgr)+2)/length(nodes)),1);
    end
    [~,xi,beta] = lejabagby(sigma(1:maxdgr+2),Xi,gamma,maxdgr+2,true,p);
elseif leja == 1 % use leja nodes in expansion phase
    if isempty(nodes)
        [gamma,nodes] = discretizepolygon(Sigma);
    else
        gamma = discretizepolygon(Sigma);
    end
    nodes = repmat(nodes(:),ceil((maxit+1)/length(nodes)),1);
    [sigma,xi,beta] = lejabagby(gamma,Xi,gamma,maxdgr+2,false,p);
else % use leja nodes in both phases
    gamma = discretizepolygon(Sigma);
    if static
        [sigma,xi,beta] = lejabagby(gamma,Xi,gamma,maxit+maxdgr+2,false,p);
    else
        [sigma,xi,beta] = lejabagby(gamma,Xi,gamma,max(maxit,maxdgr)+2,false,p);
    end
end
xi(maxdgr+2) = NaN; % not used
if (Ahandle || ~isfunm) && length(sigma) ~= length(unique(sigma))
    error(['All interpolation nodes must be distinct when no matrix ',...
        'functions are used for computing the generalized divided ',...
        'differences.']);
end

%% Rational Newton coefficients
range = 1:maxdgr+2;
if Ahandle
    D = ratnewtoncoeffs(funA,sigma(range),xi(range),beta(range));
    nrmD(1) = norm(D{1},'fro'); %
else
    % Compute scalar generalized divided differences
    [sgddp,sgddq] = scgendivdiffs(sigma(range),xi(range),beta(range));
    % Construct first generalized divided difference
    if computeD
        D{1} = constructD(0);
    end
    % Norm of first generalized divided difference
    nrmD(1) = max(abs([sgddp(:,1);sgddq(:,1)]));
end
if ~isfinite(nrmD(1)) % check for NaN
    error('The generalized divided differences must be finite.');
end
lusolver; % reset lusolver

%% Rational Krylov
if reuselu == 2
    v0 = lusolver(sigma(1),v0/norm(v0));
else
    v0 = funA(sigma(1))\(v0/norm(v0));
end
V(1:n,1) = v0/norm(v0);
expand = true;
kconv = Inf;
kn = n;   % length of vectors in V
l = 0;    % number of vectors in V
N = 0;    % degree of approximations
kmax = maxit;
if static
    kmax = kmax + maxdgr;
end
k = 1;
while k <= kmax
    
    % allociation
    if l > 0 && (b == 1 || mod(l+1,b) == 1)
        nb = 1 + l/b;
        if Ahandle
            V(kn+b*n,nb*b+1) = 0;
        else
            if expand && computeD
                D{l+b+1} = [];
            end
            if expand
                if r == 0 || l + b < p
                    V(kn+b*n,nb*b+1) = 0;
                elseif l < p-1
                    V(p*n+(nb*b-p+1)*r,nb*b+1) = 0;
                else % l => p-1
                    V(kn+b*r,nb*b+1) = 0;
                end
            else
                V(:,nb*b+1) = 0;
            end
        end
        H(end+b,end+b) = 0;
        K(end+b,end+b) = 0;
        if nargout > 3
            Lam(end+b,end+b) = 0;
            Res(end+b,end+b) = 0;
            if expand
                nrmD(end+b) = 0;
            end
        end
    end
    
    % set length of vectors
    if expand
        if r == 0 || k < p
            kn = kn + n;
        else
            kn = kn + r;
        end
    end
    
    % rational divided differences
    if expand
        if Ahandle
            % 
        elseif computeD
            D{k+1} = constructD(k);
        end
        N = N + 1;
    end
    
    % monitoring norms of divided difference matrices
    if expand
        if Ahandle
            nrmD(k+1) = norm(D{k+1},'fro');
        else
            nrmD(k+1) = max(abs([sgddp(:,k+1);sgddq(:,k+1)]));
        end
        if ~isfinite(nrmD(k+1)) % check for NaN
            error('The generalized divided differences must be finite.');
        end
        if n > 1 && k >= 5 && k < kconv
            if sum(nrmD(k-3:k+1)) < 5*tollin
                kconv = k - 1;
                if static
                    kmax = maxit + kconv;
                end
                expand = false;
                if leja == 1
                    sigma(k+1:kmax+1) = nodes(1:kmax-k+1);
                end
                if Ahandle || computeD
                    D = D(1:k);
                end
                xi = xi(1:k);
                beta = beta(1:k);
                nrmD = nrmD(1:k);
                if static
                    if r == 0 || k < p
                        kn = kn - n;
                    else
                        kn = kn - r;
                    end
                    V(kn,b+1) = 0;
                end
                N = N - 1;
                if verbose > 0
                    display(['Linearization converged after ',num2str(kconv),...
                        ' iterations']);
                    display(' --> freeze linearization');
                end
            elseif k == maxdgr+1
                kconv = k;
                expand = false;
                if leja == 1
                    sigma(k+1:kmax+1) = nodes(1:kmax-k+1);
                end
                if static
                    V(kn,b+1) = 0;
                end
                N = N - 1;
                warning('NLEIGS:noconvergence',['Linearization not',...
                    ' converged after ',num2str(maxdgr),' iterations']);
                if verbose > 0
                    display(' --> freeze linearization');
                end
            end
        end
    end
    
    if ~static
        l = k;
    else
        l = k - N;
    end
    
    % shift-and-invert
    if ~static || (static && ~expand)
        t = [zeros(l-1,1);1];  % contination combination
        wc = V(1:kn,l);        % contination vector
        w = backslash(wc);
    end
    
    % orthogonalization
    if ~static || (static && ~expand)
        normw = norm(w);
        h = V(1:kn,1:l)'*w;
        w = w - V(1:kn,1:l)*h;
        H(1:l,l) = h;
        eta = 1/sqrt(2);       % reorthogonalization constant
        if norm(w) < eta*normw
            h = V(1:kn,1:l)'*w;
            w = w - V(1:kn,1:l)*h;
            H(1:l,l) = H(1:l,l) + h;
        end
        K(1:l,l) = H(1:l,l)*sigma(k+1) + t;
    end
    
    % new vector
    if ~static || (static && ~expand)
        H(l+1,l) = norm(w);
        K(l+1,l) = H(l+1,l)*sigma(k+1);
        V(1:kn,l+1) = w/H(l+1,l);
    end
    
    % Ritz pairs
    if nargout <= 3 && ((~expand && k >= N + minit && ...
            mod(k-(N+minit),resfreq) == 0) || (k >= kconv + minit && ...
            mod(k-(kconv+minit),resfreq) == 0) || k == kmax)
        [S,Lambda] = eig(K(1:l,1:l),H(1:l,1:l));
        lambda = diag(Lambda);
        % select eigenvalues
        ilam = 1:l;
        ilam = ilam(inSigma(lambda));
        lam = lambda(ilam);
        nblamin = length(lam);
        for i = ilam
            S(:,i) = S(:,i)/norm(H(1:l+1,1:l)*S(:,i));
        end
        X = V(1:n,1:l+1)*(H(1:l+1,1:l)*S(:,ilam));
        for i = 1:size(X,2)
            X(:,i) = X(:,i)/norm(X(:,i));
        end
        % compute residuals
        res = funres(lam,X);
        % check for convergence
        conv = abs(res) < tolres;
        nbconv = sum(conv);
        if verbose > 0
            if static
                kstr = num2str(k - N);
            else
                kstr = num2str(k);
            end
            display(['  iteration ',kstr,': ',num2str(nbconv),...
                ' of ',num2str(nblamin),' < ',num2str(tolres)]);
        end
    elseif nargout > 3 % info output
        if ~static || (static && ~expand)
            [S,Lambda] = eig(K(1:l,1:l),H(1:l,1:l));
            lambda = diag(Lambda);
            % select eigenvalues
            ilam = 1:l;
            ilam = ilam(isfinite(lambda));
            lam = lambda(ilam);
            lamin = inSigma(lam);
            nblamin = sum(lamin);
            for i = ilam
                S(:,i) = S(:,i)/norm(H(1:l+1,1:l)*S(:,i));
            end
            X = V(1:n,1:l+1)*(H(1:l+1,1:l)*S(:,ilam));
            for i = 1:size(X,2)
                X(:,i) = X(:,i)/norm(X(:,i));
            end
            % compute residuals
            res = zeros(l,1);
            res(~isfinite(lambda)) = NaN;
            res(ilam) = funres(lam,X);
            [~,si] = sort(lambda);
            Res(1:l,l) = res(si);
            Lam(1:l,l) = lambda(si);
            % check for convergence
            res = res(ilam);
            conv = (abs(res) < tolres) & lamin;
            nbconv = sum(conv);
            if verbose > 0
                if static
                    kstr = num2str(k - N);
                else
                    kstr = num2str(k);
                end
                display(['  iteration ',kstr,': ',num2str(nbconv),...
                    ' of ',num2str(nblamin),' < ',num2str(tolres)]);
            end
        end
    end
    
    % stopping
    if ((~expand && k >= N + minit) || k >= kconv + minit) && nblamin == nbconv
        break;
    end
    
    % increment k
    k = k + 1;
    
end
lusolver; % reset lusolver

%% Output(s)
if nargout <= 1
    varargout{1} = lam(conv);
end
if nargout > 1
    varargout{1} = X(:,conv);
    varargout{2} = lam(conv);
end
if nargout > 2
    varargout{3} = res(conv);
end
if nargout > 3
    Lam = Lam(1:l,1:l);
    Res = Res(1:l,1:l);
    sigma = sigma(1:k);
    if expand
        xi = xi(1:k);
        beta = beta(1:k);
        nrmD = nrmD(1:k);
        warning('NLEIGS:noconvergence',['Linearization not converged after ',...
                    num2str(maxdgr),' iterations']);
    end
    info = struct('Lam',Lam,'Res',Res,'sigma',sigma,'xi',xi,'beta',beta,...
        'nrmD',nrmD,'kconv',kconv);
    varargout{4} = info;
end



% ---------------------------------------------------------------------------- %
% Nested functions
% ---------------------------------------------------------------------------- %

% checkInputs: error checks the inputs to NLEP and also derives some variables
% '''''''''''' from them:
%
%   funA       function handle for A(lambda)
%   Ahandle    is true if NLEP is given as function handle
%   B          cell array {B0,B1,...,Bp}
%   BB         matrix [B0;B1;...;Bp]
%   pf         cell array {x^0,x^1,...,x^p}
%   C          cell array {C1,C2,...,Cq}
%   CC         matrix [C1;C2;...;Cq]
%   f          cell array {f1,f2,...,fq}
%   iL         vector with indices of L-factors
%   L          cell array {L1,L2,...,Lq}
%   LL         matrix [L1,L2,...,Lq]
%   U          matrix [U1,U2,...,Uq]
%   n          dimension of NLEP
%   p          order of polynomial part (length of B = p + 1)
%   q          length of f and C
%   r          sum of ranks of low rank matrices in C
%   Sigma      vector with target set (polygon)
%   leja       0: no leja, 1: leja in expansion phase, 2: always leja
%   nodes      [] or given nodes
%   Xi         vector with singularity set (discretized)
%   computeD   is true if generalized divided differences are explicitly used
%   tollin     tolerance for convergence of linearization
%   tolres     tolerance for residual
%   maxdgr     maximum degree of approximation
%   minit      minimum number of iterations after linearization is converged
%   maxit      maximum number of iterations
%   isfunm     is true if f are matrix functions
%   static     is true if static version is used
%   v0         starting vector
%   reuselu    positive integer for reuse of LU-factorizations of A(sigma)
%   funres     function handle for residual R(Lambda,X)
%   b          block size for pre-allocation
%   verbose    level of display [ {0} | 1 | 2 ]
    function [funA,Ahandle,B,BB,pf,C,CC,f,iL,L,LL,U,n,p,q,r,Sigma,leja,nodes,...
            Xi,tollin,tolres,maxdgr,minit,maxit,isfunm,static,v0,reuselu,...
            funres,b,computeD,resfreq,verbose] = checkInputs(varargin)
        
        %% initialize
        B = [];
        BB = [];
        C = [];
        CC = [];
        pf = {};
        f = {};
        iL = 0;
        L = [];
        LL = [];
        U = [];
        p = -1;
        q = 0;
        r = 0;
        
        %% process the input A
        A = varargin{1};
        if ~isa(A,'struct')
            error('The input ''A'' must be a struct representing the NLEP.')
        end
        if isfield(A,'Fun')
            Ahandle = true;
            % function handle for A(lambda)
            funA = A.Fun;
        else
            Ahandle = false;
            % polynomial part B
            B = A.B;
            if isempty(B)
                p = -1;
            elseif isfloat(B)
                p = 0;
                B = {B};
            elseif iscell(B) && isvector(B)
                p = length(B) - 1;
            else
                error(['The constant matrices of the polynomial part ''B'' ',...
                    'must be a 1 dimensional cell array.']);
            end
            % nonlinear part C
            if isfield(A,'C')
                C = A.C;
                if isempty(C)
                    qC = 0;
                elseif isfloat(C)
                    qC = 1;
                    C = {C};
                elseif iscell(C) && isvector(C)
                    qC = length(C);
                else
                    error(['The constant matrices of the nonlinear part ',...
                        '''C'' must be a 1 dimensional cell array.']);
                end
                f = A.f;
            end
            % L and U factors of the low rank nonlinear part C
            if isfield(A,'L')
                L = A.L;
                if isfloat(L)
                    qL = 1;
                    L = {L};
                elseif iscell(L) && isvector(L)
                    qL = length(L);
                else
                    error(['The ''L''-factor matrices of the nonlinear ',...
                        'part ''C'' must be a 1 dimensional cell array.']);
                end
                U = A.U;
                if isfloat(U)
                    qU = 1;
                    U = {U};
                elseif iscell(U) && isvector(U)
                    qU = length(U);
                else
                    error(['The ''U''-factor matrices of the nonlinear ',...
                        'part ''C'' must be a 1 dimensional cell array.']);
                end
                if qL ~= qU
                    error(['The number of ''L''- and ''U''-factors must be ',...
                        'equal.']);
                end
                if ~isfield(A,'C')
                    qC = qL;
                    C = cell(qC,1);
                    for ii = 1:qC
                        C{ii} = L{ii}*U{ii}';
                    end
                elseif qC ~= qL
                    error(['The number of ''L''- and ''U''-factors must be ',...
                        'equal to the number of constant matrices of the '...
                        'nonlinear part ''C''.']);
                end
                U = cell2mat(U(:).');
                r = size(U,2);
                iL = zeros(r,1);
                c = 0;
                for ii = 1:qL
                    ri = size(L{ii},2);
                    iL(c+1:c+ri) = ii;
                    c = c + ri;
                end
                LL = cell2mat(L(:).');
            end
            % nonlinear function f
            if isfield(A,'C') || isfield(A,'L')
                f = A.f;
                if isempty(f)
                    qf = 0;
                elseif isa(f,'function_handle')
                    qf = 1;
                    f = {f};
                elseif iscell(f) && isvector(f)
                    qf = length(f);
                else
                    error(['The scalar functions ''f'' of the nonlinear ',...
                        'part ''C'' must be a 1 dimensional cell array.']);
                end
                if qC == qf
                    q = qC;
                else
                    error(['The number of constant matrices ''C'' and the ',...
                        'number of scalar functions ''f'' must be equal.']);
                end
            end
        end
        
        %% process the input Sigma
        Sigma = varargin{2};
        if ~isnumeric(Sigma) || ~isvector(Sigma)
            error('The target set ''Sigma'' must be a vector.');
        end
        
        %% process the input Xi
        if nargin > 2
            Xi = varargin{3};
            if isempty(Xi)
                Xi = Inf;
            end
            if ~isvector(Xi) || (isvector(Xi) && ~isnumeric(Xi))
                error('The poles set ''Xi'' must be a vector.');
            end
        else
            Xi = Inf;
        end
        
        %% set n and funA
        if Ahandle
            % dimension of A(lambda)
            n = A.n;
            if ~isscalar(n) || ~isreal(n) || (n<0) || ~isfinite(n)
                error('Size of problem ''n'' must be a positive integer.');
            end
            if issparse(n)
                n = full(n);
            end
            if (round(n) ~= n)
                warning('WarnTests:convertTest',...
                    ['Size of problem ''n'' must be a positive integer.',...
                    '\n         Rounding input size.']);
                n = round(n);
            end
        else
            if p >= 0
                n = size(B{1},1);
                pf = monomials(p);
            elseif q > 0
                n = size(C{1},1);
            else
                error('''B'' or ''C'' must be non-empty.');
            end
            if q == 0
                if issparse(B{1})
                    A = cell2mat(cellfun(@(x) x(:),B,'UniformOutput',0));
                else
                    A = cell2mat(reshape(B,1,1,[]));
                end
                F = pf;
            elseif p < 0
                if issparse(C{1})
                    A = cell2mat(cellfun(@(x) x(:),C,'UniformOutput',0));
                else
                    A = cell2mat(reshape(C,1,1,[]));
                end
                F = f(:);
            else
                if issparse(B{1})
                    A = cell2mat(cellfun(@(x) x(:),[B(:).',C(:).'],...
                        'UniformOutput',0));
                else
                    A = cell2mat(reshape([B(:);C(:)],1,1,[]));
                end
                F = [pf(:);f(:)];
            end
            if issparse(A)
                % funA = @(lambda) reshape(A*sparse(cell2mat(...
                %     cellfun(@(x) x(lambda),F,'UniformOutput',0))),[n,n]);
                % avoid "out of memory" bug with sparse-sparse mult.
                funA = @(lambda) reshape(spmultiply(A,sparse(cell2mat(...
                    cellfun(@(x) x(lambda),F,'UniformOutput',0)))),[n,n]);
            else
                funA = @(lambda) sum(bsxfun(@times,A,reshape(cell2mat(...
                    cellfun(@(x) x(lambda),F,'UniformOutput',0)),1,1,[])),3);
            end
            BB = cell2mat(B(:));
            CC = cell2mat(C(:));
        end
        
        %% set defaults
        verbose = 0;
        maxdgr = 100;
        minit = 20;
        maxit = 200;
        tolres = 1e-10;
        tollin = max(tolres/10,100*eps);
        v0 = randn(n,1);
        funres = @residual;
        isfunm = true;
        static = false;
        leja = 1;
        nodes = [];
        reuselu = 1;
        b = 20;
        % extra defaults
        computeD = (n <= 400);
        resfreq = 5;
        
        %% process the input options
        if nargin > 3
            options = varargin{4};
            if ~isa(options,'struct')
                error('The input argument ''options'' must be a struct.');
            end
            if isfield(options,'disp') && ~isempty(options.disp)
                verbose = options.disp;
            end
            if isfield(options,'maxdgr') && ~isempty(options.maxdgr)
                maxdgr = options.maxdgr;
            end
            if isfield(options,'minit') && ~isempty(options.minit)
                minit = options.minit;
            end
            if isfield(options,'maxit') && ~isempty(options.maxit)
                maxit = options.maxit;
            end
            if isfield(options,'tolres') && ~isempty(options.tolres)
                tolres = options.tolres;
            end
            if isfield(options,'tollin') && ~isempty(options.tollin)
                tollin = options.tollin;
            end
            if isfield(options,'v0') && ~isempty(options.v0)
                v0 = options.v0;
            end
            if isfield(options,'funres') && ~isempty(options.funres)
                funres = options.funres;
            end
            if isfield(options,'isfunm') && ~isempty(options.isfunm)
                isfunm = options.isfunm;
            end
            if isfield(options,'static') && ~isempty(options.static)
                static = options.static;
            end
            if isfield(options,'leja') && ~isempty(options.leja)
                leja = options.leja;
            end
            if isfield(options,'nodes') && ~isempty(options.nodes)
                nodes = options.nodes;
            end
            if isfield(options,'reuselu') && ~isempty(options.reuselu)
                reuselu = options.reuselu;
            end
            if isfield(options,'blksize') && ~isempty(options.blksize)
                b = options.blksize;
            end
        end
        if n == 1
            maxdgr = maxit + 1;
        end

    end % checkInputs

% ---------------------------------------------------------------------------- %

% scgendivdiffs: compute scalar generalized divided differences
% ''''''''''''''
%   sigma   discretization of target set
%   xi      discretization of singularity set
%   beta    scaling factors
    function [sgddp,sgddq] = scgendivdiffs(sigma,xi,beta)
        
        %% Compute scalar generalized divided differences of polynomials part
        sgddp = zeros(p+1,maxdgr+2);
        for ii = 1:p+1
            if isfunm
                sgddp(ii,:) = cell2mat(ratnewtoncoeffsm(pf{ii},sigma,xi,beta));
            else
                sgddp(ii,:) = cell2mat(ratnewtoncoeffs(pf{ii},sigma,xi,beta));
            end
        end
        %% Compute scalar generalized divided differences of nonlinear part
        sgddq = zeros(q,maxdgr+2);
        for ii = 1:q
            if isfunm
                sgddq(ii,:) = cell2mat(ratnewtoncoeffsm(f{ii},sigma,xi,beta));
            else
                sgddq(ii,:) = cell2mat(ratnewtoncoeffs(f{ii},sigma,xi,beta));
            end
        end
        
    end % scgendivdiffs

% ---------------------------------------------------------------------------- %

% constructD: Construct generalized divided difference
% '''''''''''
%   nb  number
    function D = constructD(nb)
        
        if r == 0 || nb <= p
            D = spalloc(n,n,n);
            for ii = 1:p+1
                D = D + sgddp(ii,nb+1)*B{ii};
            end
            for ii = 1:q
                D = D + sgddq(ii,nb+1)*C{ii};
            end
        else
            Lt = cell(1,q);
            for ii = 1:q
                Lt{ii} = sgddq(ii,nb+1)*L{ii};
            end
            D = cell2mat(Lt);
        end
        
    end % constructD

% ---------------------------------------------------------------------------- %

% lusolver: Solve system with reusing lu-factors
% '''''''''
%   shift   shift for evaluating funA(shift)
%   y       right hand side
    function x = lusolver(shift,y)
        
        persistent Alu shifts
        
        %% reset
        if nargin < 2,
            Alu = cell(1); shifts = [];
            if verbose > 1
                display('RESET all factorizations');
            end
            return
        end
       
        %% check for new shift
        newlu = false;
        if isempty(shifts)
            idx = 1;
            shifts(idx) = shift;
            newlu = true;
        else
            idx = find(shifts == shift,1,'first');
            if isempty(idx)
                idx = length(shifts) + 1;
                shifts(idx) = shift;
                newlu = true;
            end
        end
        
        %% solve funA(shift)*x = y
        if newlu
            As = funA(shift);
            if issparse(As)
                if verbose > 1
                    display(['COMPUTE sparse factorization nr ',num2str(idx)]);
                end
                [AL,AU,AP,AQ,AR] = lu(As);
                x = AQ * (AU \ (AL \ (AP * (AR \ y))));
                % improve accuracy
                resid = y - As*x;
                err = AQ * (AU \ (AL \ (AP * (AR \ resid))));
                x = x + err;
                Alu{idx} = struct('A',As,'L',AL,'U',AU,'P',AP,'Q',AQ,'R',AR);
            else
                if verbose > 1
                    display(['COMPUTE full factorization nr ',num2str(idx)]);
                end
                [AL,AU,Ap] = lu(As,'vector');
                x = AU \ (AL \ y(Ap,:));
                % improve accuracy
                resid = y - As*x;
                err = AU \ (AL \ resid(Ap,:));
                x = x + err;
                Alu{idx} = struct('A',As,'L',AL,'U',AU,'p',Ap);
            end
        else
            if isfield(Alu{idx},'R')
                if verbose > 1
                    display(['REUSING sparse factorization nr ',num2str(idx)]);
                end
                x = Alu{idx}.Q * (Alu{idx}.U \ (Alu{idx}.L \ ...
                    (Alu{idx}.P * (Alu{idx}.R \ y))));
                % impove accuracy
                resid = y - Alu{idx}.A*x;
                err = Alu{idx}.Q * (Alu{idx}.U \ (Alu{idx}.L \ ...
                    (Alu{idx}.P * (Alu{idx}.R \ resid))));
                x = x + err;
            else
                if verbose > 1
                    display(['REUSING full factorization nr ',num2str(idx)]);
                end
                x = Alu{idx}.U \ (Alu{idx}.L \ y(Alu{idx}.p,:));
                % improve accuracy
                resid = y - Alu{idx}.A*x;
                err = Alu{idx}.U \ (Alu{idx}.L \ resid(Alu{idx}.p,:));
                x = x + err;
            end
        end
        
    end % lusolver

% ---------------------------------------------------------------------------- %

% backslash: Backslash or left matrix divide
% ''''''''''
%   wc       continuation vector
    function w = backslash(wc)
        
        shift = sigma(k+1);
        
        %% construction of B*wc
        Bw = zeros(size(wc));
        % first block (if low rank)
        if r > 0
            i0b = (p-1)*n + 1;
            i0e = p*n;
            if Ahandle || computeD
                Bw(1:n) = -D{p+1}*wc(i0b:i0e)/beta(p+1);
            else
                Bw(1:n) = -sum(bsxfun(@times,reshape(BB*wc(i0b:i0e),n,p+1),...
                    sgddp(:,p+1).'),2)/beta(p+1) - sum(bsxfun(@times,reshape(...
                    CC*wc(i0b:i0e),n,q),sgddq(:,p+1).'),2)/beta(p+1);
            end
        end
        % other blocks
        i0b = 1;
        i0e = n;
        for ii = 1:N
            % range of block i+1
            i1b = i0e + 1;
            if r == 0 || ii < p
                i1e = i0e + n;
            else
                i1e = i0e + r;
            end
            % compute block i+1
            if r == 0 || ii ~= p
                Bw(i1b:i1e) = wc(i0b:i0e) + beta(ii+1)/xi(ii)*wc(i1b:i1e);
            else
                Bw(i1b:i1e) = U'*wc(i0b:i0e) + beta(ii+1)/xi(ii)*wc(i1b:i1e);
            end
            % range of next block i
            i0b = i1b;
            i0e = i1e;
        end
        
        %% construction of z0
        z = Bw;
        i1b = n + 1;
        if r == 0 || p > 1
            i1e = 2*n;
        else
            i1e = n + r;
        end
        nu = beta(2)*(1 - shift/xi(1));
        z(i1b:i1e) = 1/nu * z(i1b:i1e);
        for ii = 1:N
            % range of block i+2
            i2b = i1e + 1;
            if r == 0 || ii < p-1
                i2e = i1e + n;
            else
                i2e = i1e + r;
            end
            % add extra term to z0
            if Ahandle || computeD
                if r == 0 || ii ~= p
                    z(1:n) = z(1:n) - D{ii+1}*z(i1b:i1e);
                end
            else
                if r == 0 || ii < p
                    if p >= 0
                        z(1:n) = z(1:n) - sum(bsxfun(@times,reshape(...
                            BB*z(i1b:i1e),n,p+1),sgddp(:,ii+1).'),2);
                    end
                    if q > 0
                        z(1:n) = z(1:n) - sum(bsxfun(@times,reshape(...
                            CC*z(i1b:i1e),n,q),sgddq(:,ii+1).'),2);
                    end
                elseif ii > p
                    dd = sgddq(:,ii+1);
                    z(1:n) = z(1:n) - LL*(z(i1b:i1e).*dd(iL));
                end
            end
            % update block i+2
            if ii < N
                mu = shift  - sigma(ii+1);
                nu = beta(ii+2)*(1 - shift/xi(ii+1));
                if r == 0 || ii ~= p-1
                    z(i2b:i2e) = 1/nu * z(i2b:i2e) + mu/nu * z(i1b:i1e);
                else % i == p-1
                    z(i2b:i2e) = 1/nu * z(i2b:i2e) + mu/nu * U'*z(i1b:i1e);
                end
            end
            % range of next block i+1
            i1b = i2b;
            i1e = i2e;
        end
        
        %% solving Alam x0 = z0
        w = zeros(size(wc));
        if ((~expand || k > kconv) && reuselu == 1) || (reuselu == 2)
            w(1:n) = lusolver(shift,z(1:n)/beta(1));
        else
            w(1:n) = funA(shift)\(z(1:n)/beta(1));
        end
        
        %% substitutions x[i+1] = mu/nu*x[i] + 1/nu*Bw[i+1]
        i0b = 1;
        i0e = n;
        for ii = 1:N
            % range of block i+1
            i1b = i0e + 1;
            if r == 0 || ii < p
                i1e = i0e + n;
            else
                i1e = i0e + r;
            end
            % compute block i+1
            mu = shift  - sigma(ii);
            nu = beta(ii+1)*(1 - shift/xi(ii));
            if r == 0 || ii ~= p
                w(i1b:i1e) = mu/nu * w(i0b:i0e) + 1/nu * Bw(i1b:i1e);
            else
                w(i1b:i1e) = mu/nu * U'*w(i0b:i0e) + 1/nu * Bw(i1b:i1e);
            end
            % range of next block i
            i0b = i1b;
            i0e = i1e;
        end
        
    end % backslash

% ---------------------------------------------------------------------------- %

% inSigma: True for points inside Sigma
% ''''''''
%   z      (complex) points
    function in = inSigma(z)
        
        if length(Sigma) == 2 && isreal(Sigma)
            realSigma = [Sigma(1);Sigma(1);Sigma(2);Sigma(2)];
            imagSigma = [-tolres;tolres;tolres;-tolres];
        else
            realSigma = real(Sigma);
            imagSigma = imag(Sigma);
        end
        in = inpolygon(real(z),imag(z),realSigma,imagSigma);
        
    end

% ---------------------------------------------------------------------------- %

% residual: Residual of the NLEP for given eigenvalues and eigenvectors
% '''''''''
%   Lambda  eigenvalues
%   X       eigenvectors (normalized)
    function R = residual(Lambda,X)
        
        R = zeros(length(Lambda),1);
        if Ahandle
            for ii = 1:length(Lambda)
                R(ii) = norm(funA(Lambda(ii))*X(:,ii));
            end
        else
            if p < 0
                BCX = reshape(CC*X,[],q,size(X,2));
            else
                BCX = reshape([BB*X;CC*X],[],p+q+1,size(X,2));
            end
            FL = cell2mat(cellfun(@(x) arrayfun(x,Lambda),[pf(:);f(:)]',...
                'UniformOutput',0));
            RR = sum(bsxfun(@times,BCX,reshape(FL.',1,size(BCX,2),[])),2);
            for ii = 1:length(Lambda)
                R(ii) = norm(RR(:,:,ii));
            end
        end
        
    end % residual

end % nleigs

% ---------------------------------------------------------------------------- %
% Subfunctions
% ---------------------------------------------------------------------------- %

function f = monomials(p)

f = cell(1,p+1);
for i = 1:p+1
    f{i} = @(x) x^(i-1);
end

end

function w = spmultiply(A,v)
% Write out sparse matrix-matrix(vector) mulitiplication using a for loop over
% the columns of A to avoid "out of memory" bug with small RAM.

try
    w = A*v;
catch %#ok<CTCH>
    w = sparse(size(A,1),size(v,2));
    for j = 1:size(A,2),
        w = w + A(:,j)*v(j,:);
    end
end

end
