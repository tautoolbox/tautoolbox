function [varargout] = ks(A,nev,which,tol,ncv)
% KS  Non-symmetric Krylov-Schur
%
% [V,D] = ks(A,nev,which,tol,ncv) returns nev eigenvalues and
% eigenvectors of A, where
%    which = 'lm'   largest magnitude
%    which = 'sm'   smallest magnitude
%    which = 'lr'   largest real part (rightmost eigenvalues)
%    which = 'sr'   smallest real part (leftmost eigenvalues)
%    tol: tolerance
%    ncv: maximum dimension of subspace
%
% Simple implementation: non-locking, twice orthogonalization
% coded by Jose E. Roman

if nargin<1, error('Not enough arguments'), end
n = size(A,1);
if size(A,2)~=n, error('Matrix is not square'), end
if nargin<2, nev = 4; end
if nargin<3, which = 'sm'; end
if nargin<4, tol = 1e-12; end
if nargin<5, ncv = 16; end

v0    = rand(n,1);
nconv = 0;
m = ncv;
H = zeros(m+1,m);
V = v0/norm(v0);

k = 0;  % size of leading block of H
outer = 0;

while nconv<nev

  outer = outer + 1;
  
  % 1. Build a Krylov decomposition AV(:,1:m)=VH
  for j=k+1:m
    f = A*V(:,j);
    y = V(:,1:j)'*f;
    f = f - V(:,1:j)*y;
    c = V(:,1:j)'*f;
    f = f - V(:,1:j)*c;
    H(1:j,j) = y + c;
    beta = norm(f);
    if beta<1e-12, error('breakdown'), end
    V(:,j+1) = f/beta;
    H(j+1,j) = beta;
  end
  B = H(1:m,1:m);
  b = H(m+1,1:m)';
  U = V(:,1:m);
  u = V(:,m+1);
  
  k = nconv + floor((m-nconv)/2);
  
  % 2. Compute Schur form of B
  [Y,S] = schur(B);

  % 3. Sort the Krylov-Schur decomposition
  e = ordeig(S);
  switch lower(which)
    case {'lm'}
      [~,p] = sort(abs(e),'descend');
    case {'sm'}
      [~,p] = sort(abs(e));
    case {'lr'}
      [~,p] = sort(real(e),'descend');
    case {'sr'}
      [~,p] = sort(real(e));
  end
  select = zeros(m,1);
  select(p(1:k)) = 1;
  [Y,S] = ordschur(Y,S,select);

  % 4. Truncate the decomposition
  if abs(S(k+1,k))>1e-14, k = k+1; end
  W = Y(:,1:k); 
  V = [U*W u];
  b = W'*b;
  H = [S(1:k,1:k); b'];
  
  % 5. Check convergence
  errest = abs(b);
  nconv = 0; 
  for i=1:k
    if errest(i)<tol, nconv=nconv+1; else, break, end
  end

end

H = H(1:nconv,1:nconv);
V = V(:,1:nconv);
[V,H] = rsf2csf(V,H);
[Z,D] = eig(H);
V = V*Z;

% eigenvalues
if nargout >= 0
    varargout{1} = diag(D);
end

% eigenvectors matrix
if  nargout > 1
    varargout{1} = V;
    varargout{2} = D;
end