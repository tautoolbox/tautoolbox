# External software bundled with Tau Toolbox

There are several functions that are need to run Tautoolbox and that for simplicity we bundle inside the Tau Toolbox in a special directory. We list each function together with the original URL and the respective license.

* `is_octave` [https://github.com/catch22/octave-doctest](https://github.com/catch22/octave-doctest)
 - License: BSD-3-Clause (enclosed in the file)

* `quadeig` [https://github.com/ftisseur/quadratic-eigensolver](https://github.com/ftisseur/quadratic-eigensolver) (matlab and octave versions)
 - License: 2-Clause-BSD (file:`quadeig_licence.txt`)
 
* `plotxx` [https://www.mathworks.com/matlabcentral/fileexchange/317-plotxx-m](https://www.mathworks.com/matlabcentral/fileexchange/317-plotxx-m)
  - License: BSD-3-Clause

* `nleigs` [http://twr.cs.kuleuven.be/research/software/nleps/nleigs.php] (http://twr.cs.kuleuven.be/research/software/nleps/nleigs.php)
 - License: GNU GPL v3.0 license (file:`nleigs_licence.txt`)