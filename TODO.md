# TODO
Version 0.92 : 2023-xx-yy

Tau Toolbox is a MATLAB/Octave library for the solution of integro-differential
problems based on the Lanczos' Tau method.

Tau Toolbox is free software released under the GNU Lesser General Public License version 3 (LGPLv3) along with Tau Toolbox. A copy of the License is enclosed in the project.

This file holds a series of issues that we are planning to be addressed for the next development cycles.


## Version 1

This is the list of tasks that we would like to complete before version 1.0.

* Documentation:
    1. User Guide (under development);
    2. Review and update functions documentation (inline);
    3. Update reference Tau Toolbox card.

* Ordinary integro-differential problems:
    a. #conditions vs orders of the equations for systems of differential equations;
    2. Evaluate quality of piecewise solutions;
    3. What is the best initial approximation for nonlinear problems?
    4. recheck tau.matrix functions.


## Version 2

* Orthogonal Polynomials:
    1. use barycentric weights for functions interpolation?

* Eigenvalue problems:
    1. System of differential equations are not yet available for eigenvalue problems
    2. Unwrapping to be extended for the nonlinear problems