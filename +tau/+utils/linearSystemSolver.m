classdef linearSystemSolver

    properties
        algorithm
        fx
        args
    end

    methods
        function obj = linearSystemSolver(varargin)
            obj = obj.validate(varargin{:});

            switch obj.algorithm
                case 'gauss elimination'  % backslash Matlab solver.
                    obj.fx = @tau.utils.linearSystemSolver.gaussElimination;
                case 'qr'                 % QR solver.
                    obj.fx = @tau.utils.linearSystemSolver.qrSolver;
                case 'gmres'              % gmres solver.
                    obj.fx = @(T,b) gmres(T, b, [], obj.args{:});
                case 'bicgstab'           % bicgstab solver.
                    obj.fx = @(T,b) bicgstab(T, b, obj.args{:});
                case 'bicgstabl'          % bicgstabl solver.
                    obj.fx = @(T,b) bicgstabl(T, b, obj.args{:});
                case 'bicg'               % bicg solver.
                    obj.fx = @(T,b) bicg(T, b, obj.args{:});
                case 'minres'             % minres solver.
                    obj.fx = @(T,b) minres(T, b, obj.args{:});
                case 'qmr'                % qmr solver.
                    obj.fx = @(T,b) qmr(T, b, obj.args{:});
                case 'tfqmr'              % tfqmr solver.
                    obj.fx = @(T,b) tfqmr(T, b, obj.args{:});
                case 'lsqr'               % lsqr solver.
                    obj.fx = @(T,b) lsqr(T, b, obj.args{:});
            end

        end

        function obj = validate(obj, varargin)
            persistent defaultSolver;
            persistent common;
            persistent mlb_iterative; % available in Matlab only
            persistent iterative;
            persistent solvers;

            if isempty(solvers)
                defaultSolver = 'gauss elimination';
                direct = {'gauss elimination', 'qr'};
                % iterative
                common = {'gmres', 'bicgstab', 'bicg', 'qmr', 'tfqmr'};
                mlb_iterative = {'bicgstabl', 'minres', 'lsqr'};
                iterative = union(common, mlb_iterative);
                solvers = union(direct, iterative);
            end

            if nargin == 1
                value = defaultSolver;
            else
                value = varargin{1};
                if ~ischar(value) && ~isstring(value) && ~iscell(value)
                    warning('tau.utils.linearSystemSolver: solver must be a string, setting default value')
                    value = defaultSolver;
                end
                value = lower(value);
            end

            if ~ismember(value, solvers)
                warning('tau.utils.linearSystemSolvers: unknown linear system solver, setting default value')
                obj.algorithm = defaultSolver;
            elseif ismember(value, mlb_iterative) && is_octave()
                obj.algorithm = common{1};
                warning('tau.utils.linearSystemSolvers: Octave does not support "%s" linear system solver, setting it to "%s"', value, obj.algorithm)
            else
                obj.algorithm = value;
            end

            if ismember(value, iterative) && nargin > 2
                obj.args = value(2:end);
            else
                obj.args = {};
            end
        end

        function display(obj)
            disp    ('        Linear System Solver')
            fprintf ('       algorithm:  %s\n', obj.algorithm)
            fprintf ('              fx:  %s\n', disp(obj.fx))
        end

        function [yn, iter] = solve(obj, T, b, problem, options)
            %solve - Solve a system of linear algebraic equations.
            %   solve(T, b, options) returns a tau.polynomial, with the same basis
            %   defined in options and with coefficients given by T^(-1)*obj.coeff.
            %
            %   A specific solver and a preconditioner can be selected in options.
            %
            %input (required):
            %   T       = linear system matrix
            %   b       = independent terms
            %   problem = tau.problem object
            %
            %input (optional):
            %   options = tau.settings object (in case it differs from problem.options)
            %
            %output:
            %   yn      = tau.polynomial with coefficients given by T^(-1)*b
            %   iter    = number of iterations (for iterative methods)

            if nargin < 5
                options = problem.options;
            end

            % solve the linear system
            [a, ~, ~, iter] = obj.fx(T, b);

            % reshape the coefficients with the right dimensions
            a = reshape(a, [], problem.nequations, options.npieces);
            yn = tau.polynomial(a, options);
        end
    end

    methods (Static = true)
        function [a,b,c,iter] = gaussElimination(T, b)
            a = T\b;
            b = [];
            c = [];
            iter = 0;
        end

        function [a,b,c,iter] = qrSolver(T, b)
            [Q, R] = qr(T);
            a = R\(Q'*b);
            b = [];
            c = [];
            iter = 0;
        end
    end
end
