function spy(T)
%tau.spy(T) - Plots a 3D spy of matrix T
% tau.spy(T) improved sparse matrix structure representation

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

[m, n] = size(T);
nz = nnz(T);
box on; hold on
d = 0.2;
for i = 1:m
    for j = 1:n
        if T(i, j) ~= 0
            surf([j-0.5+d j+0.5-d;j-0.5+d j+0.5-d], ...
                 [i-0.5+d i-0.5+d;i+0.5-d i+0.5-d], ...
                 [T(i, j) T(i, j); T(i, j) T(i, j)])
        end
    end
end
xlabel(['nz = ' num2str(nz)])
axis ij
axis('equal')
xlim([0 n+0.8+d]);
ylim([0 m+0.8+d]);
colorbar('EastOutside');

end
