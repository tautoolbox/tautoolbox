function V = normalize(A)
%normalize - function to normalize the columns of a matrix
%   V = normalize(A) normalizes the columns of A.
%
% This function is useful to normalize eigenvectors.
%
%input:
%   A = matrix to normalize by columns
%
%output:
%   V = normalized matrix
%
%examples:
%   A = gallery ("chebvand", 6);
%   [V, ~] = eig((A+A')/2);
%   % this is the same as V./vecnorm(V) for Octave and for Matlab older than 2016a
%   V = tau.utils.normalize(V);

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    if is_octave() || ~verLessThan('matlab', '9.1')
        V = A./vecnorm(A);
    else
        V = A./repmat(vecnorm(A), size(A,1), 1);
    end
end
