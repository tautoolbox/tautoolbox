function params = parse_schur_params(name, varargin)
    persistent p
    if isempty(p)
        p = inputParser ();
        p.FunctionName = name;

        p.addParameter('precision', 10*eps, @(n) isscalar(n) && isnumeric(n))
        p.addParameter('maxDim', 256, @(n) isscalar(n) && isnumeric(n))
        p.addParameter('sizeBlock', 10, @(n) isscalar(n) && isnumeric(n))
        p.addParameter('initialLU', 32, @(n) isscalar(n) && isnumeric(n))
    end
    p.parse(varargin{:});
    params = p.Results;
end

