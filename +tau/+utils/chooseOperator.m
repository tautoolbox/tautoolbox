function opSel = chooseOperator(op)
%chooseOperator - returns a cell list of operators where op fits
%
%input (required):
%   op         = operator (string)
%
%output:
%   opSel      = cell arrays of candidates that satisfy condition

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    persistent opList;
    if isempty(opList)
        opList = {'m', 'multiplication', 'n', 'differentiation', ...
                  'o', 'integration',    'c', 'condition', ...
                  'd', 'operator',       't', 'taumatrix', ...
                  'r', 'tauresidual',    'b', 'taurhs', ...
                  'v', 'orth2pow',       'w', 'pow2orth'};
    end
    % ensure that the first argument is char or string
    if ~ischar(op) && ~isstring(op)
        error('tau.utils.chooseOperator: prescribed operation must a char/string (see help tau.matrix)')
    end
    op = lower(op);

    % test exact match
    pos = strcmp(op, opList);
    if any(any(pos))
        opSel = opList(pos);
        if ischar(opSel) || isstring(opSel)
            return
        end
        opSel = opSel{1};
        return
    end

    % test approximate match
    pos = strncmp(op, opList, length(op));

    opSel = opList(pos);
    if length(opSel) == 1
        opSel = opSel{1};
        return
    end

    if isempty(opSel)
        error('tau.utils.chooseOperator: the 1st input argument must be a prescribed operation (see help tau.matrix)')
    end

    error('tau.utils.chooseOperator: operator name "%s" is ambiguous, possible matches are:\n\t%s.', op, strjoin(opList(pos), ', '))
end
