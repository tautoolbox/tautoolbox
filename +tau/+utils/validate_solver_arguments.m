function validate_solver_arguments(problem, name, multiple_eq, nonlin, mpieces, givp)
%validate_solver_arguments - validates if the solver is suitable to solve the problem
%
%input (required):
%   problem          = problem being studied (tau.problem object)
%   name             = solver's name (string or char array)
%   multiple_eq      = solver supports multiple equations (boolean)
%   nonlin           = solver supports nonlinear problems
%   mpieces          = solver supports multiple pieces
%   givp             = solver supports only conditions on the first piece

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

msg = {};
n = 0; % number of errors detected

if ~isa(problem, 'tau.problem')
    n = n + 1;
    msg{n} = ' * the input argument must be a tau.problem';
end

if multiple_eq == false && problem.nequations > 1
    n = n + 1;
    msg{n} = ' * this solver does not handle systems of equations';
end

if nonlin == false && problem.isLinearized
    n = n + 1;
    msg{n} = ' * this solver does not handle nonlinear problems';
end

if mpieces == false && problem.npieces > 1
    n = n + 1;
    msg{n} = ' * this solver does not handle piecewise problems';
end

if givp == true && ~problem.isGIVP
    n = n + 1;
    msg{n} = ' * does not handle piecewise problems with conditions in pieces other than the first';
end

if n == 0
    return
end

msg = [{[name, ' does not handle this problem because:']}, msg(:)'];
error(strjoin(msg, '\n'))
end

