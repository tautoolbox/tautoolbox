function params = parse_nonlinear_params(name, varargin)
    persistent p
    if isempty(p)
        p = inputParser ();
        p.FunctionName = name;

        p.addParameter('tol', 1e-10, @(n) isscalar(n) && isnumeric(n))
        p.addParameter('maxIter', 100, @(n) isscalar(n) && isnumeric(n))
        p.addParameter('steps', 100, @(n) isscalar(n) && isnumeric(n))
    end
    p.parse(varargin{:});
    params = p.Results;
end

