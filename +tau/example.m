function varargout = example(varargin)
%tau.example - Get and run Tautoolbox examples.
%
%   tau.example() prints all the examples available in the Tautoolbox
%   out = tau.example() returns a cell array of examples
%   tau.example(tau_example)
%   tau.example(tau_example, 'run')
%   tau.example(tau_example, 'info')
%   tau.example(tau_example, 'list')
%
% input (optional):
%   tau_example = specify operator heigth (can set 0 if the degree is to be kept)
%
%output:
%   out         = returns a cell array of examples
%
%examples:
%   tau.example('tau_logo')  % run the Tautoolbox logo example
%   out = tau.example(); % gets a cell array with the examples

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    persistent example_list
    persistent examples_idx
    persistent examples
    persistent allowed_cmd
    persistent tau_path

    if isempty(example_list)
        tau_path = tauPath();
        allowed_cmd = {'info', 'list', 'run'};
        fileID = fopen(fullfile(tau_path, 'examples/examples.csv'),'r');
        C = textscan(fileID,'%s %s %s %s %s','Delimiter',',', 'HeaderLines', 1 );
        fclose(fileID);
        example_list = [C{:}];
        examples_idx = containers.Map(example_list(:,1), (1:size(example_list,1))');
        fields = {'name', 'category', 'type', 'basis', 'directory'};
        examples = cell2struct(example_list', fields);
    end

    if nargin > 2
        error('tau.example: wrong number of arguments, at most 2 expected')
    end

    if nargin == 0
        if nargout == 1
            varargout{1} = example_list(:,1);
        else
            disp(example_list(:,1))
        end
        return
    end

    % Get the example name
    name = varargin{1};

    if ~ismember(name, examples_idx.keys())
        error('tau.example: "%s" is not a Tautoolbox example', name)
    end

    if nargin == 2
        cmd = varargin{2};
        if ~ismember(cmd, allowed_cmd)
            warning('tau.example: unknown command, assuming *run*')
            cmd = 'run';
        end
    else
        cmd = 'run';
    end

    choosen_example = examples(examples_idx(name));
    choosen_path = fullfile(tau_path, 'examples', choosen_example.directory);
    switch cmd
        case 'info'
            varargout{1} = choosen_example;
        case 'list'
            disp(fileread(fullfile(choosen_path, [choosen_example.name '.m'])))
        case 'run'
            run(fullfile(choosen_path, [choosen_example.name '.m']))
    end
end
