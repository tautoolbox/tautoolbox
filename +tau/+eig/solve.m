function [varargout] = solve(problem)
%solve - Lanczos' Tau solver drive for differential eigenvalue problems.
%   tau.eig.solve returns the eigenpairs for a (n-1)th degree polynomial
%   approximation in the Tau sense.
%   For now it can solve polynomial eigenproblems:
%   F(lambda)(x)=(A0+lambda*A1+lambda^2*A2+...+lambda^k*Ak)x=0.
%
%   [lambda, V] = tau.eig.solve(equation, conditions, varargin)
%
%input (required):
%   equation            = ordinary differential equation (char).
%   domain              = interval.
%   conditions          = problem conditions (cell of char).
%
%input (optional):
%   options                = tau solver options (see tau.settings).
%
%output:
%   lambda         = eigenvalues (vector)
%   V              = eigenvectors (matrix)
%   R              = tau residual matrix
%
%See also LEP, QEP, PEP, NEP.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

if ~isa(problem, 'tau.problem')
    error('tau.eig.solve: the input argument must be a tau.problem')
end

switch length(problem.equations{1}.rhs)
    case 1    % LEP
        [varargout{1:nargout}] = tau.eig.LEP(problem);
    case 2    % QEP
        [varargout{1:nargout}] = tau.eig.QEP(problem);
    otherwise % PEP
        [varargout{1:nargout}] = tau.eig.PEP(problem);
end

end
