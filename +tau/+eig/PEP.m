function [varargout] = PEP(problem)
%PEP - Lanczos' tau method for Polynomial Eigenvalue Problem.
%   lambda = tau.eig.PEP(equation, domain, conditions, options) returns the eigenvalues
%   of the differential eigenvalue problem
%
%input (required):
%   problem  = contains ordinary differential equation, domain and
%              conditions.
%
%output:
%   lambda   = eigenvalues ordered from smallest to largest (vector).
%   V        = eigenvectors (tau polynomial).
%   R        = Tau residual (tau polynomial).
%
% See also LEP, QEP, NEP.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

if ~isa(problem, 'tau.problem')
    error('tau.eig.PEP: the input argument must be a tau.problem')
end

n = problem.n;
nu = problem.nconditions;

% build Tau matrix T
T = tau.eig.matrix('T', problem);

% adjust T dimensions to required degree
for i = 1:length(T)
    T{i} = T{i}(1:n, 1:n);
end

% is T empty?
for i = 2:length(T)
    if isempty(T{i}), T{i} = zeros(n); end
end

% solve the problem
if nargout < 2
    lambda = polyeig(T{:});
else
    [V, lambda] = polyeig(T{:});
end
[~,p] = sort(abs(lambda));
lambda = lambda(p(1:(length(T)-1)*(n-nu)));

% eigenvalues
if nargout >= 0
    varargout{1} = lambda;
end

% eigenvectors as tau polynomials
if nargout > 1
    V = V(:, p(1:(length(T)-1)*(n-nu)));
    varargout{2} = tau.polynomial(V, problem.options);
end

% tau residual as tau polynomials
if nargout > 2
    R = tau.eig.matrix('R', problem);
    [nu, n] = size(R{1});
    Taures = zeros(nu, n);
    for j = 1:length(lambda)
        aux = R{1};
        for i = 2:length(R)
            if ~isempty(R{i})
                aux = aux + lambda(j)^j*R{i};
            end
        end
        Taures(:, j) = aux*V(:, j);
    end
    if nu ~= n
        % extend vector (eg. tau residual) to dimension n
        Taures = [zeros(n-nu, length(lambda)); Taures];
    end
    varargout{3} = tau.polynomial(Taures, problem.options);
end

if nargout > 3
    error('tau.eig.PEP: wrong number of output arguments')
end

end
