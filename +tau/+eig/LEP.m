function [varargout] = LEP(problem, varargin)
%LEP - Lanczos' tau method for Linear Eigenvalue Problem.
%   lambda = tau.eig.LEP(problem) returns the eigenvalues
%   of the differential eigenvalue problem
%
%input (required):
%   problem  = contains ordinary differential equation, domain and
%              conditions.
%
%output:
%   lambda   = eigenvalues ordered from smallest to largest (vector).
%   V        = eigenvectors (tau polynomial).
%   R        = Tau residual (tau polynomial).
%
% See also NEP, PEP, QEP.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

if ~isa(problem, 'tau.problem')
    error('tau.eig.LEP: the input argument must be a tau.problem')
end

persistent solvers
persistent trigger
persistent p
if isempty(solvers)
    solvers = {'qr', 'qz', 'schur qr', 'schur qz', 'arnoldi', ...
               'arnoldi generalized', 'jacobi-davidson'};
    trigger = {'on', 'off'}
    p = inputParser ();
    p.FunctionName = 'tau.eig.LEP';

    p.addParameter('solver', 'schur qr', @(s) ismember(s, solvers));
    p.addParameter('split', 'off', @(s) ismember(s, trigger))
    p.addParameter('deflation', 'off', @(s) ismember(s, trigger))
    p.addParameter('deflatevalue', 0, @(n) isscalar(n) && isnumeric(n))
    p.addParameter('neigs', inf, @(n) isscalar(n) && isnumeric(n))
    p.addParameter('target', 'off', @(s) ismember(s, trigger))
    p.addParameter('targetvalue', 1, @(n) isscalar(n) && isnumeric(n))
end

p.parse (varargin{:});
options = p.Results;
options.n = problem.n;
options.nu = problem.nconditions;

% build Tau matrix T
T = tau.eig.matrix('T', problem);

if nargout < 2
    lambda = tau.eig.solveLEP(T, options);
else
    [lambda, V] = tau.eig.solveLEP(T, options);
end

% eigenvalues
if nargout >= 0
    varargout{1} = lambda;
end

% eigenvectors as tau polynomials
if  nargout > 1
    varargout{2} = tau.polynomial(V, problem.options);
end

% tau residual as tau polynomials
if nargout > 2
    R = tau.eig.matrix('R', problem);
    [nu, n] = size(R{1});
    Taures = zeros(nu, n);
    for j = 1:length(lambda)
        Taures(:, j) = (R{1} + lambda(j)*R{2})*V(:, j);
    end
    if nu ~= n
        % extend vector (eg. tau residual) to dimension n
        Taures = [zeros(n-nu, n); Taures];
    end
    varargout{3} = tau.polynomial(Taures, problem.options);
end

if nargout > 3
    error('tau.eig.LEP: wrong number of output arguments')
end

end
