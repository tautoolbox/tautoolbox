function [lambda, V] = solveLEP(T, options)
%tau.solveLEP - Solve a linear eigenvalue problem T*x=lambda*x.
%   lambda = solveLEP(T) returns the vector of eigenvalues, and
%   [lambda, V] = solveLEP(T) returns also the eigenvectors.
%
%input (required):
%   T        = matrix.
%   options  = nb. of conditions, nb. of polynomial terms, solver,
%              split, deflation, deflatevalue
%
%output:
%   lambda   = eigenvalues (vector)
%   V        = eiganvectors (tau.polynomials)

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

warning('off', 'backtrace')

if length(T)~=2 || isempty(T{1}) || isempty(T{2})
    error('tau.eig.LEP: there is a problem with T')
end

% set working dimensions
n = options.n;
nu = options.nu;
k = options.neigs;
sigma = options.targetvalue;

tolcond = 1e14; % tolerance for the condition estimator

if strcmpi(options.solver, 'schur qr') || strcmpi(options.solver, 'schur qz')

    % set block matrices
    S =  T{1}(1:nu, 1:nu)\T{1}(1:nu, nu+1:end);
    A =  T{1}(nu+1:n, nu+1:end)-T{1}(nu+1:n, 1:nu)*S;
    B =  T{2}(nu+1:n, nu+1:end)-T{2}(nu+1:n, 1:nu)*S;

    % permuted matrices
    if strcmpi(options.split, 'on')
        nn = size(A, 1);
        ps = [1:2:nn 2:2:nn];  % perfect shuffle permutation
        Aps = A(ps, ps); Bps = B(ps, ps);
        % diagonal blocks
        hf = ceil(nn/2);
        A1 = Aps(1:hf, 1:hf); A2 = Aps(hf+1:nn, hf+1:nn);
        B1 = Bps(1:hf, 1:hf); B2 = Bps(hf+1:nn, hf+1:nn);
        if norm(Aps(1:hf, hf+1:nn)) || norm(Aps(hf+1:nn, 1:hf)) || ...
           norm(Bps(1:hf, hf+1:nn)) || norm(Bps(hf+1:nn, 1:hf)) > 1e-14
           error('tau.eig.LEP: split cannot be used, problem is nonsymmetric')
        end
    end

end

switch options.solver

    case 'qr'

        if condest(T{1}) > tolcond
            warning('tau.eig.LEP: high condition number; maybe try "qz" solver')
        end

        if nargout < 2
            lambda = eig(-T{1}\T{2}); lambda = 1./lambda;
            [~, p] = sort(abs(lambda)); lambda = lambda(p);
            lambda = lambda(1:end-nu);
            if k < inf
                lambda = lambda(1:k);
            end
        else
            [V, D] = eig(-T{1}\T{2}); lambda = 1./diag(D);
            [~, p] = sort(abs(lambda)); lambda = lambda(p);
            lambda = lambda(1:end-nu);
            V = V(:, p);
            if k < inf
                lambda = lambda(1:k); V = V(:, 1:k);
            end
        end

    case 'qz'

        if nargout < 2
            lambda = eig(T{1}, -T{2});
            [~,p] = sort(abs(lambda)); lambda = lambda(p);
            lambda = lambda(1:end-nu);
            if k < inf
                lambda = lambda(1:k);
            end
        else
            [V, D] = eig(T{1}, -T{2}); lambda = diag(D);
            [~, p] = sort(abs(lambda)); lambda = lambda(p);
            lambda = lambda(1:end-nu);
            V = V(:,p);
            if k < inf
                lambda = lambda(1:k); V = V(:, 1:k);
            end
            V = tau.utils.normalize(V);
        end

    case 'arnoldi'

        if k == inf, k = 6; end
        if condest(T{1}) > tolcond
            warning('tau.eig.LEP: high condition number; maybe try "arnoldi generalized" solver')
        end
        if nargout < 2
            if strcmpi(options.target, 'on')
                lambda = eigs(-T{1}\T{2}, k, sigma);
            else
                lambda = eigs(-T{1}\T{2}, k);
            end
            lambda = 1./lambda; [~,p] = sort(abs(lambda));
            lambda = lambda(p);
        else
            if strcmpi(options.target, 'on')
                [V, D] = eigs(-T{1}\T{2}, k, sigma);
            else
                [V, D] = eigs(-T{1}\T{2}, k);
            end
            lambda = 1./diag(D); [~, p] = sort(abs(lambda));
            lambda = lambda(p); V = V(:,p);
        end

    case 'arnoldi generalized'

        if k == inf, k = 6; end
        if nargout < 2
            if strcmpi(options.target, 'on')
                lambda = eigs(T{1}, -T{2}, k, sigma);
            else
                lambda = eigs(T{1}, -T{2}, k, 'smallestabs');
            end
            [~,p] = sort(abs(lambda)); lambda = lambda(p);
        else
            if strcmpi(options.target, 'on')
                [V, D] = eigs(T{1}, -T{2}, k, sigma);
            else
                [V, D] = eigs(T{1}, -T{2}, k, 'smallestabs');
            end
            lambda = diag(D); [~, p] = sort(abs(lambda));
            lambda = lambda(p); V = V(:,p);
        end

    case 'jacobi-davidson'

        if k == inf, k = 6; end
        if nargout < 2
            if strcmpi(options.target, 'on')
                lambda = jdqr(-T{1}\T{2}, k, sigma);
            else
                lambda = jdqr(-T{1}\T{2}, k, 'lr');
            end
            lambda = 1./lambda; [~,p] = sort(abs(lambda));
            lambda = lambda(p);
        else
            if strcmpi(options.target, 'on')
                [V, D] = jdqr(-T{1}\T{2}, k, sigma);
            else
                [V, D] = jdqr(-T{1}\T{2}, k, 'lr');
            end
            lambda = 1./diag(D); [~, p] = sort(abs(lambda));
            lambda = lambda(p); V = V(:,p);
        end

    case 'schur qr'

        % split
        if strcmpi(options.split, 'on') && strcmpi(options.deflation, 'off')
            condA1 = condest(A1); condA2 = condest(A2);
            if condA1 > tolcond || condA2 > tolcond
                warning('tau.eig.LEP: high condition numbers; maybe try "qz" solver')
            end
            if nargout < 2
                lambda = [1./eig(-A1\B1); 1./eig(-A2\B2)];
                [~, p] = sort(abs(lambda)); lambda = lambda(p);
                if k < inf
                    lambda = lambda(1:k);
                end
            else
                [V1, D1] = eig(-A1\B1); [V2, D2] = eig(-A2\B2);
                lambda = [1./diag(D1); 1./diag(D2)];
                [~, p] = sort(abs(lambda)); lambda = lambda(p);
                V = [V1 zeros(size(V1, 1), size(V2, 2)); zeros(size(V2, 1), size(V1, 2)) V2];
                V = V(ps, ps);
                V = tau.utils.normalize([-S*V; V]);
                V = V(:, p);
                if k < inf
                    lambda = lambda(1:k); V = V(:, 1:k);
                end
                warning('tau.eig.LEP: eigenvectors for Schur QR split not yet implemented')
                V = zeros(1, size(V, 2)); %todo
            end
        end

        % deflated
        if strcmpi(options.split, 'off') && strcmpi(options.deflation, 'on')
            condA = condest(A); %condB = condest(B);
            if condA > tolcond
                warning('tau.eig.LEP: high condition numbers; try solver="qz"')
            end
            if nargout <2
                [V, ~] = eigs(-A\B, 1);
                sigma = options.deflatevalue; w = B*V(:, 1);
                lambda = eig(-(A+sigma*(w*w'))\B); lambda = 1./lambda;
                [~, p] = sort(abs(lambda)); lambda = lambda(p);
                if k < inf
                    lambda = lambda(1:k);
                end
            else
                [V, ~] = eigs(-A\B, 1);
                sigma = options.deflatevalue; w = B*V(:, 1);
                [V, D] = eig(-(A+sigma*(w*w'))\B); lambda = 1./diag(D);
                [~, p] = sort(abs(lambda)); lambda = lambda(p);
                if k < inf
                    lambda = lambda(1:k); V = V(:, 1:k);
                end
                warning('tau.eig.LEP: eigenvectors for Schur QR with deflation not yet implemented')
                V = zeros(1, size(V, 2)); %todo
            end
        end

        % split and deflated
        if strcmpi(options.split, 'on') && strcmpi(options.deflation, 'on')
            condA1 = condest(A1); %condB1 = condest(B1);
            condA2 = condest(A2); %condB2 = condest(B2);
            if condA1 > tolcond || condA2 > tolcond
                warning('tau.eig.LEP: high condition numbers; maybe try solver="qz"')
            end
            if nargout < 2
                [V, ~] = eigs(-A1\B1, 1);
                sigma = options.deflatevalue; w = B1*V(:, 1);
                lambda1 = eig(-(A1+sigma*(w*w'))\B1); lambda2 = eig(-A2\B2);
                lambda = [1./lambda1; 1./lambda2];
                [~, p] = sort(abs(lambda)); lambda = lambda(p);
                if k < inf
                    lambda = lambda(1:k);
                end
            else
                [V, ~] = eigs(-A1\B1, 1);
                sigma = options.deflatevalue; w = B1*V(:, 1);
                [V1, D1] = eig(-(A1+sigma*(w*w'))\B1);
                [V2, D2] = eig(-A2\B2);
                lambda = [1./diag(D1); 1./diag(D2)]; % [V2, D2] = eig(-A2\B2); lambda = [diag(D1); 1./diag(D2)];
                [~, p] = sort(abs(lambda)); lambda = lambda(p);
                V = [V1 zeros(size(V1, 1), size(V2, 2)); zeros(size(V2, 1), size(V1, 2)) V2];
                V = V(ps, ps);
                V = tau.utils.normalize([-S*V; V]);
                V = V(:, p);
                if k < inf
                    lambda = lambda(1:k); V = V(:, 1:k);
                end
                warning('tau.eig.LEP: eigenvectors for Schur QR with split and deflation not yet implemented')
                V = zeros(1, size(V, 2)); %todo
            end
        end

        % general
        if strcmpi(options.split, 'off') && strcmpi(options.deflation, 'off')
            condA = condest(A); condB = condest(B);
            if condA > tolcond || condB > tolcond
                warning('tau.eig.LEP: high condition numbers; try "qz" solver')
            end
            if condA < tolcond
                if nargout < 2
                    lambda = 1./eig(-A\B);
                    [~, p] = sort(abs(lambda)); lambda = lambda(p);
                    if k < inf
                        lambda = lambda(1:k);
                    end
                else
                    [V, lambda] = eig(-A\B); lambda = 1./diag(lambda);
                    [~, p] = sort(abs(lambda)); lambda = lambda(p);
                    V = tau.utils.normalize([-S*V; V]);
                    V = V(:, p);
                    if k < inf
                        lambda = lambda(1:k); V = V(:, 1:k);
                    end
                end
            else
                warning('tau.eig.LEP: A has high condition number; trying eig(-B\A) ...')
                if nargout < 2
                    lambda = eig(-B\A);
                    [~, p] = sort(abs(lambda)); lambda = lambda(p);
                    if k < inf
                        lambda = lambda(1:k);
                    end
                else
                    [V, lambda] = eig(-B\A); lambda = diag(lambda);
                    [~, p] = sort(abs(lambda)); lambda = lambda(p);
                    V = tau.utils.normalize([-S*V; V]);
                    V = V(:, p);
                    if k < inf
                        lambda = lambda(1:k); V = V(:, 1:k);
                    end
                end
            end
        end

    case 'schur qz'

        % split
        if strcmpi(options.split, 'on') && strcmpi(options.deflation, 'off')
            if nargout < 2
                lambda = [eig(A1, -B1); eig(A2, -B2)];
                [~, p] = sort(abs(lambda)); lambda = lambda(p);
                if k < inf
                    lambda = lambda(1:k);
                end
            else
                [V1, D1] = eig(A1, -B1); [V2, D2] = eig(A2, -B2);
                lambda = [diag(D1); diag(D2)];
                [~, p] = sort(abs(lambda)); lambda = lambda(p);
                V = [V1 zeros(size(V1, 1), size(V2, 2)); zeros(size(V2, 1), size(V1, 2)) V2];
                V = V(ps, ps);
                V = tau.utils.normalize([-S*V; V]);
                V = V(:, p);
                if k < inf
                    lambda = lambda(1:k); V = V(:, 1:k);
                end
                warning('tau.eig.LEP: eigenvectors for Schur QZ split not yet implemented')
                V = zeros(1, size(V, 2)); %todo
            end
        end

        % deflated
        if strcmpi(options.split, 'off') && strcmpi(options.deflation, 'on')
            if nargout < 2
                [V, ~] = eigs(A, -B, 1);
                sigma = options.deflatevalue; w = B*V(:,1);
                lambda = eig(A+sigma*(w*w'), -B);
                [~, p] = sort(abs(lambda)); lambda = lambda(p);
                if k < inf
                    lambda = lambda(1:k);
                end
            else
                [V, ~] = eigs(A, -B, 1);
                sigma = options.deflatevalue; w = B*V(:,1);
                [V, D] = eig(A+sigma*(w*w'), -B);
                lambda = diag(D); [~,p] = sort(abs(lambda)); lambda = lambda(p);
                if k < inf
                    lambda = lambda(1:k); V = V(:, 1:k);
                end
                warning('tau.eig.LEP: eigenvectors for Schur QR with deflation not yet implemented')
                V = tau.utils.normalize([-S*V; V]);
                V = V(:, p);
                V = zeros(1, size(V, 2)); %todo
            end
        end

        % split and deflated
        if strcmpi(options.split, 'on') && strcmpi(options.deflation, 'on')
            if nargout < 2
                [V, ~] = eigs(A1, -B1, 1);
                sigma = options.deflatevalue;
                w = B1*V(:, 1);
                lambda1 = eig(A1+sigma*(w*w'), -B1); lambda2 = eig(A2, -B2);
                lambda = [lambda1; lambda2];
                [~, p] = sort(abs(lambda)); lambda = lambda(p);
                if k < inf
                    lambda = lambda(1:k);
                end
            else
                [V, ~] = eigs(A1, -B1, 1);
                sigma = options.deflatevalue;
                w = B1*V(:, 1);
                [V1, D1] = eig(A1+sigma*(w*w'), -B1); [V2, D2] = eig(A2, -B2);
                lambda = [diag(D1); diag(D2)];
                [~, p] = sort(abs(lambda)); lambda = lambda(p);
                V = [V1 zeros(size(V1, 1), size(V2, 2)); zeros(size(V2, 1), size(V1, 2)) V2];
                V = V(ps, ps);
                V = tau.utils.normalize([-S*V; V]);
                V = V(:, p);
                if k < inf
                    lambda = lambda(1:k); V = V(:, 1:k);
                end
                V = zeros(1, size(V, 2)); %todo
            end
        end

        % general
        if strcmpi(options.split, 'off') && strcmpi(options.deflation, 'off')
            if nargout < 2
                lambda = eig(A, -B);
                [~, p] = sort(abs(lambda)); lambda = lambda(p);
                if k < inf
                    lambda = lambda(1:k);
                end
            else
                [V, D] = eig(A, -B); lambda = diag(D);
                [~, p] = sort(abs(lambda)); lambda = lambda(p);
                V = tau.utils.normalize([-S*V; V]);
                V = V(:, p);
                if k < inf
                    lambda = lambda(1:k); V = V(:, 1:k);
                end
            end
        end

    otherwise
        error('tau.eig.solveLEP: wrong tau.settings.solver input specfication')

end

end
