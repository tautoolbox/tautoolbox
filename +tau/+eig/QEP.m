function [varargout] = QEP(problem)
%QEP - Lanczos' tau method for Quadratic Eigenvalue Problem.
%   lambda = tau.eig.QEP(equation, domain, conditions, options) returns the eigenvalues
%   of the differential eigenvalue problem
%
%input (required):
%   problem  = contains ordinary differential equation, domain and
%              conditions.
%
%output:
%   lambda   = eigenvalues ordered from smallest to largest (vector).
%   V        = eigenvectors (tau polynomial).
%   R        = Tau residual (tau polynomial).
%
% See also LEP, PEP, NEP.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

if ~isa(problem, 'tau.problem')
    error('tau.eig.QEP: the input argument must be a tau.problem')
end

% build Tau matrix T
T = tau.eig.matrix('T', problem);

% solve the problem
[V, lambda] = quadeig(T{3}, T{2}, T{1});
[~,p] = sort(abs(lambda));
lambda = lambda(p(1:2*(problem.n-problem.nconditions))); % remove the last 2*nu eigenvalues

% eigenvalues
if nargout >= 0
    varargout{1} = lambda;
end

% eigenvectors as tau polynomials
if nargout > 1
    V = V(:, p(1:2*(problem.n-problem.nconditions)));
    varargout{2} = tau.polynomial(V, problem.options);
end

% tau residual as tau polynomials
if nargout > 2
    R = tau.eig.matrix('R', problem);
    [nu, n] = size(R{1});
    Taures = zeros(nu, n);
    for j = 1:length(lambda)
        aux = R{1};
        for i = 2:length(R)
            if ~isempty(R{i})
                aux = aux + lambda(j)^j*R{i};
            end
        end
        Taures(:, j) = aux*V(:, j);
    end
    if nu ~= n
        % extend vector (eg. tau residual) to dimension n
        Taures = [zeros(n-nu, length(lambda)); Taures];
    end
    varargout{3} = tau.polynomial(Taures, problem.options);
end

if nargout > 3
    error('tau.eig.QEP: wrong number of output arguments')
end

end
