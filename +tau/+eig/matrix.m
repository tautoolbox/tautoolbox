function out = matrix(op, problem, h)
%tau.eig.matrix - get matrix for the operation op.
%   mat = tau.ode.matrix(op, problem) returns the matrix
%   representing the operation op given a differential problem.
%
%input (required):
%   op         = operation ('mult' or 'M', 'diff' or 'N', 'int' or 'O',
%                'C' or 'cond', 'D' or 'operator', 'T' or 'taumatirx',
%                'R' or 'tauresidual', 'b' or 'taurhs').
%   problem    = ordinary differential problem (tau object);
%                (ode, domain, conditions) or , (ode, domain, conditions, options).
%
% input (optional):
%   h          = specify operator heigth (can set 0 if the degree is to be kept)
%
%output:
%   out        = returns the matrix representing the operation op,
%                using the polynomial given basis.
%
%example:
%   out = tau.eig.matrix('T', problem);

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

op = tau.utils.chooseOperator(op);

if ~isa(problem, 'tau.problem')
    error('tau.eig.matrix: the 2nd input argument must be a tau.problem')
end

if nargin == 3 && ~isnumeric(h)
    error('tau.eig.matrix: the 3rd input argument must be an integer (see help tau.matrix')
end

if problem.nequations ~= 1
    error ('tau.eig.matrix: tau.eig.matrix only prepared for one equation.')
end

% set definitions
equation = problem.equations{1};
conditions = problem.conditions;
options = problem.options;

n = options.n;
nu = problem.nconditions;

% evaluate operator heigth
if nargin<3
    h = problem.height{1};
end

% build with extended size to accomodate height
options.n = n+nu+h;
x = tau.polynomial(options);
y = tau.operator(options);

basis = polynomial.basis(problem.options);
% get the required matrix
switch op
   case {'condition', 'c'}
        out = basis.systemBlock(conditions, problem.n, 1);
   case {'operator', 'd'}
        out = cell(1,1+length(equation.rhs));
        operator = equation.lhs(x, y);
        D = operator.mat;
        out{1} = D(1:n-nu, 1:n);
        for i = 1:length(equation.rhs)
            if ~isempty(equation.rhs{i})
                operator = -equation.rhs{i}(x, y);
                D = operator.mat;
                out{i+1} = D(1:n-nu, 1:n);
            end
        end
   case {'taumatrix', 't'}
        C = basis.systemBlock(conditions, options.n, 1);
        D = cell(1,1+length(equation.rhs));
        D{1} = equation.lhs(x, y);
        for i = 1:length(equation.rhs)
            if ~isempty(equation.rhs{i})
                D{i+1} = -equation.rhs{i}(x, y);
            end
        end
       out = cell(1,1+length(equation.rhs));
       T = [C; D{1}.mat];
       out{1} = T(1:n, 1:n);
       for i = 2:length(D)
           if ~isempty(D{i})
               T = [zeros(nu, size(D{i}.mat, 2)); D{i}.mat];
               out{i} = T(1:n, 1:n);
           else
               out{i} = [];
           end
       end
   case {'tauresidual', 'r'}
        out = cell(1,1+length(equation.rhs));
        R = equation.lhs(x, y);
        R = R.mat; out{1} = R(n-nu+1:n+h, 1:n); %%
        for i = 1:length(equation.rhs)
            if ~isempty(equation.rhs{i})
                R = -equation.rhs{i}(x, y);
                R = R.mat;
                out{i+1} = R(n-nu+1:n+h, 1:n); %%
            end
        end
    case {'multiplication', 'm'}
        out = basis.matrixM(problem.n);
    case {'differentiation', 'n'}
        out = basis.matrixN(problem.n);
    case {'integration', 'o'}
        out = basis.matrixO(problem.n);
end

end
