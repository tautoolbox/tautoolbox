classdef condition
%condition - Class to better express the conditions of integro-differential
%            equations solved by TauToolbox
%   c = tau.eig.condition() creates an object c (uninitialized).
%   c = tau.ode.condition(str) converts the string to a tau.ode.condition object
%
%input:
%   str = string representing the integro-differential condition
%
%output:
%   c = condition of the tau method (condition object).
%
%examples:
%   y = tau.ode.condition();
%   conditions = {y(1); y(1)'-1};
%   % equivalent to the set of conditions:
%   %  y(1) = 0;
%   %  y'(1) = 1;
%
%See also tau.operator, tau.equation, tau.problem and tau.polynomial.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    properties (SetAccess=private)
        condition_ode
        condition_eig
    end

    properties (Dependent)
        n               % number of components
    end

    methods (Access = public)
        function obj = condition(conds, nequations)
            if ~iscell(conds)
                error('tau.eig.condition: wrong first argument (should be a cell)')
            end
            if rem(length(conds), 2) == 0
                error('tau.eig.condition: wrong # of conditions')
            end
            obj.condition_ode = conds{1};
            obj.condition_eig = conds(2:end);
        end
    end

    methods
        function value = get.n(obj)
            value = length(obj.condition_eig)/2 + 1;
        end
    end

    methods (Access=public)
        function obj = subsref (obj, idxs)

            % This code is required to deal with the interplay of the different access types

            if strcmp(idxs(1).type, '{}')
                if idxs(1).subs{1} > obj.n
                    error('tau.eig.condition: this is a simple condition, there are no further conditions')
                end
                idx = idxs(1).subs{1};
                if idx == 1
                    obj = obj.condition_ode;
                else
                    obj = tau.ode.condition(obj.condition_eig{2*idx-2});
                end
                idxs = idxs(2:end);
            end

            if ~isempty(idxs)
                obj = builtin('subsref',obj,idxs);
            end
        end

        function obj = minus(obj, obj2)
            %+ - Plus operation for tau.eig.condition objects.
            %    plus(obj1, obj2) returns obj1 + obj2.
            %
            %input:
            %   obj1 = tau.eig.condition object
            %   obj2 = tau.eig.condition object
            %
            %output:
            %   obj = tau.eig.condition object

            if isa(obj, 'tau.eig.condition') && isa(obj2, 'tau.eig.condition')
                for k=1:min(obj.n, obj2.n)
                    if isempty(obj.conditions{k}) && ~isempty(obj2.conditions{k})
                        obj.conditions{k} = -obj2.conditions{k};
                    else
                        obj.conditions{k} = obj.conditions{k} - obj2.conditions{k};
                    end
                end

                if obj.n < obj2.n
                    for k=obj.n+1:obj2.n
                        if ~isempty(obj2.conditions{k})
                            obj.conditions{k, 1} = -obj2.conditions{k, 1};
                        end
                    end
                end
            elseif isa(obj, 'tau.eig.condition') && (isa(obj2, 'numeric') || isa(obj2, 'tau.ode.condition'))
                if isempty(obj.conditions{1})
                    obj.conditions{1} = -obj2;
                else
                    obj.conditions{1} = obj.conditions{1} - obj2;
                end
            elseif isa(obj2, 'tau.eig.condition') && isa(obj, 'numeric')
                if isempty(obj2.conditions{1})
                    obj2.conditions{1} = -obj;
                else
                    obj2.conditions{1} = obj2.conditions{1} - obj;
                end
                obj = obj2;
            else
                polynomial.utils.errorOp('-', class(obj), class(obj2));
            end
        end

        function obj = plus(obj, obj2)
            %+ - Plus operation for tau.eig.condition objects.
            %    plus(obj1, obj2) returns obj1 + obj2.
            %
            %input:
            %   obj1 = tau.eig.condition object
            %   obj2 = tau.eig.condition object
            %
            %output:
            %   obj = tau.eig.condition object

            if isa(obj, 'tau.eig.condition') && isa(obj2, 'tau.eig.condition')
                for k=1:min(obj.n, obj2.n)
                    if isempty(obj.conditions{k}) && ~isempty(obj2.conditions{k})
                        obj.conditions{k} = obj2.conditions{k};
                    else
                        obj.conditions{k} = obj.conditions{k} + obj2.conditions{k};
                    end
                end

                if obj.n < obj2.n
                    obj.conditions(obj.n+1:obj2.n, 1) = obj2.conditions(obj.n+1:obj2.n, 1);
                end
            elseif isa(obj, 'tau.eig.condition') && (isa(obj2, 'numeric') || isa(obj2, 'tau.ode.condition'))
                if isempty(obj.conditions{1})
                    obj.conditions{1} = obj2;
                else
                    obj.conditions{1} = obj.conditions{1} + obj2;
                end
            elseif isa(obj2, 'tau.eig.condition') && isa(obj, 'numeric')
                if isempty(obj2.conditions{1})
                    obj2.conditions{1} = obj;
                else
                    obj2.conditions{1} = obj2.conditions{1} + obj;
                end
                obj = obj2;
            else
                polynomial.utils.errorOp('-', class(obj), class(obj2));
            end
        end

        function obj = mtimes(obj, obj2)
            %+ - Multiplication operation for condition objects.
            %    plus(obj1, obj2) returns obj1 + obj2.
            %
            %input:
            %   obj1 = condition object or real
            %   obj2 = condition object or real
            %
            %output:
            %   obj = condition object

            if isa(obj, 'tau.eig.condition') && isa(obj2, 'tau.eig.condition')
                error('tau.eig.condition.mtimes: only linear conditions are accepted')
            elseif isa(obj, 'tau.eig.condition') && isa(obj2, 'numeric')
                for k=1:obj.n
                    if ~isempty(obj.conditions{k})
                        obj.conditions{k} = obj.conditions{k} .* obj2;
                    end
                end
            elseif isa(obj2, 'tau.eig.condition') && isa(obj, 'numeric')
                for k=1:obj.n
                    if ~isempty(obj2.conditions{k})
                        obj2.conditions{k} = obj2.conditions{k} .* obj;
                    end
                end
                obj = obj2;
            elseif isa(obj, 'tau.eig.condition') && isa(obj2, 'polynomial.Polynomial1')
                if ~strcmp(obj2.basis{1}.name, 'PowerX')
                    error('tau.eig.condition.mtimes: operations with bases other than the power basis are not yet supported')
                end
                for k=1:obj.n
                    if ~isempty(obj.conditions{k})
                        obj.conditions{k} = obj.conditions{k} .* obj2;
                    end
                end
            else
                polynomial.utils.errorOp('*', class(obj), class(obj2));
            end
        end

        function obj = mrdivide(obj, obj2)
            %/ - Division operation for condition objects by scalars.
            %    mrdivide(obj, obj2) returns obj / obj2.
            %
            %inputs:
            %   obj  = condition object
            %   obj2 = scalar
            %
            %output:
            %   obj  = obj  / obj2 (condition object)

            if isa(obj, 'tau.ode.condition') && isa(obj2, 'numeric')
                if ~isscalar(obj2)
                    polynomial.utils.errorOp('/', class(obj), polynomial.utils.numclass(obj2));
                end
                for k=1:obj.n
                    if ~isempty(obj.conditions{k})
                        obj.conditions{k} = obj.conditions{k} / obj2;
                    end
                end
            else
                polynomial.utils.errorOp('/', class(obj), class(obj2));
            end
        end

        function obj = uminus(obj)
            for k=1:obj.n
                if ~isempty(obj.conditions{k}) %#ok
                    obj.conditions{k} = -obj.conditions{k}; %#ok
                end
            end
        end

        function obj = uplus(obj)
        end

        function f = isGIVP(obj, domain)
            %isGIVP - is this a Generalized Initial Value Problem?
            %   f = isGIVP(condition, domain) verifies if all the points of the
            %       conditions/constraints are in the initial piece, so this is a
            %       Generalized Initial Value Problem (GIVP).
            %
            %   Some remarks:
            %       * if there is only one piece then this is always true;
            %       * an Initial Value Problem is always a GIVP, no mater how many pieces there are;
            %       * if there is only one piece then a Boundary Value Problem (BVP) is a
            %         GIVP because all the conditions/constraints are in the first piece.
            %
            %input:
            %   condition  = condition of the problem (tau.eig.condition object)
            %   domain     = domain (double vector).
            %
            %output:
            %   f = boolean variable: true for GIVP, false if not.

            f = obj.condition_ode.isGIVP(domain);
        end

        function disp(obj)
            disp(obj.condition_ode)
            disp(obj.condition_eig)
        end

        function value = point(obj)
            value = [];
            for k=1:obj.n
                if ~isempty(obj.conditions{k}) %#ok
                    value = [value; obj.conditions{k}.point]; %#ok
                end
            end
        end
    end
end
