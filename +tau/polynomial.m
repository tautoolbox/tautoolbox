function obj = polynomial(varargin)
%polynomial - returns an orthogonal polynomial
%   yn = tau.polynomial(options) creates an object yn of type polynomial.Polynomial1
%
%input:
%   options (optional) = options (tau.settings object)
%
%output:
%   yn = polynomial.Polynomial1 object
%
%examples:
%   yn = polynomial.Polynomial1();
%   plot(yn)
%
%See also polynomial.Polynomial1 and tau.settings.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    obj = polynomial.Polynomial1(varargin{:});
end
