classdef settings < polynomial.settings
%settings - Class to store the TauToolbox settings
%   options = tau.settings('param1', value1, 'param2', value2, ...)
%             creates an object with the TauToolbox settings.
%
%input parameters:
%   basis   = orthogonal polynomial basis
%            'ChebyshevT' (1st kind) (default), 'ChebyshevU' (2nd kind),
%            'ChebyshevV' (3rd kind), 'ChebyshevW' (4th kind),
%            'LegendreP', 'HermiteH', 'LaguerreL', 'GegenbauerC',
%            'BesselY' and 'PowerX'.
%
%   domain  = [a b] ([-1 1] default)
%   n       = degree of the polynomial approximation + 1 (default 32)
%
%output:
%   options = settings object (tau.settings object).
%
%examples:
%   options = tau.settings('basis', 'ChebyshevU', 'degree', 20);
%   % all the other options take the default values set for the session.
%
%See also tau.settings.session and tau.settings.default.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    properties
        % Problem options
        degree         % degree of the polynomial approximation to be used
        kind           % kind of problem to be solved
    end

    properties (Dependent)
        n              % 1+degree of the polynomial approximation to be used
    end

    methods
        function obj = settings(varargin)

            obj@polynomial.settings(varargin{:});

            % deal with the command version first
            % possible alternatives are: session; default and reset
            if nargin == 0 || strcmp(varargin{1}, 'session')
                obj = tau.settings(tau.settings.session__());
                return
            end

            % copy constructor
            if isa(varargin{1}, 'tau.settings')
                obj = varargin{1};
                return
            end
            
            if strcmp(varargin{1}, 'defaults')
                obj = tau.settings(tau.settings.defaults__());
                return
            end

            if strcmp(varargin{1}, 'reset')
                obj = tau.settings.session(tau.settings.defaults());
                return
            end

            % build object from a structure
            if isstruct(varargin{1})
                for fn = intersect(fieldnames(varargin{1}), properties('tau.settings'))'
                    obj.(fn{1}) = varargin{1}.(fn{1});
                end
                return
            end

            % build object from the arguments lists as parameters
            p = tau.settings.parser(tau.settings.session__());
            p.parse(varargin{:});

            for fn = fieldnames(p.Results)'
                obj.(fn{1}) = p.Results.(fn{1});
            end
        end

        function obj = set.degree(obj, value)
            if value ~= floor(value) || value < 0
                warning('tau.settings: degree must be positive and integer, setting default value')
                obj.degree = tau.settings.session__('degree');
            else
                obj.degree = value;
            end
        end

        function obj = set.n(obj, value)
            if value ~= floor(value) || value < 1
                warning('tau.settings: degree must be non-negative and integer, setting default value')
                obj.degree = tau.settings.session__('degree');
            else
                obj.degree = value - 1;
            end
        end

        function value = get.n(obj)
            value = obj.degree + 1;
        end

        function obj = set.kind(obj, value)
            if ~tau.problem.isKind(value)
                obj.kind = 'auto';
                warning('tau.settings: unknown problem kind, resetting to "auto"')
            else
                obj.kind = value;
            end
        end
    end

    methods
        function disp(obj)
            disp    ('        Tau settings options')
            disp    ('Orthogonal basis/polynomial')
            if iscell(obj.basis)
                basis__ = obj.basis{1};
            else
                basis__ = obj.basis;
            end
            fprintf ('           basis:  %s\n', basis__)
            fprintf ('          domain:  %s\n', mat2str(obj.domain))
            fprintf ('   quadPrecision:  %s\n', mat2str(obj.quadPrecision))
            fprintf ('      nequations:  %d\n', obj.nequations)
            fprintf ('          pieces:  %s\n', mat2str(obj.pieces))
            fprintf ('      firstpiece:  %s\n', mat2str(obj.firstpiece))
            fprintf ('     colorPieces:  %s\n', mat2str(obj.colorPieces))
            fprintf ('           steps:  %d\n', obj.steps)
            disp    ('')

            disp    ('Problem')
            fprintf ('          degree:  %d\n', obj.degree)
            fprintf ('            kind:  %s\n', obj.kind)
        end
    end

    methods (Static = true)
        function varargout = defaults()
        %tau.settings.defaults - Tau default settings.
        %   tau.settings.defaults() gets the  default settings used within Tau Toolbox.
        %
        %   -- options = tau.settings.defaults()
        %
        %output (optional):
        %   val = tau.settings object with the default values
            if nargout > 0
             varargout{1} = tau.settings('defaults');
             return
            end
            obj = tau.settings.defaults__();
            disp    ('tau.settings defaults')
            disp    ('=====================')
            disp    ('The format for each setting is:')
            disp    ('      optionName:  [default], type, allowed values')
            disp    ('')

            disp    ('Orthogonal basis/polynomial:')
            fprintf ('           basis:  ["%s"], switch:{one of orthogonal bases}\n', obj.basis)
            fprintf ('          domain:  [%s], interval\n', mat2str(obj.domain))
            fprintf ('   quadPrecision:  [%s]\n', mat2str(obj.quadPrecision))
            fprintf ('      nequations:  [%d]\n', obj.nequations)
            fprintf ('          pieces:  [%s]\n', mat2str(obj.pieces))
            fprintf ('      firstpiece:  [%s], boolean\n', mat2str(obj.firstpiece))
            fprintf ('     colorPieces:  [%s], boolean\n', mat2str(obj.colorPieces))
            fprintf ('           steps:  [%d], scalar, integer>0\n', obj.steps)

            disp    ('Problem:')
            fprintf ('          degree:  [%d], scalar, integer>0\n', obj.degree)
            fprintf ('            kind:  ["%s"], switch:{"auto", "eig", "ide", "ode"}\n', obj.kind)
            disp    ('')

            disp    ('Interpolation (only for session values):')
            fprintf ('    interpRelTol:  [%g], scalar, >0\n', obj.interpRelTol)
            fprintf ('    interpMaxDim:  [%d], scalar, integer>0\n', obj.interpMaxDim)
            disp    ('')
        end

        function varargout = session(varargin)
        %tau.settings.session - Tau session settings.
        %   tau.settings.session() gets the  default settings used within Tau Toolbox.
        %
        %   To read from the session settings
        %   -- options = tau.settings.session()
        %   -- value   = tau.settings.session('property')
        %
        %   To write in the session settings
        %   -- [opt] = tau.settings.session(options)
        %      options is a tau.settings object that is copied to the session settings
        %   -- [opt] = tau.settings.session('property_1', value_1, ...., 'property_n', value_n)
        %
            sessionSettings = polynomial.settings.session__();
            if nargin == 1
                if ~isa(varargin{1}, 'tau.settings')
                    varargout{1} = sessionSettings.(varargin{1});
                    return
                else
                    for fn = fieldnames(tau.settings.session__())'
                        tau.settings.session__(fn{1}, varargin{1}.(fn{1}));
                    end
                end
            elseif rem(nargin, 2) == 0
                for k = 1:2:nargin
                    sessionSettings.(varargin{k}) = varargin{k+1}; % validate value
                    tau.settings.session__(varargin{k}, sessionSettings.(varargin{k}));
                end
            else
                error('tau.settings.session: the number of arguments should either be 1 or even')
            end
            if nargout > 0
                varargout{1} = tau.settings('session');
            else
                disp    (tau.settings('session'))
                disp    ('')
                disp    ('Interpolation (only for session values):')
                fprintf ('    interpRelTol:  %g\n', sessionSettings.interpRelTol)
                fprintf ('    interpMaxDim:  %g\n', sessionSettings.interpMaxDim)
            end
        end

        function varargout = reset()
        %tau.settings.reset - reset Tau session settings to default values.
        %   tau.settings.reset() resets the session settings to the default values.
        %
        %   Similar to tau.settings('reset') but it

            tau.settings.session(tau.settings('defaults'));
            if nargout == 1
                varargout{1} = tau.settings();
            end
        end
    end

    methods (Static = true, Access = protected)
        function varargout = defaults__ (varargin)
        %tau.settings.defaults__ - Tau settings defaults.
        %   tau.settings.defaults__() gets the settings defaults used within Tau Toolbox.
        %
        %   -- options = tau.settings.defaults()
        %
        %output (optional):
        %   val = value of each property

            persistent defaultSettings;

            % defaultSettings is not defined so set all properties to the default values
            if isempty(defaultSettings)
                defaultSettings = defaults__@polynomial.settings(varargin{:});
                defaultSettings.degree = 31;
                defaultSettings.kind = 'auto';
            end

            varargout{1} = defaultSettings;
        end

        function varargout = session__(varargin)
            persistent sessionSettings;
            if isempty(sessionSettings)
                sessionSettings = tau.settings.defaults__();
            end

            if isempty(varargin)
                varargout{1} = sessionSettings;
                return
            elseif length(varargin) == 1
                varargout{1} = sessionSettings.(varargin{1});
                return
            elseif length(varargin) == 2
                sessionSettings.(varargin{1}) = varargin{2};
                varargout{1} = sessionSettings;
                return
            end
        end

        function p = parser(sessionSettings)
            p = inputParser ();
            p.KeepUnmatched = true;

            % problem
            p.addParameter('degree',         sessionSettings.degree)
            p.addParameter('kind',           sessionSettings.kind)
        end
    end
end
