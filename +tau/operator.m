classdef operator
%operator - Class for the Tau operator (linear combination).
%   obj = tau.operator(options) creates tau.operator object.
%
%input:
%   options (optional)   = options (tau.settings object)
%
%output:
%   obj = tau.operator object
%
%examples:
%   options = tau.settings('basis', 'ChebyshevU', 'degree', 20);
%   obj = tau.operator(options)
%
%See also tau.polynomial and tau.settings.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    properties  (SetAccess = private, GetAccess = public)
        basis         % Orthogonal polynomial basis.
        domain        % Domain [a b], interval of orthogonality of the basis.
        n             % Dimension of the truncated matrices poly has degree n-1.
        nequations    % number of equations
        quadPrecision % use, or not, quadruple precision
        activeVar     % list of active variable components
        opMat         % block matrix for operator
        odiff         % order of the derivative
        hdiff         % Pairs [diff order, power of M] for all the terms.
        hasIntegral   % Boolean that tells if the there are integral operators
    end

    properties (Access = private)
        N             % Matrix associated to the differentiation operator
    end

    % properties (Dependent)
    %     mat           % Matrix associated to the operator.
    % end

    methods (Access = public)
        function obj = operator(varargin)
            if nargin >= 2
                error('tau.operator: wrong number of arguments')
            elseif nargin == 1
                if ~isa(varargin{1}, 'tau.settings')
                    error('tau.operator: the optional must be of class tau.settings')
                end
                options = varargin{1};
            else
                options = tau.settings();
            end

            obj.basis = polynomial.basis(options);
            obj.nequations = options.nequations;
            obj.quadPrecision = options.quadPrecision;
            if obj.quadPrecision
                obj.domain = polynomial.utils.quadprecision(obj.domain);
            else
                obj.domain = options.domain;
            end
            obj.n = options.n;

            if obj.nequations == 1
                obj.activeVar = 1;
                if obj.quadPrecision
                    obj.opMat = {polynomial.utils.quadprecision(eye(obj.n))};
                else
                    obj.opMat = {eye(obj.n)};
                end
            else
                obj.activeVar = zeros(obj.nequations, 1);
                obj.opMat = cell(obj.nequations, 1);
            end

            obj.hdiff = cell(obj.nequations, 1);
            obj.odiff = cell(obj.nequations, 1);
            obj.hasIntegral = cell(obj.nequations, 1);
            for k = 1:obj.nequations
                obj.hdiff{k} = [0 0];
                obj.odiff{k} = 0;
                obj.hasIntegral{k} = false;
            end

            obj.N = obj.basis.matrixN(obj.n);
        end

        function obj = subsref (obj, idxs)
            idx = idxs(1);
            if strcmp(idx.type, '()') || strcmp(idx.type, '{}')
                if obj.nequations == 1
                    if idx.subs{1} == 1
                        return
                    end
                    error('tau.operator: wrong index %d (should be at most %d)', ...
                           idx.subs{1}, 1)
                end
                if any(obj.activeVar)
                    error('tau.operator: operation already defined');
                end
                obj.activeVar(idx.subs{1}) = 1;
                if obj.quadPrecision
                    obj.opMat{idx.subs{1}} = polynomial.utils.quadprecision(eye(obj.n));
                else
                    obj.opMat{idx.subs{1}} = eye(obj.n);
                end
            else % we are not interested in overrinding the . operator
                obj = builtin('subsref',obj,idxs);
            end
        end

        function m = mat(obj, ncols)
            if nargin == 1
                ncols = obj.n;
            end

            m = zeros(obj.n, obj.nequations*ncols);
            if obj.quadPrecision
                m = polynomial.utils.quadprecision(m);
            end
            for k = 1:obj.nequations
                if obj.activeVar(k)
                    m(:,1+(k-1)*ncols:k*ncols) = obj.opMat{k}(:,1:ncols);
                end
            end
        end

        function obj = ctranspose(obj)
            %' - Differentiation operation for tau.operator objects.
            %    ctranspose(obj) returns the derivative of the tau.operator expression.

            obj = diff(obj);
        end

        function obj = diff(obj, order)
            %diff - Differentiation of a tau.operator object for a given order.
            %       diff(obj, order) returns the tau.operator object representing obj'.
            %
            %input:
            %   obj (required)   = tau.operator object
            %   order (optional) = derivative order (integer) (default = 1)
            %
            %output:
            %   obj = tau.operator object
            %
            %See also fred, int and volt.

            if nargin == 1
                order = 1;
            end

            for k =1:obj.nequations
                if obj.activeVar(k) == 0
                    continue
                end
                obj.odiff{k} = obj.odiff{k} + order;
                for i=1:size(obj.hdiff{k}, 1)
                    obj.hdiff{k}(i,1) = obj.hdiff{k}(i,1) + order;
                end
                obj.opMat{k} = obj.N^order * obj.opMat{k};
            end
        end

        function obj = int(obj, order)
            %int - Integration for tau.operator object.
            %      int(obj) returns the tau.operator object representing int(obj)dx.
            %
            %input:
            %   obj = tau.operator object
            %
            %output:
            %   obj = tau.operator object
            %
            %See also diff, fred and volt.

            if nargin == 1
                order = 1;
            end

            matO = obj.basis.matrixO(obj.n);
            for k =1:obj.nequations
                if obj.activeVar(k) == 0
                    continue
                end
                obj.odiff{k} = obj.odiff{k} - order;
                for i=1:size(obj.hdiff{k}, 1)
                    obj.hdiff{k}(i,1) = obj.hdiff{k}(i,1) - order;
                end
                obj.opMat{k} = matO^order * obj.opMat{k};
                obj.hasIntegral{k} = true;
            end
        end

        function obj = fred(obj, varargin)
            %fred - Fredholm integral for tau.operator object.
            %       fred(obj, K) returns the tau.operator object representing the Fredholm
            %       integral term int_a^b K(x, t)obj(t)dt.
            %
            %input:
            %   obj (required) = tau.operator object
            %   K (optional) = two variable function K(x, t) (default is @(x, t) 1).
            %       If K is char, then the coefficients will be automatically found.
            %       If K is numeric column, then is related with coeff a of aP(x).
            %       If K is numeric row, then is related with coeff a of aP(t).
            %       If K is double matrix, then is related with coeff a of aP(x)P(t).
            %   domain (optional) = integration domain.
            %
            %output:
            %   obj = tau.operator object
            %
            %example:
            %   obj = tau.operator('ChebyshevU', [1 2], 5);
            %   fred(obj, @(x,t) cos(x+t))
            %
            %See also diff, int and volt.

            if nargin == 2
                K = varargin{1};
            elseif nargin == 1
                K=1; % equivalent to K = @(x,t) x.^0.*t.^0;
            end

            if isa(K, 'function_handle')
                K = obj.interporth2inc(K);
            end

            for k = 1:obj.nequations
                if obj.activeVar(k) == 0
                    continue
                end

                obj.odiff{k} = obj.odiff{k} - 1;
                for i=1:size(obj.hdiff{k}, 1)
                    obj.hdiff{k}(i,1) = obj.hdiff{k}(i,1) - 1;
                    obj.hdiff{k}(i,2) = obj.hdiff{k}(i,2) + size(K,1)-1;
                end

                Pa(1:obj.n) = obj.basis.polyvalb(obj.domain(1), obj.n);
                Pb(1:obj.n) = obj.basis.polyvalb(obj.domain(2), obj.n);

                % Compute the Fredholm term.
                if obj.quadPrecision
                        opmat = quadPrecision(zeros(obj.n));
                else
                        opmat = zeros(obj.n);
                end
                matO = obj.basis.matrixO(obj.n);
                for i = 1:size(K,2)
                    for j = 1:size(K,1)
                        coeff = zeros(1, j); coeff(j) = 1;
                        PjM = obj.basis.polyvalmM(coeff, obj.n);
                            opmat = opmat + K(j, i)*(tau.operator.epow(i, obj.n)*(Pb-Pa)*matO*PjM);
                    end
                end
                obj.opMat{k} = opmat * obj.opMat{k};
                obj.hasIntegral{k} = true;
            end
        end

        function obj = volt(obj, varargin)
            %volt - Volterra integral term for tau.operator object.
            %       volt(obj, K) returns the tau.operator object representing the Volterra
            %       integral term int_a^x K(x, t)obj(t)dt.
            %
            %input:
            %   obj (required) = tau.operator object
            %   K (optional) = two variable function K(x, t) (default is @(x, t) 1).
            %       If K is char, then the coefficients will be automatically found.
            %       If K is numeric column, then is related with coeff a of aP(x).
            %       If K is numeric row, then is related with coeff a of aP(t).
            %       If K is double matrix, then is related with coeff a of aP(x)P(t).
            %   domain (optional) = integration domain
            %
            %output:
            %   obj = tau.operator object
            %
            %example:
            %   obj = tau.operator('ChebyshevU', [1 2], 5);
            %   volt(obj, @(x,t) cos(x+t))
            %
            %See also diff, fred and int.

            if nargin == 2
                K = varargin{1};
            elseif nargin == 1
                K=1; % equivalent to K = @(x,t) x.^0.*t.^0;
            end

            if isa(K, 'function_handle')
                K = obj.interporth2inc(K);
            end

            for k = 1:obj.nequations
                if obj.activeVar(k) == 0
                    continue
                end

                obj.odiff{k} = obj.odiff{k} - 1;
                for i=1:size(obj.hdiff{k}, 1)
                    obj.hdiff{k}(i,1) = obj.hdiff{k}(i,1) - 1;
                    obj.hdiff{k}(i,2) = obj.hdiff{k}(i,2) + size(K,1)-1;
                end

                Pa(1:obj.n) = obj.basis.polyvalb(obj.domain(1), obj.n);

                matO = obj.basis.matrixO(obj.n);
                % Once the Volterra integral has limits [a x], we define the constants:
                firstrow = zeros(1, obj.n);
                for j = 1:obj.n-1
                    firstrow(j) = obj.basis.polyval([0; matO(2:j+1, j)], obj.domain(1));
                end
                matO(1, :) = firstrow(1:end);

                % Compute the Volterra term.
                opmat = zeros(obj.n);
                for i = 1:size(K,2)
                    coeff = zeros(1, i);
                    coeff(i) = 1;
                    PiM = obj.basis.polyvalmM(coeff, obj.n);
                    for j = 1:size(K,1)
                        coeff = zeros(1, j);
                        coeff(j) = 1;
                        PjM = obj.basis.polyvalmM(coeff, obj.n);
                            opmat = opmat + K(j, i)*((PiM - tau.operator.epow(i, obj.n)*Pa)*matO*PjM);
                    end
                end
                obj.opMat{k} = opmat * obj.opMat{k};
                obj.hasIntegral{k} = true;
            end
        end

        function obj = plus(obj, obj2)
            %+ - Sum operator for tau.operator objects.
            %    plus(obj, obj2) returns obj + obj2.

            if isa(obj, 'tau.operator') && isa(obj2, 'tau.operator')
                for k = 1:obj.nequations
                    if obj.activeVar(k) && obj2.activeVar(k)
                        obj.hdiff{k} = [obj.hdiff{k}; obj2.hdiff{k}];
                        obj.odiff{k} = max(obj.odiff{k}, obj2.odiff{k});
                        obj.opMat{k} = obj.opMat{k} + obj2.opMat{k};
                        obj.hasIntegral{k} = obj.hasIntegral{k} || obj2.hasIntegral{k};
                    elseif obj2.activeVar(k)
                        obj.activeVar(k) = 1;
                        obj.hdiff{k} = obj2.hdiff{k};
                        obj.odiff{k} = obj2.odiff{k};
                        obj.opMat{k} = obj2.opMat{k};
                        obj.hasIntegral{k} = obj2.hasIntegral{k};
                    end
                end
            else
                polynomial.utils.errorOp('+', class(obj), class(obj2));
            end
        end

        function obj = uplus(obj)
            %+ - Unary plus operator for tau.operator objects.
            %    uplus(obj) returns +obj.

            % no change required thus the body is empty
        end

        function obj = minus(obj, obj2)
            %minus - Subtraction operator for tau.operator objects.
            %        minus(obj1, obj2) returns obj1 - obj2.

            if isa(obj, 'tau.operator') && isa(obj2, 'tau.operator')
                for k = 1:obj.nequations
                    if obj.activeVar(k) && obj2.activeVar(k)
                        obj.hdiff{k} = [obj.hdiff{k}; obj2.hdiff{k}];
                        obj.odiff{k} = max(obj.odiff{k}, obj2.odiff{k});
                        obj.opMat{k} = obj.opMat{k} - obj2.opMat{k};
                        obj.hasIntegral{k} = obj.hasIntegral{k} || obj2.hasIntegral{k};
                    elseif obj2.activeVar(k)
                        obj.activeVar(k) = 1;
                        obj.hdiff{k} = obj2.hdiff{k};
                        obj.odiff{k} = obj2.odiff{k};
                        obj.opMat{k} = -obj2.opMat{k};
                        obj.hasIntegral{k} = obj2.hasIntegral{k};
                    end
                end
            else
                polynomial.utils.errorOp('-', class(obj), class(obj2));
            end
        end

        function obj = uminus(obj)
            %- - Unary minus operator for tau.operator objects.
            %    uminus(obj) returns -obj.

            for k = 1:obj.nequations
                if obj.activeVar(k)
                    obj.opMat{k} = -obj.opMat{k};
                end
            end
        end

        function obj = times(obj, obj2)
            %* - Product operator for tau.operator objects.
            %    times(obj, obj2) returns obj .* obj2 (always a tau.operator object).

            % If obj is a scalar and thus obj is tau.operator
            if isa(obj, 'numeric')
                if ~isscalar(obj)
                    polynomial.utils.errorOp('*', polynomial.utils.numclass(obj), class(obj2));
                end
                obj1 = obj;
                obj = obj2;
                for k = 1:obj.nequations
                    if obj.activeVar(k)
                        obj.opMat{k} = obj1 * obj2.opMat{k};
                    end
                end
            % If only obj is is tau.operator and obj2 is scalar
            elseif isa(obj2, 'numeric')
                if ~isscalar(obj2)
                    polynomial.utils.errorOp('*', class(obj), polynomial.utils.numclass(obj2));
                end
                obj.mat = obj2 * obj.mat;
            elseif isa(obj, 'tau.operator') && isa(obj2, 'tau.operator')
                error(['tau.operator: support for non linear terms is not yet automatic.' ...
                       'Please linearize the equation.'])
            elseif isa(obj2, 'polynomial.Polynomial1')
                for k = 1:obj.nequations
                    if obj.activeVar(k)
                        obj.hdiff{k}(end,2) = obj.hdiff{k}(end,2) + size(obj2.coeff, 1)-1;
                        obj.opMat{k} = obj.basis.polyvalmM(obj2.coeff, obj.n) * obj.opMat{k};
                    end
                end
            else
                polynomial.utils.errorOp('*', class(obj), class(obj2));
            end
        end

        function obj = mtimes(obj, obj2)
            obj = times(obj, obj2);
        end

        function obj = rdivide(obj, obj2)
            %* - Division operation for tau.operator objects by scalars.
            %    rdivide(obj1, obj2) returns obj1 / obj2.

            if isa(obj, 'tau.operator') && isa(obj2, 'numeric')
                if ~isscalar(obj2)
                    polynomial.utils.errorOp('/', class(obj), polynomial.utils.numclass(obj2));
                end
                for k = 1:obj.nequations
                    if obj.activeVar(k)
                        obj.opMat{k} = obj.opMat{k}/obj2;
                    end
                end
            else
                polynomial.utils.errorOp('/', class(obj), class(obj2));
            end
        end

        function obj = mrdivide(obj, obj2)
            obj = rdivide(obj, obj2);
        end

        function disp(obj)
            %disp - Display the contents of the tau.operator object.

            mat_size = [obj.n obj.n];

            str = sprintf('  %s object with properties:\n', class(obj));
            str = [str, sprintf('          basis: \t %s\n', obj.basis.name)];
            str = [str, sprintf('         domain: \t [%g, %g]\n', ...
                double(obj.domain(1)), double(obj.domain(2)))];
            str = [str, sprintf('         degree: \t %d\n', obj.n-1)];
            if obj.quadPrecision
                str = [str, sprintf('quad. precision: \t yes\n')];
            end
            str = [str, sprintf('            mat: \t [%dx%d %s]\n', ...
                mat_size(1), mat_size(2), class(obj.opMat))];
            fprintf(str);
        end

        function h = opHeight(obj)
            %opHeight - height of the differential operator.
            %   h = opHeight(obj) returns the heigth of the associated differential operator.
            %
            %input:
            %   obj = tau.operator object
            %
            %output:
            %   h = height of the differential operator (integer)

            % h = max(max(obj.hdiff(:,2) - obj.hdiff(:,1)), 0);
            h = cellfun(@(x) max(max(x(:,2)-x(:,1)),0), obj.hdiff);
        end

        function [f, esys, eeval, edom] = interporth2inc(obj, Fxt)
            %inteporth2inc - Incremental orthogonal polynomial interpolation for 2D.
            %   f = interporth2inc(x, Fxt) interpolates an expression by using the
            %   Chebyshev or Legendre points. In this version an incremental scheme is
            %   applied, and the interpolation degree is automatically found.
            %
            %inputs (required):
            %   obj    = tau.operator object.
            %   Fxt    = two variable function F(x, t) (char).
            %
            %output:
            %   f      = coefficients of interpolation (double matrix).
            %   esys   = error in the linear system (double vector).
            %   eeval  = (max.) error in the evaluation (double scalar).
            %   edom   = error in the full domain (double matrix).
            %
            %See also interporth2.

            % Use as default values
            inc = 3;
            tol = 1e-14;

            err = [1, Inf];
            k = 5;
            cont = 2;
            warn = 0;
            while err > tol
                [f, esys, eeval, edom] = obj.interporth2(Fxt, k);
                err(cont,:) = [k, eeval];
                if (err(cont,2) > err(cont-1,2)) && (abs(err(cont,2) - err(cont-1,2))<1e-10)
                    warn = warn + 1;
                end
                if warn == 5
                    [~, idxm] = min(err(:,2));
                    [f, esys, eeval, edom] = obj.interporth2(Fxt, err(idxm,1));
                    break
                end
                cont = cont +1;
                k = k + inc;
            end
        end

        function [f, esys, eeval, edom] = interporth2(obj, Fxt, n)
            %inteporth2 - Orthogonal polynomial interpolation for 2 dimension.
            %   f = interporth2(x, Fxt) interpolates an expression by using the
            %   Chebyshev or Legendre points; the approximation is
            %   P(x, t) = sum(i = 0:n-1)sum(j = 0:n-1)a_{i, j}Px_i(x)Pt_j(t), where
            %         A = [a_{0, 0} a_{0, 1} ... a{0, N}]
            %             [a_{1, 0} a_{1, 1} ... a{1, N}]
            %             [           ...            ]
            %             [a_{N, 0} a_{N, 1} ... a{N, N}], where N = n-1.
            %
            %inputs (required):
            %   obj       = tau.operator object.
            %   Fxt       = two variable function F(x, t) (char).
            %
            %inputs (optional):
            %   domain    = rectangular domain (double vector: [x1 x2 y1 y2]).
            %
            %output:
            %   f         = interpolation coefficients (double matrix).
            %   esys      = error in the linear system (double vector).
            %   eeval     = maximum error in the evaluation (double scalar).
            %   edom      = error in the full domain (double matrix).
            %
            %See also interporth2inc.

            % manage input arguments

            x = tau.polynomial(obj.basis);
            t = x;

            % Creating the points of choosen orthogonal polynomial basis.
            x_points = x.basis{1}.nodes(n);
            t_points = t.basis{1}.nodes(n);

            % Memory allocation for Px, Pt, X and T.
            Px = zeros(n);
            Pt = zeros(n);
            X  = zeros(n);
            T  = zeros(n);

            % Evaluate Px and Pt with polyval and allocate X and T.
            coeff = zeros(1, n);
            for j = 1: n
                coeff(j) = 1;
                % Px = [P0(x0)   ... Pn(x0) ;
                %       P0(x1)   ... Pn(x1) ;
                %                ...
                %       P0(xn-1) ... Pn(xn-1)]
                Px(:, j) = x.basis{1}.polyval(coeff, x_points);

                % Pt = [P0(t0)   ... Pn(t0) ;
                %       P0(t1)   ... Pn(t1) ;
                %                ...
                %       P0(tn-1) ... Pn(tn-1)]
                Pt(:, j) = t.basis{1}.polyval(coeff, t_points);

                % X = [x0 ... xn-1 ; x0 ... xn-1 ; ... ; x0 ... xn-1]
                X(:, j) = x_points(j);
                % T = [t0 ... t0 ; t1 ... t1 ; ... ; tn-1 ... tn-1]
                T(j,:) = t_points(j);
                coeff(j)=0;
            end

            % Allocate the elements of Ax = Px(X) and Ay = Pt(T).
            for i = 1: n
                for j = 1: n
                    Ax((i-1)*n+1:i*n, (j-1)*n+1:j*n) = Px(i, j);
                    At((i-1)*n+1:i*n, (j-1)*n+1:j*n) = Pt;
                end
            end

            % Obtaining the function Fxt and evaluating it at the b vector.
            % first try to see if it is vectorized (use a minimal sample)
            b_success = 0;
            try
                bb = Fxt(X([1,n]),T([1,n]));
                b_success = 1;
            catch

            end
            if b_success && all(size(bb) == [1,2])
                b = Fxt(reshape(X, n^2, 1), reshape(T, n^2, 1));
            else
                b = arrayfun(Fxt, reshape(X, n^2, 1), reshape(T, n^2, 1));
            end

            % Solving the system and changing the shape.
            solution = (Ax.*At)\b; f = reshape(solution, n, n);

            if nargout == 1
                return
            end
            % Calculating the error.
            esys = max(abs(Ax.*At*solution-b));
            [maxerr, X, T, F] = tau.operator.interporth2_error(x, t, solution, Fxt, n);
            ED(:, :, 1) = X; ED(:, :, 2) = T; ED(:, :, 3) = reshape(F, n, n);
            eeval = maxerr;
            edom = ED;
        end
    end

    methods (Static = true)
        function [f, X, T, F] = interporth2_error(x, t, solution, Fxt, n)
            %interporth2_error - Error in the orthogonal interpolation of F(x,t).
            %   [f, X, T, F] = interporth2_error(varargin) returns the error in the
            %   approximation of F(x, t).
            %
            %inputs:
            %   x         = orthogonal polynomial (polynomial.Polynomial1 object).
            %   t         = orthogonal polynomial (polynomial.Polynomial1 object).
            %   solution  = coefficients of approximate solution (double vector).
            %   Fxt       = function to compare (char).
            %   n         = number of linear points to interpolate (since this is 2D -> n^2)
            %
            %outputs:
            %   f = maximum error (double).
            %   X = grid on x (double matrix).
            %   T = grid on t (double matrix).
            %   F = pointwise error in the retangular domain (double matrix).

            % manage input arguments
            p = inputParser();
            p.addRequired('x', @(x) isa(x, 'polynomial.Polynomial1'))
            p.addRequired('t', @(x) isa(x, 'polynomial.Polynomial1'))
            p.addRequired('solution', @isnumeric)
            p.addRequired('Fxt', @(x) isa (x, 'function_handle'))
            p.addRequired('n', @isnumeric)

            p.parse(x, t, solution, Fxt, n)

            % Memory allocation for Px, Pt, X and T.
            Px = zeros(n); Pt = zeros(n);
            X = zeros(n); T = zeros(n);

            % Points to evaluate the error.
            x_points = linspace(x.domain(1), x.domain(2), n);
            t_points = linspace(t.domain(1), t.domain(2), n);

            % Evaluating Px and Pt with orthoval function and allocatig X and T.
            for j = 1: n
                coeff = zeros(1, j);
                coeff(j) = 1;
                % Px = [P0(x0) ... Pn(x0) ;
                %       P0(x1) ... Pn(x1) ; ... ;
                %       P0(xn-1) ... Pn(xn-1)]
                Px(:, j) = x.basis{1}.polyval(coeff, x_points);

                % Pt = [P0(t0) ... Pn(t0) ;
                %       P0(t1) ... Pn(t1) ; ... ;
                %       P0(tn-1) ... Pn(tn-1)]
                Pt(:, j) = t.basis{1}.polyval(coeff, t_points);

                % X = [x0 ... xn-1 ; x0 ... xn-1 ; ... ; x0 ... xn-1]
                X(1:n, j) = x_points(j);

                % T = [t0 ... t0 ; t1 ... t1 ; ... ; tn-1 ... tn-1]
                T(j, 1:n) = t_points(j);
            end

            % Allocating the elements of Ax = Px(X) and Ay = Pt(T).
            for i = 1: n
                for j = 1: n
                    Ax((i-1)*n+1:i*n, (j-1)*n+1:j*n) = Px(i, j);
                    At((i-1)*n+1:i*n, (j-1)*n+1:j*n) = Pt;
                end
            end
            % first try to see if it is vectorized (use a minimal sample)
            b_success = 0;
            try
                bb = Fxt(X([1,n]),T([1,n]));
                b_success = 1;
            catch

            end
            if b_success && all(size(bb) == [1,2])
                b = Fxt(reshape(X, n^2, 1), reshape(T, n^2, 1));
            else
                b = arrayfun(Fxt, reshape(X, n^2, 1), reshape(T, n^2, 1));
            end

            F = Ax.*At*solution - b;
            f = max(abs(F));
        end
    end

    methods (Static = true)
        function f = epow(i, n)
            %epow - ith vector from canonical basis.
            %   f = epow(i, n) returns the ith vector from the canonical basis
            %   truncated at dimension n.
            %
            %inputs:
            %   i   = vector position in the basis integer).
            %   n   = dimension of the truncated vector (integer).
            %
            %output:
            %   f   = ith vector from the canonical basis (sparse vector).

            f = zeros(n, 1);
            if i >= 1 && i <= n
                f(i) = 1;
                % f = sparse(i, 1, 1);
            end

        end
    end
end
