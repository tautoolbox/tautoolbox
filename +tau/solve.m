function [varargout] = solve(problem)
%tau.solve - Lanczos' Tau solver for integro-differential problems,
%   with initial/boundary/constraint conditions.
%   The problems can be integro-differential or eigenvalue related.
%   tau.solve calls the specific solver for the type of problem to be solved.
%
%   yn = tau.solve(problem)
%   [yn, info, residual, iterror, tauresidual] = tau.solve(problem)
%
%input (required):
%   problem      = problem (tau.problem object).
%
%output:
%   the output depends on the problem type.
%
%See also tau.eig.solve and tau.ode.solve.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

if ~isa(problem, 'tau.problem')
    error('tau.solve: the input argument must be a tau.problem')
end

switch problem.kind
    case {'ide', 'ode'}
        [varargout{1:nargout}] = tau.ode.solve(problem);
    case 'eig'
        [varargout{1:nargout}] = tau.eig.solve(problem);
end

end
