function out = matrix(op, problem, h)
%tau.matrix - get matrix for the operation op.
%   out = tau.matrix(op, problem) returns the matrix
%   representing the operation op given a differential problem.
%   out = tau.matrix(op, tau.settings()) returns the matrix
%   representing the operations op = 'M', 'N' or 'O'.
%   out = tau.matrix(op) also returns the matrix
%   representing the operations op = 'M', 'N' or 'O' using the
%   session tau.settings() specifications.
%
%input (required):
%   op         = operation ('mult' or 'M', 'diff' or 'N', 'int' or 'O',
%                'C' or 'cond', 'D' or 'operator', 'T' or 'taumatrix',
%                'R' or 'tauresidual', 'b' or 'taurhs',
%                'V' or 'orth2pow', 'W' or 'pow2orth').
%   problem    = ordinary differential problem (ode, domain, conditions, options).
%                For op = 'M', 'N' or 'O' it can be replaced by
%                tau.settings.
%
% input (optional):
%   h          = specify operator heigth (can set 0 if the degree is to be kept)
%
%output:
%   out        = returns the matrix representing the operation op,
%                using the polynomial given basis.
%
%examples:
%   out = tau.matrix('mult'); % get matrix M with the session tau.settings
%   out = tau.matrix('mult', tau.settings('degree',5)); % get matrix M with
%         the specifications in tau.settings()
%   out = tau.matrix('mult', problem); % get matrix M with the specifications
%         in tau.problem
%
%See also matrixM, matrixN, matrixO.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

op = tau.utils.chooseOperator(op);

if nargin == 1
    problem = tau.settings();
end

if nargin>=2 && ~isa(problem, 'tau.problem') && ~isa(problem, 'tau.settings')
    error('tau.matrix: the 2nd input argument must be a tau.problem or tau.settings object')
end

if nargin == 3 && ~isnumeric(h)
    error('tau.matrix: the 3rd input argument must be an integer (see help tau.matrix)')
end

if isa(problem, 'tau.problem')
    % evaluate operator heigth
    if nargin<3
        h = problem.height{1};
    end

    switch problem.kind
        case {'ide', 'ode'}
            out = tau.ode.matrix(op, problem, h);
        case 'eig'
            out = tau.eig.matrix(op, problem, h);
    end
else
    basis = polynomial.basis(problem);

    switch op
       case {'multiplication', 'm'}
            out = basis.matrixM(problem.n);
       case {'differentiation', 'n'}
           out = basis.matrixN(problem.n);
       case {'integration', 'o'}
           out = basis.matrixO(problem.n);
       case {'orth2pow', 'v'}
           out = basis.orth2powmatrix(problem.n);
       case {'pow2orth', 'w'}
           out = basis.pow2orthmatrix(problem.n);
       otherwise
           error('tau.matrix: the operator "%s" requires a tau.problem object as 2nd argument', op)
    end
end
end
