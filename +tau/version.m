function [varargout] = version(varargin)
%version - returns TauToolbox version
%
%output (optional):
%   vstring        = char array with the TauToolbox version.
%   info           = struct with the major, minor and patch levels of the TauToolbox version.
%
%See also tau.settings.session.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

persistent tau_version;
persistent ver;
if isempty(tau_version)
    ver.major = 0;
    ver.minor = 91;
    ver.patch = 0;
    tau_version = sprintf('%d.%d.%d', ver.major, ver.minor, ver.patch);
end

switch nargout
case 1
    varargout{1} = tau_version;
case 2
    varargout{1} = tau_version;
    varargout{2} = ver;
otherwise
    disp(['TauToolbox version: ', tau_version]);
end

end
