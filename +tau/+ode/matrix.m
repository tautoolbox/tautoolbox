function out = matrix(op, problem, h)
%tau.ode.matrix - get matrix for the operation op.
%   mat = tau.ode.matrix(op, problem) returns the matrix
%   representing the operation op given a differential problem.
%
%input (required):
%   op         = operation ('mult' or 'M', 'diff' or 'N', 'int' or 'O',
%                'C' or 'cond', 'D' or 'operator', 'T' or 'taumatirx',
%                'R' or 'tauresidual', 'b' or 'taurhs').
%   problem    = ordinary differential problem (tau object);
%                (ode, domain, conditions) or , (ode, domain, conditions, options).
%
% input (optional):
%   h          = specify operator heigth (can set 0 if the degree is to be kept)
%
%output:
%   out        = returns the matrix representing the operation op,
%                using the polynomial given basis.
%
%example:
%   out = tau.ode.matrix('T', problem);

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

op = tau.utils.chooseOperator(op);

if ~isa(problem, 'tau.problem')
    error('tau.ode.matrix: the 2nd input argument must be a tau.problem')
end

if nargin == 3 && ~isnumeric(h)
    error('tau.ode.matrix: the 3rd input argument must be an integer (see help tau.matrix')
end

if problem.npieces > 1
    error ('tau.ode.matrix: not yet implemented.')
end

% set definitions
equations = problem.equations;
conditions = problem.conditions;
options = problem.options;

% define for conveniency (shorter to type and read)
nequations = problem.nequations;
n = problem.n;
nu = problem.nconditions;

h = max(cell2mat(problem.height));  % evaluate operator heigth
he = zeros(problem.nequations, 1); % segregate h per equation
for i = 1:problem.nequations
    he(i) = max(problem.height{i});
end

% prepare blocks, we just care about the polynomials until degree n-1
if is_octave() || ~verLessThan('matlab', '9.1')
    cols = (n+h)*(0:nequations-1)+(1:n)';
else
    cols = repmat((n+h)*(0:nequations-1), n, 1) + repmat((1:n)',1,length(0:nequations-1));
end

options.n = n+h;
basis = polynomial.basis(problem.options);

% get the required matrix
switch op
    case {'condition', 'c'}
        out = basis.systemBlock(conditions, problem.n, nequations);
    case {'operator', 'd'}
        idx = 0;
        out = zeros(nequations*n-nu, n);
        for neq = 1:nequations
            D = problem.equations{neq}.lhs(tau.polynomial(options),tau.operator(options));
            D = D.mat;
            rows = 1:(n-problem.condpart(neq));
            out(idx + rows,:) = D(rows, cols(:));
            idx = idx + n - problem.condpart(neq);
        end
    case {'taumatrix', 't'}
        out = basis.systemBlock(conditions, problem.n, nequations);
        idx = nu;
        for neq = 1:nequations
            D = problem.equations{neq}.lhs(tau.polynomial(options),tau.operator(options));
            D = D.mat;
            rows = 1:(n-problem.condpart(neq));
            out(idx + rows,:) = D(rows, cols(:));
            idx = idx + n - problem.condpart(neq);
        end
    case {'taurhs', 'b'}
        out = zeros(problem.n, 1);
        [~, out(1:nu, 1)] = basis.systemBlock(conditions, problem.n, nequations);
        idx = nu;
        for neq = 1:nequations
            basis = polynomial.basis(options);
            rows = 1:(n-problem.condpart(neq));
            out(idx + rows) = basis.interp1(equations{neq}.rhs, rows(end));
            idx = idx + n - problem.condpart(neq);
        end
    case {'tauresidual', 'r'}
        idx = 0;
        out = zeros(nu+h, n*nequations);
        y = tau.operator(options);
        % options.nequations = 1; % we use this because at each line there only one equation
        x = tau.polynomial(options);
        for neq = 1:nequations
            D = problem.equations{neq}.lhs(x, y);
            D = D.mat;
            rows = n-nu+1:n+he(neq);
            out(idx + rows - n + nu,:) = D(rows, cols(:));
            idx = idx + length(rows);
        end
    case {'multiplication', 'm'}
        out = basis.matrixM(n+h);
    case {'differentiation', 'n'}
        out = basis.matrixN(n+h);
    case {'integration', 'o'}
        out = basis.matrixO(n+h);
end
end
