function [yo, varargout] = equations(problem, varargin)
%tau.ode.nonlinearSystem - Lanczos' Tau solver for linearized integro-differential problems,
%   with initial or boundary conditions.
%   tau.odee.nonlinearSystem returns the nth degree polynomial approximation yn=Pn*an,
%   on the orthogonal polynomial basis Pn, of the linearized integro-differential
%   problem dy/dx=F(x,y), via the solution of the Tau system Tn*an=bn.
%
%   yn = tau.ode.nonlinearSystem(problem)
%   [yn, info, residual, tauresidual] = tau.ode.nonlinearSystem(problem)
%
%input (required):
%   problem     = tau.problem object with the description of the problem
%
%output:
%   yn          = approximate solution (tau polynomial)
%   info        = information structure
%   residual    = residual Tn*an-bn (tau polynomial)
%   tauresidual = tau residual Rn*an (tau polynomial)
%
%See also tau.ode.nonlinear.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

name = 'tau.ode.nonlinear.equations';
tau.utils.validate_solver_arguments(problem, name, ...
            true,  ...                 % supports multiple equations
            true,  ...                 % supports nonlinear problems
            false, ...                 % supports multiple pieces solution
            false)                     % supports conditions only in the first piece

equations = problem.equations;
conditions = problem.conditions;

% define for conveniency (shorter to type and read)
nequations = problem.nequations;
n = problem.n;
nu = problem.nconditions;              % problem's number of conditions

params = tau.utils.parse_nonlinear_params(name, varargin{:});

nux = max(problem.condpart);           % order of the largest derivative for variable k
h = max(cell2mat(problem.height));     % evaluate operator heigth

T = zeros(nequations*n);               % T matrix by var*eq blocks
b = zeros(nequations*n, 1);            % b vector by blocks
R = zeros((nux+h)*nequations, n*nequations);

% incorporate the conditions in the system matrix and independent vector
basis = polynomial.basis(problem.options);
[C, bs] = basis.systemBlock(conditions, n, nequations);
T(1:nu, :) = C;
b(1:nu)    = bs;

options = problem.options;
options.n = n+h;
x = tau.polynomial(options);
solver = tau.utils.linearSystemSolver();

% Points where to evaluate the iterative solution
points = linspace(x.domain(1), x.domain(2), params.steps);

% create initial solution that satisfies the problem's conditions
cols = zeros(1, nu);
idx = 1;
for k=1:nequations
    cols(idx:idx + problem.condpart(k)-1)= (1:problem.condpart(k)) + (k-1)*n;
    idx = idx + problem.condpart(k);
end

yo = tau.polynomial((C(1:nu,cols)\b(1:nu))', options);

yo_values = yo(points);

% Applying the Newton method.
k = 0;
error_ = 1;
while error_ > params.tol && k < params.maxIter
    idx = nu;
    k = k+1;
    for neq = 1:nequations
        if nargin(problem.equations{neq}.lhs) == 3
            D = problem.equations{neq}.lhs(x{1},tau.operator(options),yo);
        else
            D = problem.equations{neq}.lhs(x{1},tau.operator(options));
        end
        D = D.mat(n);

        rows = 1:(n-problem.condpart(neq));
        T(idx + rows,:) = D(rows,:);
        if nargin(equations{neq}.rhs) == 2
            b(idx + rows) = basis.interp1(equations{neq}.rhs(x,yo), rows(end));
        else
            b(idx + rows) = basis.interp1(equations{neq}.rhs, rows(end));
        end
        idx = idx + n - problem.condpart(neq);
        if nargout > 1
            ns = (nux+h)*(neq-1);
            nr = n+h-rows(end);
            R(ns+1:ns+nr,:) = D(rows(end)+1:n+h,:);
        end
    end

    % Solving the associated linear system
    [yo, ~] = solver.solve(T, b, problem);

    % Evaluating the approximate solution.
    previous_values = yo_values;
    yo_values = yo(points);
    error_ = max(max(abs(yo_values-previous_values)));
end

% info structure
if nargout > 1
    info.tau_solver = name;
    info.linear_system_solver = solver.algorithm;

    varargout{1} = info;
end

% residual vector (tau polynomial object)
if nargout > 2
    varargout{2} = [];
end

% tau residual vector (tau polynomial object)
if nargout > 3
    varargout{3} = [];
end

end
