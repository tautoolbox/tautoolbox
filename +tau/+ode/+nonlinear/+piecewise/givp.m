function [yn, varargout] = givp(problem, varargin)
%tau.ode.piecewise.nonlinearGIVP - Lanczos' Tau solver for linear integro-differential problems,
%   for Generalized Initial Value Problems (GIVP).
%   A GIVP has all conditions/constraints (initial, boundary or in the middle)
%   in the first piece. Note that if the problem only has a single piece then
%   even a Boundary Value Problem (BVP) is a GIVP because all the conditions
%   are in the first piece.
%   nonlinearGIVP returns the (n-1)th degree polynomial approximation yn=Pn*an,
%   on the orthogonal polynomial basis Pn, of the linearized integro-differential
%   problem dy/dx=f(x,y), via the solution of the Tau system Tn*an=bn.
%   The domain is divided in pieces and the solver acts on each subinterval
%   appropriately.
%
%   yn = tau.ode.piecewise.nonlinearGIVP(problem)
%   [yn, info, residual, tauresidual] = tau.ode.piecewise.nonlinearGIVP(problem)
%
%input (required):
%   problem     = tau.problem object with the description of the problem
%
%output:
%   yn          = approximate solution (tau polynomial object)
%   info        = information structure
%   residual    = residual Tn*an-bn (tau polynomial object)
%   tauresidual = tau residual Rn*an (tau polynomial object)
%
%See also tau.ode.linear, tau.ode.piecewise.linear,
% tau.ode.linearSystem, tau.ode.piecewise.linearSystem.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

name = 'tau.ode.nonlinear.piecewise.givp';
tau.utils.validate_solver_arguments(problem, name, ...
            false, ...                 % supports multiple equations
            true,  ...                 % supports nonlinear problems
            true,  ...                 % supports multiple pieces solution
            true)                      % supports conditions only in the first piece

equation = problem.equations{1};

coeff = zeros(problem.n, problem.nequations, problem.npieces);
yn = tau.polynomial(problem.options);
iterations = zeros(problem.npieces,1);
newton_iterations = zeros(problem.npieces,1);

nu = problem.nconditions;
conditions = problem.conditions; % initial conditions are the problem conditions

params = tau.utils.parse_nonlinear_params(name, varargin{:});
solver = tau.utils.linearSystemSolver();

% prepare the conditions at the continuity nodes
cc = tau.ode.condition(1);
ccond = cell(nu, 1);
for j=1:nu
    ccond {j} = cc;
    cc = diff(cc);
end

options = problem.options;
n = options.n;
h = max(cell2mat(problem.height));  % evaluate operator heigth
options.n = n+h;
options.pieces = 1;

for s = 1:problem.npieces
    % Create new tau objects
    options.domain = yn.pieces(s:s+1);
    x = tau.polynomial(options);
    y = tau.operator(options);
    basis = polynomial.basis(options);

    % Points where to evaluate the iterative solution
    points = linspace(x.domain(1), x.domain(2), params.steps);

    % incorporate the conditions in the system matrix and independent vector
    b = zeros(n,1);
    [C, b(1:nu)] = basis.systemBlock(conditions, n, 1);

    % create initial solution that satisfies the problem's conditions
    yo = tau.polynomial(C(1:nu,1:nu)\b(1:nu), options);
    yo_values = yo(points);

    % Applying the Newton method.
    k = 0;
    error_ = 1;
    while error_ > params.tol && k < params.maxIter
        k = k+1;

        % Evaluating the operator and the rigth hand side.
        D = equation.lhs(x,y,yo);
        D = D.mat;
        b(1+nu:n) = basis.interp1(equation.rhs(x,yo), n-nu);

        % Truncating the system.
        T = [C; D(1:n-nu, 1:n)];

        % Calculating the approximate solution.
        [yo, iter] = solver.solve(T, b, problem, options);

        % Evaluating the approximate solution.
        previous_values = yo_values;
        yo_values = yo(points);
        error_ = max(abs(yo_values-previous_values));
    end

    % Store output of the last iteration.
    coeff(:,:, s) = yo.coeff_n(n);
    iterations(s) = iter;
    newton_iterations(s) = k;

    if s < problem.npieces % Update conditions for the next piece
        sol = yo;
        conditions = cell(nu, 1);
        for k=1:nu
            conditions{k} = ccond{k}(options.domain(2))-sol(options.domain(2));
            sol = diff(sol); % repeat the procedure for the next derivative
        end
    end
end

yn = tau.polynomial(coeff, problem.options);

% info structure
if nargout > 1
    info.tau_solver = name;
    info.linear_system_solver = solver.algorithm;
    if any(iterations)
        info.iterations = iterations;
    end
    info.newton_iterations = newton_iterations;

    varargout{1} = info;
end

% residual vector (tau polynomial object)
if nargout > 2
    varargout{2} = [];
end

% tau residual vector (tau polynomial object)
if nargout > 3
    varargout{3} = [];
end
end
