function [f, varargout] = equations(problem)
%tau.ode.piecewise.nonlinearSystem - Lanczos' Tau solver for linearized integro-differential problems,
%   with initial or boundary conditions.
%   tau.ode.piecewise.nonlinearSystem returns the nth degree polynomial approximation yn=Pn*an,
%   on the orthogonal polynomial basis Pn, of the linearized integro-differential
%   problem dy/dx=F(x,y), via the solution of the Tau system Tn*an=bn.
%
%   yn = tau.ode.piecewise.nonlinearSystem(problem)
%   [yn, info, residual, tauresidual] = tau.ode.piecewise.nonlinearSystem(problem)
%
%input (required):
%   problem     = object of type tau.problem
%
%output:
%   yn          = approximate solution (tau.polynomial object).
%   info        = information structure.
%   residual    = residual Tn*an-bn (tau object)
%   tauresidual = tau residual Rn*an (tau polynomial object)
%
%See also tau.ode.piecewise.nonlinear.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

name = 'tau.ode.nonlinear.piecewise.equations';
tau.utils.validate_solver_arguments(problem, name, ...
            true,  ...                  % supports multiple equations
            true,  ...                  % supports nonlinear problems
            true,  ...                  % supports multiple pieces solution
            false)                      % supports conditions only in the first piece

error([name, ': unimplemented'])
end
