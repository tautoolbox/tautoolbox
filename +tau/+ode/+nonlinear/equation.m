function [yo, varargout] = equation(problem, varargin)
%tau.ode.nonlinear - Lanczos' Tau solver for linearized integro-differential problems,
%   with initial or boundary conditions.
%   tau.ode.nonlinear returns the nth degree polynomial approximation yn=Pn*an,
%   on the orthogonal polynomial basis Pn, of the linearized integro-differential
%   problem dy/dx=F(x,y), via the solution of the Tau system Tn*an=bn.
%
%   yn = tau.ode.nonlinear(problem)
%   [yn, info, residual, tauresidual] = tau.ode.nonlinear(problem)
%
%input (required):
%   problem     = tau.problem object with the description of the problem
%
%output:
%   yn          = approximate solution (tau polynomial)
%   info        = information structure
%   residual    = residual Tn*an-bn (tau polynomial)
%   tauresidual = tau residual Rn*an (tau polynomial)
%
%See also tau.ode.nonlinearSystem.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

name = 'tau.ode.nonlinear.equation';
tau.utils.validate_solver_arguments(problem, name, ...
            false, ...                 % supports multiple equations
            true,  ...                 % supports nonlinear problems
            false, ...                 % supports multiple pieces solution
            false)                     % supports conditions only in the first piece

equation = problem.equations{1};
options = problem.options;

nu = problem.nconditions;
conditions = problem.conditions; % initial conditions are the problem conditions
params = tau.utils.parse_nonlinear_params(name, varargin{:});

h = max(cell2mat(problem.height));  % evaluate operator heigth
n = options.n;
options.n = n+h;

x = tau.polynomial(options);
y = tau.operator(options);
solver = tau.utils.linearSystemSolver();

% Points where to evaluate the iterative solution
points = linspace(x.domain(1), x.domain(2), params.steps);

% prepare the matrices that will hold the linear system to be solved
T = zeros(n, n);
R = zeros(nu+h, n);  % build with extended size to accomodate height
b = zeros(n,1);

% incorporate the conditions in the system matrix and independent vector
basis = polynomial.basis(options);
[C, b(1:nu)] = basis.systemBlock(conditions, n, 1);

% create initial solution that satisfies the problem's conditions
yo = tau.polynomial(C(1:nu,1:nu)\b(1:nu), options);
yo_values = yo(points);

T(1:nu,:) = C;

% Applying the Newton method.
k = 0;        % iteration number
error_ = 1;   % maximum absolute error between consecutive approaches
while error_ > params.tol && k < params.maxIter
    k = k+1;
    % Evaluating the operator and the rigth hand side.
    D = equation.lhs(x,y,yo);
    D = D.mat(n);
    b(1+nu:n) = basis.interp1(equation.rhs(x,yo), n-nu);

    % Truncating the system.
    T(nu+1:end,:) = D(1:n-nu,:);

    % Calculating the approximate solution.
    [yo, iter] = solver.solve(T, b, problem);

    % Evaluating the approximate solution.
    previous_values = yo_values;
    yo_values = yo(points);
    error_ = max(abs(yo_values-previous_values));
end

R = D(n-nu+1:end,:);

% info structure
if nargout > 1
    info.tau_solver = name;
    info.linear_system_solver = solver.algorithm;
    if iter ~= 0
        info.iterations = iter;
    end
    info.newton_iterations = k;
    info.cond = cond(T);
    yc = yn.coeff_n(n);
    residual_v = T*yc(:) - b;
    info.residual = norm(residual_v);
    tauresidual_v = R*yc;
    info.tauresidual = norm(tauresidual_v);
    varargout{1} = info;
end

% residual vector (tau polynomial object)
if nargout > 2
    varargout{2} = tau.polynomial(residual_v, problem.options);
end

% tau residual vector (tau polynomial object)
if nargout > 3
    coeffs = zeros(n+h,1);
    coeffs(n-nu+1:end) = tauresidual_v;
    varargout{3} = tau.polynomial(coeffs, problem.options);
end

end
