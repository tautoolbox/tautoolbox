classdef condition
%condition - Class to better express the conditions of integro-differential
%            equations solved by TauToolbox
%   c = tau.ode.condition() creates an object c (uninitialized).
%   c = tau.ode.condition(str) converts the string to a tau.ode.condition object
%
%input:
%   str = string representing the integro-differential condition
%
%output:
%   c = condition of the tau method (condition object).
%
%examples:
%   y = tau.ode.condition();
%   conditions = {y(1); y(1)'-1};
%   % equivalent to the set of conditions:
%   %  y(1) = 0;
%   %  y'(1) = 1;
%
%See also tau.operator, tau.equation, tau.problem and tau.polynomial.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    properties (SetAccess=private)
        var     % number of the variable
        coeff   % coefficient that multiplies the function
        order   % order of the derivative (0 means the function)
        point   % point where the function (or derivative) is evaluated
        value   % real value that it takes
        nvars   % number of variable components
        varname % name of the variable, it is 'y' by default
    end

    properties (Dependent)
        n               % number of components
        xmin            % minimum value
        xmax            % maximum value
    end

    methods (Access = public)
        function obj = condition(nequations)

            if isa(nequations, 'tau.ode.condition')
                obj = nequations;
                return
            end

            obj.coeff = 1;
            obj.order = 0;
            obj.point = [];
            obj.value = 0;
            obj.nvars = nequations;
            if obj.nvars == 1
                obj.var = 1;
            else
                obj.var = [];
            end
            obj.varname = 'y';
        end
    end

    methods
        function value = get.n(~)
            value = 1;
        end

        function value = get.xmin(obj)
            value = min(obj.point);
        end

        function value = get.xmax(obj)
            value = max(obj.point);
        end
    end

    methods (Access=public)

        function obj = subsref (obj, idxs)
            while ~isempty(idxs)
                idx = idxs(1);
                switch idx.type
                    case '()'
                        if  ~isempty(obj.point)
                            error('tau.ode.condition: forbidden operation, point already set');
                        end
                        obj.point = idx.subs{1};
                    case '{}'
                        if ~isempty(obj.var)
                            if idx.subs{1} ~= 1
                                error('tau.ode.condition: forbidden operation, variable already set');
                            else
                                % part of the interface due to tau.eig.conditions
                                % we simulate this as a 1*1 cell of tau.ode.condition
                                idxs = idxs(2:end);
                                continue
                            end
                        end
                        obj.var = idx.subs{1};
                        if obj.var < 1
                            error('tau.ode.condition: the vector component should be a positive integer')
                        end
                        if obj.var ~= floor(obj.var)
                            warning('tau.ode.condition: the vector component should be integer, proceeding taking out the decimal part')
                            obj.var = floor(obj.var);
                        end
                        if obj.var > obj.nvars
                            error('tau.ode.condition: component %d larger than the initial setup %d', obj.var, obj.nvars)
                        end
                    case '.'
                        % we are not interested in overriding the . operator
                        obj = builtin('subsref',obj,idxs);
                        return
                end
                idxs = idxs(2:end);
            end
      end

        function obj = ctranspose(obj)
            %' - Differentiation operation for condition objects.
            %    obj = ctranspose(obj) returns the derivative of the condition tau variable.
            %
            %input:
            %   obj = condition object
            %
            %output:
            %   obj = derivative of obj (condition object).

            obj = diff(obj);
        end

        function obj = diff(obj, varargin)
            %diff - Differentiation of a condition object for a given order.
            %       diff(obj, order) returns the condition object representing obj'.
            %
            %input:
            %   obj (required)    = condition object
            %   dorder (optional) = derivative order (integer) (default = 1)
            %
            %output:
            %   obj = condition object

            if nargin == 1
                dorder = 1;
            elseif nargin == 2
                dorder = 1;
                obj.point = varargin{1};
            elseif nargin ==3
                dorder = varargin{1};
                obj.point = varargin{2};
            else
                error('tau.ode.condition.diff: wrong number of arguments')
            end
            obj.order = obj.order + dorder;
        end

        function obj = minus(obj, obj2)
            %+ - Plus operation for tau.ode.condition objects.
            %    plus(obj1, obj2) returns obj1 + obj2.
            %
            %input:
            %   obj1 = tau.ode.condition object
            %   obj2 = tau.ode.condition object
            %
            %output:
            %   obj = tau.ode.condition object

            if isa(obj, 'tau.ode.condition') && isa(obj2, 'tau.ode.condition')
                obj.var   = [obj.var;   obj2.var];
                obj.coeff = [obj.coeff; -obj2.coeff];
                obj.order = [obj.order; obj2.order];
                obj.point = [obj.point; obj2.point];
                obj.value = obj.value + -obj2.value;
            elseif isa(obj, 'tau.ode.condition') && isa(obj2, 'numeric')
                % the plus is due to the fact that value goes to rhs
                obj.value = obj.value + obj2;
            elseif isa(obj2, 'tau.ode.condition') && isa(obj, 'numeric')
                % the plus is due to the fact that value goes to rhs
                obj2.value = obj2.value + obj;
                obj = obj2;
            else
                polynomial.utils.errorOp('-', class(obj), class(obj2));
            end
        end

        function obj = plus(obj, obj2)
            %+ - Plus operation for tau.ode.condition objects.
            %    plus(obj1, obj2) returns obj1 + obj2.
            %
            %input:
            %   obj1 = tau.ode.condition object
            %   obj2 = tau.ode.condition object
            %
            %output:
            %   obj = tau.ode.condition object

            if isa(obj, 'tau.ode.condition') && isa(obj2, 'tau.ode.condition')
                obj.var   = [obj.var;   obj2.var];
                obj.coeff = [obj.coeff; obj2.coeff];
                obj.order = [obj.order; obj2.order];
                obj.point = [obj.point; obj2.point];
                obj.value = obj.value + obj2.value;
            elseif isa(obj, 'tau.ode.condition') && isa(obj2, 'numeric')
                % the minus is due to the fact that value goes to rhs
                obj.value = obj.value - obj2;
            elseif isa(obj2, 'tau.ode.condition') && isa(obj, 'numeric')
                % the minus is due to the fact that value goes to rhs
                obj2.value = obj2.value - obj;
                obj = obj2;
            else
                polynomial.utils.errorOp('+', class(obj), class(obj2));
            end
        end

        function obj = mtimes(obj, obj2)
            %+ - Multiplication operation for condition objects.
            %    plus(obj1, obj2) returns obj1 + obj2.
            %
            %input:
            %   obj1 = condition object or real
            %   obj2 = condition object or real
            %
            %output:
            %   obj = condition object

            if isa(obj, 'tau.ode.condition') && isa(obj2, 'tau.ode.condition')
                error('tau.ode.condition.mtimes: only linear conditions are accepted')
            elseif isa(obj, 'tau.ode.condition') && isa(obj2, 'numeric')
                obj.value = obj.value * obj2;
                obj.coeff = obj.coeff * obj2;
            elseif isa(obj2, 'tau.ode.condition') && isa(obj, 'numeric')
                obj2.value = obj2.value * obj;
                obj2.coeff = obj2.coeff * obj;
                obj = obj2;
            elseif isa(obj2, 'polynomial.Polynomial1')
                obj = tau.eig.condition(obj2, obj);
            else
                polynomial.utils.errorOp('*', class(obj), class(obj2));
            end
        end

        function obj = mrdivide(obj, obj2)
            %/ - Division operation for condition objects by scalars.
            %    mrdivide(obj, obj2) returns obj / obj2.
            %
            %inputs:
            %   obj  = condition object
            %   obj2 = scalar
            %
            %output:
            %   obj  = obj  / obj2 (condition object)

            if isa(obj, 'tau.ode.condition') && isa(obj2, 'numeric')
                if ~isscalar(obj2)
                    polynomial.utils.errorOp('/', class(obj), polynomial.utils.numclass(obj2));
                end
                obj.coeff = obj.coeff/obj2;
                obj.value = obj.value/obj2;
            else
                polynomial.utils.errorOp('/', class(obj), class(obj2));
            end
        end

        function obj = uminus(obj)
            obj.coeff = -obj.coeff;
        end

        function obj = uplus(obj)
        end

        function disp(obj)
            if isempty(obj.point)
                return
            end
            if obj.nvars == 1
                vname = obj.varname;
            end
            for k = 1:size(obj.coeff, 1)
                if k ~= 1 && obj.coeff(k) > 0
                    fprintf(' +');
                end
                if obj.coeff(k) == -1
                    fprintf('-');
                elseif obj.coeff(k) ~= 1
                    fprintf('%g ', obj.coeff(k));
                end
                if obj.nvars > 1
                    vname = sprintf('%s{%d}', obj.varname, obj.var(k));
                end
                if obj.order(k) ~= 0
                    if obj.order(k) > 3
                        fprintf('diff(%s, %d)', vname, obj.order(k));
                    else
                        fprintf('%s%s', vname, repmat('''', [1 obj.order(k)]));
                    end
                else
                    fprintf('%s', vname);
                end
                fprintf('(%g)', obj.point(k));
            end
            fprintf(' = %g\n', obj.value);
        end

        function f = isGIVP(obj, domain)
            %isGIVP - is this a Generalized Initial Value Problem?
            %   f = isGIVP(condition, domain) verifies if all the points of the
            %       conditions/constraints are in the initial piece, so this is a
            %       Generalized Initial Value Problem (GIVP).
            %
            %   Some remarks:
            %       * if there is only one piece then this is always true;
            %       * an Initial Value Problem is always a GIVP, no mater how many pieces there are;
            %       * if there is only one piece then a Boundary Value Problem (BVP) is a
            %         GIVP because all the conditions/constraints are in the first piece.
            %
            %input:
            %   condition  = condition of the problem (tau.ode.condition object)
            %   domain     = domain (double vector).
            %
            %output:
            %   f = boolean variable: true for GIVP, false if not.

            f = true;
            if obj.xmax > domain(2)
                f = false;
            end
        end
    end

    methods ( Access = public, Static = true)
        function obj = fromstr(conditions, nequations)
            %fromstr - Read problem (boundary) conditions from string.
            %   obj = fromstr(conditions)
            %   This is a generalized form for conditions as sum(i = 0:n)biy_i^(i)(xi) = y0.
            %
            %input:
            %   conditions = string or cell of (boundary) conditions.
            %
            %output:
            %   obj = object of type condition

            conditions = strrep(conditions,' ','');
            inde = strfind(conditions(1, :), '=');
            if length(inde) > 1
                error('tau.ode.condition.fromstr: more than one equal sign in condition');
            end
            if isempty(inde)
                error('tau.ode.condition.fromstr: no equal sign found in condition');
            end

            inda = strfind(conditions(1, 1:inde), '(');
            indf = strfind(conditions(1, 1:inde), ')');
            indv = strfind(conditions(1, 1:inde), 'y');

            if length(indf) ~= length(inda) || length(indv) ~= length(inda)
                error('tau.ode.condition.fromstr: badly formed condition, parenthesis and variables do not match in number');
            end
            if any(indv > inda) ||  any(indv > indf) || any(inda > indf)
                error('tau.ode.condition.fromstr: badly formed condition, the order of functions and parenthesis is mixed');
            end
            ord = zeros(1, length(inda));
            arg = zeros(1, length(inda));
            coeff = zeros(1, length(inda));
            nvar = zeros(1, length(inda));
            for j = 1:length(inda)
                n = '';
                % find the number of the variable
                k = indv(j)+1;
                while k <= inda(j)-1
                    if isstrprop (conditions(k), 'digit')
                        n = [n conditions(k)]; %#ok
                        k = k + 1;
                    else
                        break;
                    end
                end
                if isempty(n)
                    nvar(j) = 1;
                else
                    nvar(j) = str2double(n);
                end

                % find the order of the derivative
                ord(j) = 0;
                for l = k:inda(j)-1
                    if strcmp(conditions(l), '''') ~= 1
                        error(['tau.ode.condition.fromstr: problem in the %d-term of "%s", ' ...
                               'it is not consistent!'], j, conditions)
                    end
                    ord(j) = ord(j) +1;
                end

                arg(j) = str2num(conditions(1, (inda(j)+1:indf(j)-1))); %#ok<ST2NM>
                if j == 1
                    if isempty(conditions(1, 1:indv(j)-1))
                        coeff(j) = 1;
                    elseif strcmp(conditions(1, 1:indv(j)-1), '-') == 1
                        coeff(j) = -1;
                    elseif strcmp(conditions(1, 1:indv(j)-1), '+') == 1
                        coeff(j) = 1;
                    else
                        coeff(j) = str2num(conditions(1, 1:indv(j)-1)); %#ok<ST2NM>
                    end
                else
                    if strcmp(conditions(1, indf(j-1)+1:indv(j)-1), '-') == 1
                        coeff(j) = -1;
                    elseif strcmp(conditions(1, indf(j-1)+1:indv(j)-1), '+') == 1
                        coeff(j) = 1;
                    else
                        coeff(j) = str2num(conditions(1, indf(j-1)+1:indv(j)-1)); %#ok<ST2NM>
                    end
                end
            end
            obj = tau.ode.condition(nequations);
            obj.var   = nvar';
            obj.order = ord';
            obj.point = arg';
            obj.coeff = coeff';
            obj.value = str2num(conditions(1, inde+1:end)); %#ok<ST2NM>
            obj.nvars = max(obj.var);
        end
    end
end
