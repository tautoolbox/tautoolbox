function [yn, varargout] = equations(problem)
%tau.ode.piecewise.linearSystem - Piecewise Lanczos' tau method for system
%   of linear ODEs.
%   yn = tau.ode.piecewise.linearSystem(problem)
%   [yn, info, residual, tauresidual] = tau.ode.piecewise.linearSystem(problem)
%
%input (required):
%   problem     = tau.problem object with the description of the problem
%
%output:
%   yn          = approximate solution (tau polynomial object)
%   info        = information structure
%   residual    = residual Tn*an-bn (tau polynomial object)
%   tauresidual = tau residual Rn*an (tau polynomial object)
%
%See also tau.ode.linear, tau.ode.piecewise.linearGIVP, tau.ode.piecewise.linear,
% tau.ode.linearSystem.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

name = 'tau.ode.linear.piecewise.equations';
tau.utils.validate_solver_arguments(problem, name, ...
            true,  ...                 % supports multiple equations
            false, ...                 % supports nonlinear problems
            true,  ...                 % supports multiple pieces solution
            false)                     % supports conditions only in the first piece

equations = problem.equations;
conditions = problem.conditions;
nequations = problem.nequations;
npieces = problem.npieces;

yn = tau.polynomial(problem.options);
%cn = zeros(n, nequations, npieces);
T = zeros(n*nequations*npieces);
b = zeros(n*nequations*npieces, 1);
nu = size(conditions, 1);

h = max(cell2mat(problem.height));  % evaluate operator heigth
nux = max(problem.condpart);

options = problem.options;
n = options.n;
options.n = n+h;

% prepare the conditions at the continuity nodes
derivOrder = max(problem.derivOrder);     % for each variable

for i = 1:npieces            % perform piecewise tau method npieces times
    options.domain = yn.pieces(i:i+1);
    options.pieces = 1;
    options.firstpiece = i==1;

    x = tau.polynomial(options);
    y = tau.operator(options);
    basis = polynomial.basis(options);

    li = (i-1)*n*nequations+1; % lower index of the block
    ui = i*n*nequations;       % upper index of the block

    % component of the global conditions related with this piece
    % incorporate the conditions in the system matrix and independent vector
    [C, bs] = basis.systemBlock(conditions, n, nequations);
    T(1:nu,li:ui) = C;
    b(1:nu)    = bs;


    idx = (i-1)*n*nequations + nu;
    pproblem = tau.problem(equations, yn.pieces(i:i+1), conditions, options);

    for neq = 1:nequations
        D = pproblem.equations{neq}.lhs(x{1},y);
        D = D.mat(n);

        rows = 1:(n-pproblem.condpart(neq));
        T(idx + rows,li:ui) = D(rows,:);
        b(idx + rows)   = basis.interp1(equations{neq}.rhs, rows(end));
        idx = idx + n - pproblem.condpart(neq);
        if nargout > 1
            ns = (nux+h)*(neq-1);
            nr = n+h-rows(end);
            R(ns+1:ns+nr,:) = D(rows(end)+1:n+h,:);
        end
    end

    % the conditions are defined at the begin and for the first pieces that was already done
    if npieces == 1
        break
    end

    % Continuity conditions, function and first nu-1 derivatives
    if i ~= 1
        cnd_i = 0;  % condition index
        for neq = 1:nequations
            nuk = derivOrder(neq);
            T(li+cnd_i:li+cnd_i+nuk-1,li+(neq-1)*n:li+neq*n-1) = -basis.polyvalb(yn.pieces(i), n, 0, nuk-1);
            cnd_i = cnd_i + nuk;
        end
    end
    if i ~= npieces
        cnd_i = 1;  % condition index
        for neq = 1:nequations
            nuk = derivOrder(neq);
            T(ui+cnd_i:ui+cnd_i+nuk-1,li+(neq-1)*n:li+neq*n-1) = basis.polyvalb(yn.pieces(i+1), n, 0, nuk-1);
            cnd_i = cnd_i + nuk;
        end
    end

end

solver = tau.utils.linearSystemSolver();
[yn, iter] = solver.solve(T, b, problem);
ycoeff = yn.coeff_n(n);

% info structure
if nargout > 1
    info.tau_solver = name;
    info.linear_system_solver = solver.algorithm;
    if iter ~= 0
        info.iterations = iter;
    end
    info.cond = cond(T);
    residual_v = T*ycoeff(:) - b;
    info.residual = norm(residual_v);
    varargout{1} = info;
end

% residual vector (tau polynomial object)
if nargout > 2
    r_v = reshape(residual_v, [], yn.nequations, yn.npieces);
    varargout{2} = tau.polynomial(r_v, problem.options);
end

% tau residual vector (tau polynomial object)
if nargout > 3
    varargout{3} = [];
end

end
