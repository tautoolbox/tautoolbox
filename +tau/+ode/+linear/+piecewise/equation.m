function [yn, varargout] = equation(problem)
%tau.ode.piecewise.linear - Piecewise Lanczos' tau method for linear ODE's.
%   yn = tau.ode.piecewise.linear(problem)
%   [yn, info] = tau.ode.piecewise.linear(problem)
%   [yn, info, residual, tauerror] = tau.ode.piecewise.linear(problem)
%
%input (required):
%   problem     = tau.problem object with the description of the problem
%
%output:
%   yn          = approximate solution (tau polynomial object)
%   info        = information structure
%   residual    = residual Tn*an-bn (tau polynomial object)
%   tauresidual = tau residual Rn*an (tau polynomial object)
%
%See also tau.ode.linear, tau.ode.piecewise.linearGIVP, tau.ode.linearSystem,
% tau.ode.piecewise.linearSystem.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

name = 'tau.ode.linear.piecewise.equation';
tau.utils.validate_solver_arguments(problem, name, ...
            false, ...                 % supports multiple equations
            false, ...                 % supports nonlinear problems
            true,  ...                 % supports multiple pieces solution
            false)                     % supports conditions only in the first piece

equation = problem.equations{1};
conditions = problem.conditions;
options = problem.options;

n = options.n;
h = problem.height{1};
pieces = options.pieces;
npieces = options.npieces;

nu = problem.nconditions;
T = zeros(n*npieces);
b = zeros(n*npieces,1);
R = zeros((nu+h)*npieces, n*npieces);

% global conditions right hand side
b(1:nu) = cellfun(@(x) x.value, conditions);

% Apply piecewise tau method "npieces" times
options.pieces = 1;
for i = 1:npieces
    options.domain = pieces(i:i+1);
    options.firstpiece = i==1;

    y = tau.operator(options);
    basis = polynomial.basis(options);

    D = equation.lhs(tau.polynomial(options),y);
    D = D.mat;

    li = (i-1)*n+1; % lower index of the block
    ui = i*n;       % upper index of the block
    % component of the global conditions related with this piece
    T(1:nu,li:ui) = basis.systemBlock(conditions, n, 1);

    T(li+nu:ui,li:ui) = D(1:end-nu,:);
    b(li+nu:ui,1) = basis.interp1(equation.rhs, n-nu);

    if nargout > 3
        R((i-1)*(nu+h)+1:i*(nu+h),li:ui) = D(end-nu+1:end,:);
    end

    % the conditions are defined at the begin and for the first pieces that was already done
    if problem.npieces == 1
        break
    end

    % Continuity conditions, function and first nu-1 derivatives
    if i ~= 1
        T(li:li+nu-1,li:ui) = -basis.polyvalb(pieces(i), n, 0, nu-1);
    end
    if i ~= problem.npieces
        T(ui+1:ui+nu,li:ui) = basis.polyvalb(pieces(i+1), n, 0, nu-1);
    end
end

% solve the linear system
solver = tau.utils.linearSystemSolver();
[yn, iter] = solver.solve(T, b, problem);
ycoeff = yn.coeff_n(n);

% info structure
if nargout > 1
    info.tau_solver = name;
    info.linear_system_solver = solver.algorithm;
    if iter ~= 0
        info.iterations = iter;
    end
    info.cond = cond(T);
    residual_v = T*ycoeff(:) - b;
    info.residual = norm(residual_v);
    varargout{1} = info;
end

% residual vector (tau polynomial object)
if nargout > 2
    r_v = reshape(residual_v, [], problem.nequations, problem.npieces);
    varargout{2} = tau.polynomial(r_v, problem.options);
end

% tau residual vector (tau polynomial object)
if nargout > 3
    nequations = 1;
    coeffs = zeros(n+h,nequations,npieces);
    coeffs(n-nu+1:end,1,:) = reshape(R*ycoeff(:), [], nequations, npieces);
    varargout{3} = tau.polynomial(coeffs, problem.options);
end

end
