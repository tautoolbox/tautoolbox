function [yn, varargout] = givpSystem(problem)
%tau.ode.piecewise.linearGIVPSystem - Lanczos' Tau solver for linear integro-differential problems,
%   for Generalized Initial Value Problems (GIVP) with several equations.
%   A GIVP has all conditions/constraints (initial, boundary or in the middle)
%   in the first piece. Note that if the problem only has a single piece then
%   even a Boundary Value Problem (BVP) is a GIVP because all the conditions
%   are in the first piece.
%   linearGIVPSystem returns the (n-1)th degree polynomial approximation yn=Pn*an,
%   on the orthogonal polynomial basis Pn, of the linearized integro-differential
%   problem dy/dx=f(x,y), via the solution of the Tau system Tn*an=bn.
%   The domain is divided in pieces and the solver acts on each subinterval
%   appropriately.
%
%   yn = tau.ode.piecewise.linearGIVPSystem(problem)
%   [yn, info, residual, tauresidual] = tau.ode.piecewise.linearGIVPSystem(problem)
%
%input (required):
%   problem     = tau.problem object with the description of the problem
%
%output:
%   yn          = approximate solution (tau polynomial object)
%   info        = information structure
%   residual    = residual Tn*an-bn (tau polynomial object)
%   tauresidual = tau residual Rn*an (tau polynomial object)
%
%See also tau.ode.linear, tau.ode.piecewise.linear,
% tau.ode.linearSystem, tau.ode.piecewise.linearSystem.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

name = 'tau.ode.linear.piecewise.givpSystem';
tau.utils.validate_solver_arguments(problem, name, ...
            true,  ...                 % supports multiple equations
            false, ...                 % supports nonlinear problems
            true,  ...                 % supports multiple pieces solution
            true)                      % supports conditions only in the first piece

equations = problem.equations;
conditions = problem.conditions;
nequations = problem.nequations;
yn = tau.polynomial(problem.options);

%infopieces.iterations = zeros(1,yn.npieces);
infopieces.cond = zeros(1,problem.npieces);
infopieces.residual = zeros(1,problem.npieces);
infopieces.tauresidual = zeros(1,problem.npieces);

% prepare the conditions at the continuity nodes
derivOrder = max(problem.derivOrder);     % for each variable

coeffs = zeros(problem.n, problem.nequations, problem.npieces);
argout = cell(max(nargout-2,0), 1);
if nargout > 2
    residual_v = zeros(problem.n, problem.nequations, problem.npieces);
    if nargout > 3
        h = max(cell2mat(problem.height));
        residual_t = zeros(problem.n+h, problem.nequations, problem.npieces);
    end
end

options = problem.options;
n = options.n;
options.pieces = 1;
% perform Tau method pieces times
for i = 1:yn.npieces
    options.domain = yn.pieces(i:i+1);

    % Tau solution for the partial problem
    pproblem = tau.problem(equations, yn.pieces(i:i+1), conditions, options);
    [z, info, argout{:}] = tau.ode.linear.equations(pproblem);
    coeffs(:,:,i) = z.coeff_n(n);

    if nargout > 1
        fields = {'cond', 'residual', 'tauresidual'};
        for j = 1:length(fields)
            infopieces.(fields{j})(i) = info.(fields{j});
        end
        if nargout > 2
            residual_v(1:argout{1}.n,:,i) = argout{1}.coeff_n(n);
            if nargout > 3
                residual_t(:,:,i) = argout{2}.coeff_n(n+h);
            end
        end
    end

    if i == yn.npieces
        break;
    end

    % update conditions for the next piece
    % ensures continuity of the function and (nu-1) first derivatives
    % for the nodes of integration
    ci = 1;                  % condition counter index
    for k=1:nequations
        sol = z{k};          % consider the k-th variable of the system
        for c=1:derivOrder(k)
            conditions{ci} = @(y) diff(y{k}, c-1, yn.pieces(i+1))-sol(yn.pieces(i+1));
            sol = diff(sol); % take the next derivative of the solution
            ci = ci +1;
        end
    end
    if length(conditions) ~= ci - 1
        conditions = conditions(1:ci-1);
    end
end

coeffs = reshape(coeffs, [], yn.nequations, yn.npieces);
yn = tau.polynomial(coeffs, problem.options);

% info structure
if nargout > 1
    info.tau_solver = name;
    fields = {'cond', 'residual', 'tauresidual'};
    for i = 1:length(fields)
        info.(fields{i}) = infopieces.(fields{i});
    end
    varargout{1} = info;
end

% residual vector (tau polynomial object)
if nargout > 2
    varargout{2} = tau.polynomial(residual_v, problem.options);
end

% tau residual vector (tau polynomial object)
if nargout > 3
    varargout{3} = tau.polynomial(residual_t, problem.options);
end

end
