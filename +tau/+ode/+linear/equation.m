function [yn, varargout] = equation(problem)
%tau.ode.linear - Lanczos' Tau solver for linear integro-differential problems,
%   with initial or boundary conditions.
%   tau.ode.linear returns the n-th degree polynomial approximation yn=Pn*an,
%   on the orthogonal polynomial basis Pn, of the linear integro-differential
%   problem dy/dx=f(x,y), via the solution of the Tau system Tn*an=bn.
%
%   yn = tau.ode.linear(problem)
%   [yn, info] = tau.ode.linear(problem)
%   [yn, info, residual, tauresidual] = tau.ode.linear(problem)
%
%input (required):
%   problem     = tau.problem object with the description of the problem
%
%output:
%   yn          = approximate solution (tau polynomial)
%   info        = information structure
%   residual    = residual Tn*an-bn (tau polynomial)
%   tauresidual = tau residual Rn*an (tau polynomial)
%
%See also tau.ode.piecewise.linearGIVP, tau.ode.piecewise.linear, tau.ode.linearSystem,
% tau.ode.piecewise.linearSystem, tau.ode.nonlinear and tau.ode.piecewise.nonlinear.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

% check if this solver is able to address the problem
name = 'tau.ode.linear.equation';
tau.utils.validate_solver_arguments(problem, name, ...
            false, ...                 % supports multiple equations
            false, ...                 % supports nonlinear problems
            false, ...                 % supports multiple pieces solution
            false)                     % supports conditions only in the first piece

% define for conveniency (shorter to type and read)
equation = problem.equations{1};
options = problem.options;

n = options.n;
nu = problem.nconditions;
h = problem.height{1};       % evaluate operator heigth

% prepare the matrices that will hold the linear system to be solved
T = zeros(n, n);
R = zeros(nu+h, n);  % build with extended size to accomodate height

% incorporate the conditions in the system matrix and independent vector
basis = polynomial.basis(options);
[C, bs] = basis.systemBlock(problem.conditions, n, 1);

% complete the remaining lines
options.n = n+h;
operator = equation.lhs(tau.polynomial(options), tau.operator(options));
D = operator.mat(n);

T(1:nu,:)   = C;
T(1+nu:n,:) = D(1:n-nu,:);
R(:,:)      = D(n-nu+1:end,:);

bf = basis.interp1(equation.rhs, n-nu);
b = [bs; bf];

% solve the Tau linear system
solver = tau.utils.linearSystemSolver();
[yn, iter] = solver.solve(T, b, problem);

% info structure
if nargout > 1
    info.tau_solver = name;
    info.linear_system_solver = solver.algorithm;
    if iter ~= 0
        info.iterations = iter;
    end
    yc = yn.coeff_n(n);
    info.cond = cond(T);
    residual_v = T*yc - b;
    info.residual = norm(residual_v);
    tauresidual_v = R*yc;
    info.tauresidual = norm(tauresidual_v);
    varargout{1} = info;
end

% residual vector (tau polynomial)
if nargout > 2
    varargout{2} = tau.polynomial(residual_v, problem.options);
end

% tau residual vector (tau polynomial)
if nargout > 3
    coeffs = zeros(n+h,1);
    coeffs(n-nu+1:end) = tauresidual_v;
    varargout{3} = tau.polynomial(coeffs, problem.options);
end

end
