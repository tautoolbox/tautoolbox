function [yn, varargout] = equations(problem)
%tau.ode.linearSystem - Lanczos' tau method for system of linear ODEs.
%   tau.ode.linearSystem returns the (n-1)th degree polynomial approximation
%   yn=Pn*an, on the orthogonal polynomial basis Pn, of the linear
%   integro-differential problem dy/dx=f(x,y), via the solution of the
%   Tau system Tn*an=bn.
%
%   yn = tau.ode.linearSystem(problem)
%   [yn, info] = tau.ode.linearSystem(problem)
%   [yn, info, residual, tauresidual] = tau.ode.linearSystem(problem)
%
%input (required):
%   problem     = tau.problem object with the description of the problem
%
%output:
%   yn          = approximate solution (tau polynomial)
%   info        = information structure
%   residual    = residual Tn*an-bn (tau polynomial)
%   tauresidual = tau residual Rn*an (tau polynomial)
%
%See also tau.ode.linear, tau.ode.piecewise.linearGIVP, tau.ode.piecewise.linear,
% tau.ode.linearSystem, tau.ode.piecewise.linearSystem.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

name = 'tau.ode.linear.equations';
tau.utils.validate_solver_arguments(problem, name,  ...
            true,  ...                 % supports multiple equations
            false, ...                 % supports nonlinear problems
            false, ...                 % supports multiple pieces solution
            false)                     % supports conditions only in the first piece

% define for conveniency (shorter to type and read)
equations = problem.equations;
conditions = problem.conditions;
nequations = problem.nequations;
n = problem.n;
nu = problem.nconditions;              % problem's number of conditions
nux = max(problem.condpart);           % order of the largest derivative for variable k
h = max(cell2mat(problem.height));     % evaluate operator heigths

% prepare the matrices that will hold the linear system to be solved
T = zeros(n*nequations);               % T matrix by var*eq blocks
R = zeros((nux+h)*nequations, n*nequations);
b = zeros(n*nequations, 1);            % b vector by blocks

% incorporate the conditions in the system matrix and independent vector
basis = polynomial.basis(problem.options);
[C, bs] = basis.systemBlock(conditions, n, nequations);
T(1:nu, :) = C;
b(1:nu)    = bs;

idx = nu;
options = problem.options;
options.n = n+h;
x = tau.polynomial(options);
for neq = 1:nequations
    operator = problem.equations{neq}.lhs(x{1},tau.operator(options));
    D = operator.mat(n);

    rows = 1:(n-problem.condpart(neq));
    T(idx + rows,:) = D(rows,:);
    b(idx + rows)   = basis.interp1(equations{neq}.rhs, rows(end));
    idx = idx + n - problem.condpart(neq);

    ns = (nux+h)*(neq-1);
    nr = n+h-rows(end);
    R(ns+1:ns+nr,:) = D(rows(end)+1:n+h,:);
end

% Solving the associated linear system
solver = tau.utils.linearSystemSolver();
[yn, iter] = solver.solve(T, b, problem);

% info structure
if nargout > 1
    info.tau_solver = name;
    info.linear_system_solver = solver.algorithm;
    yc = yn.coeff_n(n);
    residual_v = T*reshape(yc, size(b,1), 1) - b;
    tauresidual_v = R*yc(:);
    info.cond = cond(T);
    info.tauresidual = norm(tauresidual_v);
    if iter ~= 0
        info.iterations = iter;
    end
    info.residual = norm(residual_v);
    varargout{1} = info;
end

% residual vector (tau polynomial object)
if nargout > 2
    varargout{2} = tau.polynomial(reshape(residual_v, [], nequations), options);
end

% tau residual vector (tau polynomial object)
if nargout > 3
    coeffs = zeros(n+h,nequations);
    coeffs(n-nux+1:end,:) = reshape(R*yc(:), [], nequations);
    varargout{3} = tau.polynomial(coeffs, problem.options);
end

end
