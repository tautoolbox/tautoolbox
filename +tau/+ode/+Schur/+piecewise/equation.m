function c = equation(problem)
%tau.ode.piecewise.Schur - Iterative Lanczos' tau method based on Schur complements (piecewise version).
%   a = tau.ode.piecewise.Schur(ode, domain, conditions, options) returns the
%   coefficients a satisfying yn = Pa. Along with the approximation of yn
%   computes an estimation for the error (used as stopping criterium).
%   This version allows for partioned domains.
%
%input (required):
%   problem     = tau.problem object with the description of the problem
%
%output:
%   yn          = approximate solution (tau polynomial object)
%   info        = information structure
%   residual    = residual Tn*an-bn (tau polynomial object)
%   tauresidual = tau residual Rn*an (tau polynomial object)
%
%See also tau.ode.linear, tau.ode.linearSystem and tau.ode.piecewise.linearSystem.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

name = 'tau.ode.Schur.piecewise.equation';
tau.utils.validate_solver_arguments(problem, name, ...
            true,  ...                 % supports multiple equations
            false, ...                 % supports nonlinear problems
            true,  ...                 % supports multiple pieces solution
            true)                      % supports conditions only in the first piece

ode = problem.equations;
conditions = problem.conditions;
options = problem.options;
n = options.n;

nu = size(conditions, 1);
c = tau.polynomial(options);

for i = 1:options.npieces
    % solve the problem at each step
    problem = tau.problem(ode, c.pieces(i:i+1), conditions, options);
    a = tau.ode.Schur(problem);
    %coeff(:,i) = a.coeff_n(n); %#ok<AGROW,NASGU>

    if i < options.npieces % update initial value conditions for the next piece
        options.domain = c.pieces(i:i+1);
        x = tau.polynomial(options);
        y = tau.operator(options);

        conditions(1:nu, 3) = J(i+1);
        for k = 1:nu
            tmp = y.N(1:n, 1:n)^conditions(k, 2)*a.coeff;
            conditions(k, 5) = x.basis.polyval(tmp, J(i+1));
        end
    end
end

end
