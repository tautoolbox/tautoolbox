function [yn, varargout] = solve(problem, varargin)
%tau.ode.solve - Lanczos' Tau solver for integro-differential problems,
%   with initial/boundary/constraint conditions.
%   Based on the type of problem tau.ode.solve will pick the appropriate solver.
%   There are specific solvers for linear/nonlinear, single equation/systems of
%   equations and whole domain or piecewise solutions.
%   tau.ode.solve returns the nth degree polynomial approximation yn=Pn*an,
%   on the orthogonal polynomial basis Pn, of the linear integro-differential
%   problem dy/dx=f(x,y), via the solution of the Tau system Tn*an=bn.
%
%   yn = tau.ode.solve(problem)
%   [yn, info, residual, tauresidual, cauchyerror] = tau.ode.solve(problem)
%
%input (required):
%   problem      = problem (tau.problem object).
%
%output:
%   yn           = approximate solution (tau.polynomial object).
%   info         = information structure.
%   residual     = residual Tn*an-bn (tau.polynomial object)
%   tauresidual  = tau residual Rn*an (tau.polynomial object)
%   cauchyerror  = Cauchy error yn-y(n-2) (tau.polynomial object).
%
%See also tau.ode.linear, tau.ode.piecewise.linearGIVP, tau.ode.piecewise.linear,
%         tau.ode.linearSystem, tau.ode.piecewise.linearSystem and tau.ode.Schur.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

if ~isa(problem, 'tau.problem')
    error('tau.ode.solve: the input argument must be a tau.problem')
end

% The fifth argout (Cauchy error) is evaluated here and so ask at most 4
nargs = min(nargout-1, 3);

% Factors that impact the solver variants:
% Number of equations
%   * single equation;
%   * multiple equations;
% Problem linearity
%   * linear;
%   * non-linear (currently linearized);
% Piecewise solution
%   * single segment;
%   * multi-segment with all the conditions in the first piece (problem.isGIVP == true);
%   * multi-segment with conditions in several segments (pieces).

% Select approach based on the previous factors
if problem.nequations == 1     % Tau method for a single equation.
    if problem.npieces == 1
        [yn, varargout{1:nargs}] = tau.ode.Schur.equation(problem, varargin{:});
    elseif problem.isGIVP
        [yn, varargout{1:nargs}] = tau.ode.Schur.piecewise.givp(problem, varargin{:});
    else
        [yn, varargout{1:nargs}] = tau.ode.Schur.piecewise.equation(problem, varargin{:});
    end
else
    if problem.npieces == 1
        [yn, varargout{1:nargs}] = tau.ode.Schur.equations(problem, varargin{:});
    elseif problem.isGIVP
        [yn, varargout{1:nargs}] = tau.ode.Schur.piecewise.givpSystem(problem, varargin{:});
    else
        [yn, varargout{1:nargs}] = tau.ode.Schur.piecewise.equations(problem, varargin{:});
    end
end

if nargout == 0
    plot(yn);
    return
end

if nargout == 5
    problem.degree = problem.degree-2;
    ynp = tau.ode.Schur.solve(problem, varargin{:});
    varargout{4} = yn-ynp;
end

end
