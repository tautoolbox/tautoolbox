function [yn] = equation(problem, varargin)
%tau.ode.Schur - Iterative Lanczos' tau method based on Schur complements.
%   a = tau.ode.Schur(varargin) returns the a coefficients satisfying
%   yn = Pa. Along with the approximation of yn computes an estimation
%   for the error (used as stopping criterium).
%
%input (required):
%   problem     = tau.problem object with the description of the problem
%
%output:
%   yn          = approximate solution (tau polynomial object)
%   info        = information structure
%   residual    = residual Tn*an-bn (tau polynomial object)
%   tauresidual = tau residual Rn*an (tau polynomial object)
%
%See also tau.ode.linear, tau.ode.linearSystem and tau.ode.piecewise.linearSystem.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

name = 'tau.ode.Schur.equation';
tau.utils.validate_solver_arguments(problem, name, ...
            false, ...                 % supports multiple equations
            false, ...                 % supports nonlinear problems
            true,  ...                 % supports multiple pieces solution
            true)                      % supports conditions only in the first piece

equation = problem.equations{1};
%domain = problem.domain;
conditions = problem.conditions;
options = problem.options;

params = tau.utils.parse_schur_params(name, varargin{:});

options.n = params.maxDim;
n = params.initialLU;
m = params.sizeBlock;
nu = size(conditions, 1);

T = zeros(options.n);
b = zeros(options.n, 1);

basis = polynomial.basis(options);
[C, b(1:nu)] = basis.systemBlock(conditions, options.n, 1);
operator = equation.lhs(tau.polynomial(options), tau.operator(options));
D = operator.mat();

T(1:nu    , :) = C;
T(1+nu:end, :) = D(1:end-nu, :);
b(nu+1:n) = basis.interp1(equation.rhs, n-nu);

[L, U, P] = lu(T(1:n, 1:n));
a = U\(L\(P*b(1:n)));
tau_res = norm(T(n+1:n+1+m,1:n)*a);

% get improved accuracy if nedeed
while tau_res > params.precision && n+2*m <= params.maxDim
    % case 'blu'
    n = n + m;
    a = T(1:n, 1:n)\b(1:n);

    tau_res = norm(T(n+1:n+m,1:n)*a);
end

% build approximation on the specified basis
options.n = n;
yn = tau.polynomial(a, options);

if tau_res > params.precision
    warning('tau.ode.Schur: required accuracy not achieved. You may increase parameter maxDim')
end

end
