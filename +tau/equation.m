classdef equation
%equation - Class to handle the integro-differential equations to be solved by
%   the TauToolbox
%   eq = tau.equation() creates an object eq.
%
%input:
%   string
%
%output:
%   eq = one (or more) equation to be solved by The tau method (tau.equation object).
%
%examples:
%   y = tau.equation('y''+y=0');
%
%See also tau.eig.condition, tau.ode.condition and tau.operator.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    properties (GetAccess=public, SetAccess=private)
        lhs ;       % a function that represents the left side functions
        rhs         % a function that represents the right hand side functions
        tree;       % parser tree of functions
        varnames;   % name of the variable, it is 'y' by default
    end

    methods (Access = public)
        function obj = equation(form)
            if isa(form, 'tau.equation')
                obj = form;
                return
            elseif isa(form, 'function_handle')
                obj.lhs = form;
                obj.rhs = @(x) x*0;
            elseif iscellstr(form) || ischar(form) || isstring(form)
                [obj.lhs, obj.rhs] = tau.equation.fromstr(form);
            elseif iscell(form)
                switch length(form)
                    case 1
                        if isa(form{1}, 'tau.equation')
                            obj = form{1};
                            return
                        end
                        obj.lhs = form{1};
                        obj.rhs = @(x) 0;
                    case 2
                        obj.lhs = form{1};
                        if length(form{2}) == 1
                            obj.rhs = form{2};
                        else % eigenvalue problem
                            % set the algebraic eigenproblem from handle function interface
                            if nargin(form{2}{1}) == 1 || nargin(form{2}{2}) == 1
                                obj.rhs = tau.equation.seteigproblem(form{2});
                            end
                        end
                    otherwise
                        error('tau.equation: wrong cell argument')
                end
            end
%             % set the algebraic eigenproblem from handle function interface
%             if length(obj.rhs) > 1
%                 if nargin(obj.rhs{1}) == 1 || nargin(obj.rhs{2}) == 1
%                     obj.rhs = tau.equation.seteigproblem(obj.rhs);
%                 end
%             end
            if iscell(obj.rhs)
                obj.varnames = {'x', 'y', 'lambda'};
            elseif nargin(obj.lhs) == 3
                obj.varnames = {'x', 'y', 'yo'};
            else
                obj.varnames = {'x', 'y'};
            end
        end

        function res = info(obj, options)
            coeff = zeros(options.n,options.nequations);
            coeff(options.n,:)= 1;
            options.n = 2;
            options.pieces = 1;
            x = tau.polynomial(options);
            if nargin(obj.lhs) == 2
                D = obj.lhs(x{1}, tau.operator(options));
            else
                yo = tau.polynomial(coeff, options);
                D = obj.lhs(x{1}, tau.operator(options), yo);
            end
            res.height = D.opHeight();
            res.hasIntegral = any(cell2mat(D.hasIntegral));
            res.derivOrder = cell2mat(D.odiff);
            res.isEig = iscell(obj.rhs);
            % eigenvalue problems take into account the rhs to determine the operator's height
            if res.isEig
                for i = 1:length(obj.rhs)
                    if ~isempty(obj.rhs{i})
                        % do not consider function handles with 1 variable (related with lambda)
                        if nargin(obj.rhs{i}) ~= 1
                            D = obj.rhs{i}(tau.polynomial(options), tau.operator(options));
                        end
                        res.height = max(res.height, D.opHeight());
                    end
                end
            end

        end

        function disp(obj)
            fprintf('Equation:\n');
            fprintf('  Left hand side:\n  ');
            disp(obj.lhs)
            fprintf('  Right hand side:\n  ');
            disp(obj.rhs)
        end
    end

    methods ( Access = public, Static = true)
        function [lhs, rhs] = fromstr(form)
            %fromstr - Split equation from text representation.
            %   [lrs, rhs] = fromstr(form) separates the lhs and the rhs.
            %
            %input:
            %   form = equation to solve (cell of char).
            %
            %output:
            %   lhs = left hand side (char).
            %   rhs = rigth hand side (char).
            %
            %example:
            %   [lhs, rhs] = fromstr({'diff(y1, 2) + (x^2)*diff(y2) = 7*x^2 + 1';...
            %                         'diff(y2) + 3*diff(y1) = 3*x^5 + 21*x'}) returns:
            %   lhs = 'diff(y1, 2) + (x^2)*diff(y2)'
            %         'diff(y2) + 3*diff(y1)'
            %   rhs = '7*x^2 + 1'
            %         '3*x^5 + 21*x'

            if iscell(form)
                form = form{1};
            end
            isLinearized = false;

            form = strrep(form, ' ', '');
            k = strfind(form, '=');
            if length(k) ~= 1
                error('tau.equation.fromstr: equation does not have an equal sign')
            end

            % Heuristic to determin if the equation is linearized
            lhs = tau.equation.normalizeOperator(form(1:k-1));
            if contains(lhs, 'yo')
                isLinearized = true;
                lhs = eval(['@(x,y,yo)' vectorize(lhs)]);
            else
                lhs = eval(['@(x,y)' vectorize(lhs)]);
            end

            if ~contains(form(k+1:end), 'lambda')
                if ~isLinearized
                    rhs = eval(['@(x)' vectorize(form(k+1:end)) '+0*x']);
                else
                    rhs = eval(['@(x,yo)' vectorize(form(k+1:end))]);
                end
            else              % assume eig problem
                % here we will assume that all the terms are on the form of
                % lambda^p*f(x,y)
                rhs = cell(1);
                terms = tau.equation.parseEquation(form(k+1:end));
                for j = 1:length(terms)
                    k = strfind(terms{j}, 'lambda');
                    if isempty(k)
                        error('tau.equation.fromstr: term whitout lambda for an eigenvalue problem')
                    end
                    if k(1) ~= 1
                        error('tau.equation.fromstr: please place the lambda at the begin of a term')
                    end
                    if strcmp(terms{j}(7), '*')
                        pow = 1;
                        start = 8;
                    elseif strcmp(terms{j}(7), '^')
                        start = strfind(terms{j}(8:end),'*');
                        pow = str2double(terms{j}(8:start(1)+6));
                        start = start(1) + 8;
                    else
                        error('tau.equation.fromstr: unknown eig expression')
                    end
                    if length(rhs) < pow || isempty(rhs{pow})
                        rhs{pow} = terms{j}(start:end);
                    else
                        rhs{pow} = [rhs{pow}, ' + ', terms{j}(start:end)];
                    end
                end
                for k = 1:length(rhs)
                    if ~isempty(rhs{k})
                        rhs{k} = eval(['@(x,y) ' rhs{k}]);
                    end
                end
            end
        end

        function funcell = parseEquation(equation)
            %fromstr - Split equation.
            %   [lrs, rhs] = fromstr(equation) separates the lhs and the rhs.
            %
            %input:
            %   equation = equation to solve (cell of char).
            %
            %output:
            %   lhs = left hand side (char).
            %   rhs = rigth hand side (char).
            %
            %example:
            %   [lhs, rhs] = fromstr({'diff(y1, 2) + (x^2)*diff(y2) = 7*x^2 + 1';...
            %                         'diff(y2) + 3*diff(y1) = 3*x^5 + 21*x'}) returns:
            %   lhs = 'diff(y1, 2) + (x^2)*diff(y2)'
            %         'diff(y2) + 3*diff(y1)'
            %   rhs = '7*x^2 + 1'
            %         '3*x^5 + 21*x'

            if ~ischar(equation)
                error('tau.equation.parseEquation: wrong argument')
            end

            % remove all spaces
            equation = strrep(equation, ' ', '');

            % insert a "+" before every "-" for easier parsing
            equation = strrep(equation, '-', '+-');

            % if the first char is a "+" discard it
            if equation(1) == '+'
                equation = equation(2:end);
            end

            % find the nodes where to break the string, those will be the plus signs
            % outside of parenthesis expressions

            nodes = NaN(1,1); ind = 1; %or nodes = [];
            parenthesis_level = 0;

            for k = 1:length(equation)
                switch equation(k)
                    case '('
                        parenthesis_level = parenthesis_level + 1;
                    case ')'
                        parenthesis_level = parenthesis_level - 1;
                    case '+'
                        if parenthesis_level == 0
                            nodes(ind) = k; ind = ind + 1; %or nodes = [nodes, k];
                        end
                end
            end

            nodes(ind) = length(equation)+1; %or nodes = [nodes, length(equation)+1];
            funcell = cell(length(nodes), 1);

            start = 1;
            for k = 1:length(nodes)
                funcell{k} = equation(start:nodes(k)-1);
                % funcell{k} = normalizeDiff(funcell{k});
                % funcell{k} = normalizeOperator(funcell{k});
                % funcell{k} = normalizePower(funcell{k});
                start = nodes(k)+1;
            end
        end

        function expression = normalizeOperator(expression)
            idx = strfind(expression, 'y');
            nvar = 1;
            while nvar <= length(idx)
                k = idx(nvar);
                if k > 1 && isstrprop(expression(k-1), 'alpha')
                    % in this case y belongs to another object and so ignore it
                    nvar = nvar + 1;
                    continue
                end
                if k < length(expression) && isstrprop (expression(k+1), 'alpha')
                    % y char belongs to a word so ignore it
                    nvar = nvar + 1;
                    continue
                end
                if (k < length(expression) && ~isstrprop (expression(k+1), 'alphanum')) || k == length(expression)
                    % y is a single variable so add it one
                    expression = [expression(1:k) '1' expression(k+1:end)];
                end

                s = k; % start of the var number
                while k < length(expression) && isstrprop(expression(k+1), 'digit')
                    k = k +1;
                end
                expression = [expression(1:s) '(' expression(s+1:k) ')' expression(k+1:end)];
                idx = strfind(expression, 'y');
                nvar = nvar + 1;
            end
        end

        function expression = normalizeDiff(expression)
            idx = strfind(expression, 'diff(');
            ndiff = 1;
            while ndiff <= length(idx)
                k = idx(ndiff) + 6; % 6 == length('diff(')+1
                parenthesis_level = 1;
                while k <= length(expression)
                    switch expression(k)
                        case '('
                            parenthesis_level = parenthesis_level + 1;
                        case ')'
                            parenthesis_level = parenthesis_level - 1;
                    end
                    if parenthesis_level == 0
                        break
                    end
                    k=k+1;
                end
                if ~contains(expression(idx(1)+6: k),',') %or if isempty(strfind(expression(idx(1)+6: k),','))
                    expression = [expression(1:k-1) ',1)' expression(k+1:end)];
                end
                ndiff = ndiff + 1;
                idx = strfind(expression, 'diff(');
            end
        end

        function expression = normalizePower(expression)
            expression = strrep(expression, '.^', '^');

            idx = strfind(expression, '^');
            while ~isempty(idx)
                idx = idx(1);
                % get basis
                if strcmp(expression(idx-1), ')')
                    parenthesis_level = -1;
                    k = idx-2;
                    while k > 0
                        switch expression(k)
                            case '('
                                parenthesis_level = parenthesis_level + 1;
                            case ')'
                                parenthesis_level = parenthesis_level - 1;
                        end
                        if parenthesis_level == 0
                            break
                        end
                        k=k+1;
                    end
                    if k == 0
                        error('tau.equation.normalizePower: unbalanced parenthesis')
                    end
                    basis = expression(k+1, idx-1);
                    % remove parenthesis (highest index first)
                    expression(idx-1) = [];
                    expression(k) = [];
                    idx = idx - 2;
                else
                    exp1 = expression(idx-1:-1:1);
                    tok = strtok(exp1, '+-*/()');
                    basis = tok(end:-1:1);
                end

                % get exponent
                if strcmp(expression(idx-1), ')')
                    parenthesis_level = 1;
                    k = idx+2;
                    while k <= length(expression)
                        switch expression(k)
                            case '('
                                parenthesis_level = parenthesis_level + 1;
                            case ')'
                                parenthesis_level = parenthesis_level - 1;
                        end
                        if parenthesis_level == 0
                            break
                        end
                        k=k+1;
                    end
                    if k > length(expression)
                        error('tau.equation.normalizePower: unbalanced parenthesis')
                    end
                    exponent = expression(idx+1, k-1);
                    % remove parenthesis  (highest index first)
                    expression(k) = [];
                    expression(idx+1) = [];
                else
                    exponent = strtok(expression(idx+1:end), '+-*/()');
                end
                exp1 = ['power(', basis, ',', exponent, ')'];
                expression = [expression(1:idx-length(basis)-1), ...
                              exp1, ...
                              expression(idx+1+length(exponent):end)];
                idx = strfind(expression, '^');
            end
        end

        function rhs = seteigproblem(form)
            %seteigproblem - set rhs equation from cell representation in eigenvalue problems.
            %   rhs = seteigproblem(form) separates the lambda terms from the ^(x,y) terms.
            %
            %input:
            %   form = rhs with lambda terms and respective (x,y) terms (cell of char).
            %
            %output:
            %   rhs = rigth hand side with (x,y) terms in correct position (char).

            % check if terms in form are as expected
            if mod(length(form),2) ~= 0
                error('tau.equation.seteigproblem: wrong number of terms in the rhs')
            end
            if nargin(form{1}) == 1 % lambda terms left
                get_iell = 1:2:length(form);
                get_ifun = 2:2:length(form);
            else                    % lambda terms rigth
                get_ifun = 1:2:length(form);
                get_iell = 2:2:length(form);
            end

            % check for polynomial degrees in lambda
            deg = zeros(1,length(form)/2);
            x = tau.polynomial();
            for k = 1:length(form)/2
                px = form{get_iell(k)}(x);
                vs = px.power_coeff;
                if vs(end) == 1 && ~any(vs(1:end-1))
                    deg(k) = length(vs) - 1;
                else
                    error('tau.equation.seteigproblem: only prepared for monomial terms in the rhs')
                end
            end
            rhs = cell(1, max(deg));
            for k = 1:length(form)/2
                rhs{deg(k)} = form{get_ifun(k)};
            end

        end
    end

end
