classdef problem
%problem - Class to that holds all the elements of the Tau problem
%
%   p = tau.problem(equation, domain, condition, options) creates an object p.
%
%output:
%   p = object with the problem to be solved by the Tau Toolbox
%
%examples:
%   p = tau.problem('y''-y=0', [-1 1], 'y(0)=1', tau.settings());
%
%See also tau.ode.condition, tau.operator, tau.equation and tau.polynomial.

% Copyright (C) 2022, University of Porto and Tau Toolbox developers.
%
% This file is part of Tau Toolbox.
%
% Tau Toolbox is free software: you can redistribute it and/or modify it
% under the terms of version 3 of the GNU Lesser General Public License as
% published by the Free Software Foundation.
%
% Tau Toolbox is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
% General Public License for more details.

    properties (SetAccess=private)
        equations    % equations that define the problem
        domain       % integration interval
        conditions   % set of conditions
        nconditions  % number of conditions of the system
        nequations   % number of variable components
        npieces      % number of pieces
        options      % tau.settings object with problem options
        derivOrder   % maximum order of derivative for each variable per equation
        condpart     % partition of conditions between the equations
        height       % height of the operator for each equation
        kind         % kind of problem
        isGIVP       % are all the condition/constraints in the first piece?
        isLinearized % is this problem linearized?
    end

    properties
        degree       % polynomial approximation degree of the operator
    end

    properties (Dependent)
        n            % degree + 1
    end

    properties (Constant = true)
        validKinds = {'auto', 'eig', 'ide', 'ode'};
    end

    methods (Access = public)
        function obj = problem(equation, domain, conditions, options)
            if nargin < 3 || nargin > 4
                error('tau.problem: wrong number of arguments (3 or 4 allowed)')
            end

            % deal with options (domain is included here)
            if nargin == 3
                options = tau.settings();
            end
            obj.options = options;
            obj.degree = options.degree;

            % deal with the domain (and pieces)
            [obj.domain, obj.options.pieces] = tau.problem.clean_domain(domain, obj.options);
            obj.options.domain = obj.domain;

            % deal with equations
            equation = tau.problem.clean_equations(equation);
            obj.nequations = size(equation, 1);
            obj.options.nequations = obj.nequations;

            [hasIntegral, isEig, obj.derivOrder, ...
            obj.height, obj.equations, obj.isLinearized] = tau.problem.process_equations(equation, obj.options);

            % deal with conditions
            conditions = tau.problem.clean_conditions(conditions, obj.nequations);
            [obj.conditions, obj.nconditions, obj.isGIVP] = ...
            tau.problem.process_conditions(conditions, obj.nequations, obj.domain, obj.options.pieces);

            % assign the partition of conditions among equations
            obj.condpart = tau.problem.partition_conditions(obj.derivOrder, obj.nconditions);

            % Use heuristics to determine the type of problem
            if strcmp(obj.options.kind, 'auto')
                if any(isEig)
                    obj.kind = 'eig';
                elseif hasIntegral
                    obj.kind = 'ide';
                else
                    obj.kind = 'ode';
                end
            else
                obj.kind = obj.options.kind;
            end

            % validation returns an object to ensure that conditions that can be solved
            % are done automatically
            obj = obj.validate();
        end

        function [varargout] = solve(obj)
            [varargout{1:nargout}] = tau.solve(obj);
        end

        function obj = validate(obj)
            % 1) The number of conditions is correct
            if sum(max(obj.derivOrder)) ~= obj.nconditions
                warning('tau.problem: the number of conditions does not fit with the order of equations');
            end

            % 2) Validate the type of problem
            % If possible recover issuing a warning, if not issue an error
            switch obj.kind
                case 'eig'
                    % currently eigenproblem only work for 1 equation
                    if obj.nequations > 1
                        error('tau.problem: not yet prepared for systems of differential eigenvalue problems');
                    end
                case 'ide'
                    if obj.options.npieces ~= 1
                        warning('tau.problem: Integro-Differential problems do not support piecewise solutions');
                        obj.options.pieces = 1;
                    end
            end
            obj.npieces = obj.options.npieces;
        end

        function disp(obj)
            fprintf('Tau problem of type %s:\n', obj.kind);
            fprintf(' * Integration domain: [%f, %f]\n', obj.domain(1), obj.domain(2));
            if obj.nequations == 1
                fprintf(' * 1 equation:\n')
                fprintf('   lhs:')
                disp(obj.equations{1}.lhs)
                fprintf('   rhs:')
                disp(obj.equations{1}.rhs)
            else
                fprintf(' * System of %d equations:\n', obj.nequations)
                for k = 1:obj.nequations
                    fprintf('   Equation #%d:\n', k)
                    fprintf('     lhs:')
                    disp(obj.equations{k}.lhs)
                    fprintf('     rhs:')
                    disp(obj.equations{k}.rhs)
                end
            end
            if obj.nconditions == 0
                fprintf(' * No condition/constraints\n')
            elseif obj.nconditions == 1
                fprintf(' * 1 condition/constraint:\n')
                fprintf('    ')
                disp(obj.conditions{1})
            else
                fprintf(' * %d conditions/constraints:\n', obj.nconditions)
                for k = 1:obj.nconditions
                    fprintf('   #%d: ', k)
                    disp(obj.conditions{k})
                end
            end
        end
    end

    methods

        function value = get.n(obj)
            value = obj.degree + 1;
        end

        function obj = set.degree(obj, value)
            if value ~= floor(value) || value < 0
                warning('tau.operator: degree must be positive and integer, setting default value')
                obj.degree = tau.settings.session__('degree');
            else
                obj.degree = value;
            end
            obj.options.n = obj.degree + 1;
        end
    end

    methods (Static = true)
        function k = isKind(kind)
            k = ismember(kind, tau.problem.validKinds);
        end

        % deal with the domain interval
        function [domain, pieces] = clean_domain(domain, options)
            if ~isscalar(domain) && isvector(domain)
                if any(diff(domain) <= 0)
                    error('tau.problem: the domain interval values should be an increasing sequence')
                end
                if length(domain) > 2
                    pieces = domain;
                    domain = domain([1,end]);
                    return
                end
            end

            if isscalar(options.pieces)
                pieces = linspace(domain(1), domain(2), options.npieces +1);
            end
        end

        function equation = clean_equations(equation)
            if ischar(equation) || isstring(equation) || isa(equation, 'function_handle')
                equation = {equation};
            end

            % convert strings to char arrays (tackles "..." as '...')
            % we can not test for a cellstr because in matlab iscellstr only applies to char arrays not strings
            if iscell(equation)
                for i = 1:length(equation)
                    equation{i} = convertStringsToChars(equation{i});
                end
            end
        end

        function [hasIntegral, isEig, derivOrder, height, equations, isLinearized] = process_equations(equation, options)
            nequations = size(equation, 1);
            hasIntegral = 0;
            isEig = zeros(nequations,1);
            derivOrder = zeros(nequations);
            height = cell(nequations, 1);
            equations = cell(nequations, 1);
            isLinearized = false;
            for k = 1:nequations
                if size(equation, 2) > 2 || (size(equation, 2) == 2 && isa(equation{k,2}, 'function_handle') && nargin(equation{k,1}) == 2 && nargin(equation{k,2}) == 2)
                    equation = {equation{1}, equation(2:end)};
                end
                equations{k} = tau.equation(equation(k,:));
                if nargin(equations{k}.lhs) == 3
                    isLinearized = true;
                end
                info = equations{k}.info(options);
                height{k} = info.height;
                derivOrder(k,:) = info.derivOrder;
                isEig(k) = info.isEig;
                hasIntegral = hasIntegral || info.hasIntegral;
            end
        end

        function conditions = clean_conditions(conditions, nequations);
            %% Process conditions
            if isa(conditions, 'function_handle')
                conditions = conditions(tau.ode.condition(nequations));
            end
            % Normalize all conditions to be in a cell array
            if ischar(conditions) || isstring(conditions) || isa(conditions, 'tau.ode.condition')
                conditions = {conditions};
            end
            % convert strings to chars (tackles " " as ' ')
            if iscell(conditions)
                for i = 1:length(conditions)
                    conditions{i} = convertStringsToChars(conditions{i});
                end
            end

            [m,n] = size(conditions);
            if m ~= 1 && n ~= 1 && ~isempty(conditions)
                msg = 'conditions can not have an array shape';
                error('tau.problem: unknown type of condition: %s', msg)
            end
            if n ~= 1
                conditions = conditions';
            end
        end

        function [conditions_, nconditions_, isGIVP_] = process_conditions(conditions, nequations_, domain_, pieces_)
            nconditions_ = size(conditions, 1);
            conditions_ = cell(nconditions_, 1);
            isGIVP_ = true;
            for k = 1:nconditions_
                if isa(conditions{k}, 'cell')
                    cond_ = conditions{k};
                else
                    cond_ = conditions(k);
                end

                for m = 1:2:length(cond_)
                    if ischar(cond_{m}) || isstring(cond_{m})
                        cond_{m} = tau.ode.condition.fromstr(cond_{m}, nequations_);
                    elseif isa(cond_{m}, 'function_handle')
                        cond_{m} = cond_{m}(tau.ode.condition(nequations_));
                    end

                    % TODO: move to validation stage?
                    % validate the points from conditions (should be inside the domain)
                    if any(cond_{m}.point < domain_(1) | cond_{m}.point > domain_(2))
                        error('tau.problem: error in condition #%d a point is outside the integration interval', k)
                    end
                end

                if isa(conditions{k}, 'cell')
                    conditions_{k} = tau.eig.condition(cond_,nequations_);
                else
                    conditions_{k} = cond_{1};
                end

                if ~conditions_{k}.isGIVP(pieces_)
                    isGIVP_ = false;
                end
            end
        end

        function condpart_ = partition_conditions(derivOrder_, nconditions_);
            % Assign the number of conditions to be distributed by each equation

            condpart_ = max(derivOrder_, [], 2);
            cond_anomaly = sum(condpart_) - nconditions_;

            % If there is an anomaly try to flatten the number of lines removed
            inc = 2*(cond_anomaly < 0) -1;
            for k=1:abs(cond_anomaly)
                if inc > 0
                    [~, ik] = min(condpart_);
                else
                    [~, ik] = max(condpart_);
                end
                condpart_(ik) = condpart_(ik) + inc;
            end
        end
    end
end
