# Tau Toolbox
Version 0.91 : 2022-04-26

## CONTRIBUTING

By contributing code to Tau Toolbox, you agree to release it under the project's
[license](https://bitbucket.org/tautoolbox/tautoolbox/src/main/) 
and moreover certify that you have the right to do so.

## Testing

To run the package self tests place the `MOxUnit`  package, available for MATLAB
and Octave, in your path and then on the root directory of the Tau Toolbox run:

```matlab
moxunit_runtests tests -verbose
```
