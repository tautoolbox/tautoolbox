# Tau Toolbox

   AUTHORS:

       Paulo B. Vasconcelos
       Centro de Matemática - Universidade do Porto
       Email: pjv@fep.up.pt

       José M. A. Matos
       Centro de Matemática - Universidade do Porto
       Email: jma@isep.ipp.pt

       José A. O. Matos
       Centro de Matemática - Universidade do Porto
       Email: jamatos@fep.up.pt

   REFERENCE:

       Solving differential eigenproblems via the spectral Tau method
       NUMERICAL ALGORITHMS
       DOI: https://doi.org/10.1007/s11075-022-01366-z

   SOFTWARE REVISION DATE:

       Version 0.91 : 2022-05-26

   SOFTWARE LANGUAGE:

       MATLAB 9.12.0 (R2022a)
       Octave 8.1.0


Tau Toolbox is a MATLAB/Octave library for the solution of integro-differential
problems based on the Lanczos' Tau method.

Tau Toolbox is free software released under the GNU Lesser General Public
License version 3 (LGPLv3). A copy of the License is enclosed in the project.

Tau Toolbox also bundles external software, present in the aptly named **external** directory.
The respective licenses are available there.

## Installation

Just clone the Tau Toolbox repository following
`git clone https://bitbucket.org/tautoolbox/tautoolbox.git`.
Tau Toolbox can also be obtained at https://cmup.fc.up.pt/tautoolbox/.
All it takes to get the package working is to run the `tauPath` script to set
the paths required to allow the package to run correctly.

### Versions supported

The Tau Toolbox works with the most recently released stable versions of Matlab(R) and Octave.

The Tau Toolbox works under Matlab(R), with the oldest version tested being 2015a.

Under Octave all of the functionality has been tested, and it works, with Octave 5 although the
eigenvalue problems require Octave 6. All the functionality works for Octave 6 and higher.

We try to use new available features in a compatible way with older versions.

### Help

A Tau Toolbox User Guide will be made available soon, along with a set of
Technical Reports.

### Support

The project is very recent. Nevertheless it has been thoroughly tested.
The project supports the tool in the sense that reports of errors or
poor performance will gain immediate attention from the developers.

## Getting started

The best way to get started is by using the many
[examples](https://bitbucket.org/tautoolbox/tautoolbox/src/main/examples/)
provided.
A list of examples can be obtained typing `tau.example()`, and a particular
one can be executed like `tau.example('tau_logo')`.

Tackle your problems with ease:

* To solve the integro-differential problem
$u'(x)+2u(x)+5 \int_0^x u(t) dt = 1$ in $[0,5]$ with $u(0)=1$,
just type:

```matlab
equation = {@(x,u) diff(u)+2*u+5*volt(u), @(x) 1};  % integro-differential equation
domain = [0 5];                                     % domain
condition = @(u) u(0) - 1;                          % initial condition
problem = tau.problem(equation, domain, condition); % set the problem
u = tau.solve(problem);                             % solve the problem
figure(), plot(u)                                   % plot the approximate solution computed via the Tau method 
```

* To solve the differential eigenvalue problem $y^{(4)}(x)=\lambda y(x)$, with
boundary conditions
$y(0)=y(1)=0$ and $y'(0)=y''(1)=0$,
just type:

```matlab
equation = {@(x,y) diff(y,4), @(x,y) y};                  % differential equation
domain = [0, 1];                                          % domain
conditions = @(y) {y(0); y(1); diff(y,1,0); diff(y,2,1)}; % boundary conditions
problem = tau.problem(equation, domain, conditions);      % set the problem
lambda = tau.solve(problem)                               % solve the eigenvalue problem
```